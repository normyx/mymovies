package com.mgoulene.web.rest;

import com.mgoulene.service.TMDBGenreService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.TMDBGenreDTO;
import com.mgoulene.service.dto.TMDBGenreCriteria;
import com.mgoulene.service.TMDBGenreQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBGenre}.
 */
@RestController
@RequestMapping("/api")
public class TMDBGenreResource {

    private final Logger log = LoggerFactory.getLogger(TMDBGenreResource.class);

    private static final String ENTITY_NAME = "tMDBGenre";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TMDBGenreService tMDBGenreService;

    private final TMDBGenreQueryService tMDBGenreQueryService;

    public TMDBGenreResource(TMDBGenreService tMDBGenreService, TMDBGenreQueryService tMDBGenreQueryService) {
        this.tMDBGenreService = tMDBGenreService;
        this.tMDBGenreQueryService = tMDBGenreQueryService;
    }

    /**
     * {@code POST  /tmdb-genres} : Create a new tMDBGenre.
     *
     * @param tMDBGenreDTO the tMDBGenreDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tMDBGenreDTO, or with status {@code 400 (Bad Request)} if the tMDBGenre has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tmdb-genres")
    public ResponseEntity<TMDBGenreDTO> createTMDBGenre(@Valid @RequestBody TMDBGenreDTO tMDBGenreDTO) throws URISyntaxException {
        log.debug("REST request to save TMDBGenre : {}", tMDBGenreDTO);
        if (tMDBGenreDTO.getId() != null) {
            throw new BadRequestAlertException("A new tMDBGenre cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TMDBGenreDTO result = tMDBGenreService.save(tMDBGenreDTO);
        return ResponseEntity.created(new URI("/api/tmdb-genres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tmdb-genres} : Updates an existing tMDBGenre.
     *
     * @param tMDBGenreDTO the tMDBGenreDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tMDBGenreDTO,
     * or with status {@code 400 (Bad Request)} if the tMDBGenreDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tMDBGenreDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tmdb-genres")
    public ResponseEntity<TMDBGenreDTO> updateTMDBGenre(@Valid @RequestBody TMDBGenreDTO tMDBGenreDTO) throws URISyntaxException {
        log.debug("REST request to update TMDBGenre : {}", tMDBGenreDTO);
        if (tMDBGenreDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TMDBGenreDTO result = tMDBGenreService.save(tMDBGenreDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tMDBGenreDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tmdb-genres} : get all the tMDBGenres.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tMDBGenres in body.
     */
    @GetMapping("/tmdb-genres")
    public ResponseEntity<List<TMDBGenreDTO>> getAllTMDBGenres(TMDBGenreCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TMDBGenres by criteria: {}", criteria);
        Page<TMDBGenreDTO> page = tMDBGenreQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tmdb-genres/count} : count all the tMDBGenres.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tmdb-genres/count")
    public ResponseEntity<Long> countTMDBGenres(TMDBGenreCriteria criteria) {
        log.debug("REST request to count TMDBGenres by criteria: {}", criteria);
        return ResponseEntity.ok().body(tMDBGenreQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tmdb-genres/:id} : get the "id" tMDBGenre.
     *
     * @param id the id of the tMDBGenreDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tMDBGenreDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tmdb-genres/{id}")
    public ResponseEntity<TMDBGenreDTO> getTMDBGenre(@PathVariable Long id) {
        log.debug("REST request to get TMDBGenre : {}", id);
        Optional<TMDBGenreDTO> tMDBGenreDTO = tMDBGenreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tMDBGenreDTO);
    }

    /**
     * {@code DELETE  /tmdb-genres/:id} : delete the "id" tMDBGenre.
     *
     * @param id the id of the tMDBGenreDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tmdb-genres/{id}")
    public ResponseEntity<Void> deleteTMDBGenre(@PathVariable Long id) {
        log.debug("REST request to delete TMDBGenre : {}", id);
        tMDBGenreService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tmdb-genres?query=:query} : search for the tMDBGenre corresponding
     * to the query.
     *
     * @param query the query of the tMDBGenre search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-genres")
    public ResponseEntity<List<TMDBGenreDTO>> searchTMDBGenres(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TMDBGenres for query {}", query);
        Page<TMDBGenreDTO> page = tMDBGenreService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
