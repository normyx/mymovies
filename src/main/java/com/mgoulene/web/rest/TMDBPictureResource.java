package com.mgoulene.web.rest;

import com.mgoulene.service.TMDBPictureService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.TMDBPictureDTO;
import com.mgoulene.service.dto.TMDBPictureCriteria;
import com.mgoulene.service.TMDBPictureQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBPicture}.
 */
@RestController
@RequestMapping("/api")
public class TMDBPictureResource {

    private final Logger log = LoggerFactory.getLogger(TMDBPictureResource.class);

    private static final String ENTITY_NAME = "tMDBPicture";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TMDBPictureService tMDBPictureService;

    private final TMDBPictureQueryService tMDBPictureQueryService;

    public TMDBPictureResource(TMDBPictureService tMDBPictureService, TMDBPictureQueryService tMDBPictureQueryService) {
        this.tMDBPictureService = tMDBPictureService;
        this.tMDBPictureQueryService = tMDBPictureQueryService;
    }

    /**
     * {@code POST  /tmdb-pictures} : Create a new tMDBPicture.
     *
     * @param tMDBPictureDTO the tMDBPictureDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tMDBPictureDTO, or with status {@code 400 (Bad Request)} if the tMDBPicture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tmdb-pictures")
    public ResponseEntity<TMDBPictureDTO> createTMDBPicture(@Valid @RequestBody TMDBPictureDTO tMDBPictureDTO) throws URISyntaxException {
        log.debug("REST request to save TMDBPicture : {}", tMDBPictureDTO);
        if (tMDBPictureDTO.getId() != null) {
            throw new BadRequestAlertException("A new tMDBPicture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TMDBPictureDTO result = tMDBPictureService.save(tMDBPictureDTO);
        return ResponseEntity.created(new URI("/api/tmdb-pictures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tmdb-pictures} : Updates an existing tMDBPicture.
     *
     * @param tMDBPictureDTO the tMDBPictureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tMDBPictureDTO,
     * or with status {@code 400 (Bad Request)} if the tMDBPictureDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tMDBPictureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tmdb-pictures")
    public ResponseEntity<TMDBPictureDTO> updateTMDBPicture(@Valid @RequestBody TMDBPictureDTO tMDBPictureDTO) throws URISyntaxException {
        log.debug("REST request to update TMDBPicture : {}", tMDBPictureDTO);
        if (tMDBPictureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TMDBPictureDTO result = tMDBPictureService.save(tMDBPictureDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tMDBPictureDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tmdb-pictures} : get all the tMDBPictures.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tMDBPictures in body.
     */
    @GetMapping("/tmdb-pictures")
    public ResponseEntity<List<TMDBPictureDTO>> getAllTMDBPictures(TMDBPictureCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TMDBPictures by criteria: {}", criteria);
        Page<TMDBPictureDTO> page = tMDBPictureQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tmdb-pictures/count} : count all the tMDBPictures.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tmdb-pictures/count")
    public ResponseEntity<Long> countTMDBPictures(TMDBPictureCriteria criteria) {
        log.debug("REST request to count TMDBPictures by criteria: {}", criteria);
        return ResponseEntity.ok().body(tMDBPictureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tmdb-pictures/:id} : get the "id" tMDBPicture.
     *
     * @param id the id of the tMDBPictureDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tMDBPictureDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tmdb-pictures/{id}")
    public ResponseEntity<TMDBPictureDTO> getTMDBPicture(@PathVariable Long id) {
        log.debug("REST request to get TMDBPicture : {}", id);
        Optional<TMDBPictureDTO> tMDBPictureDTO = tMDBPictureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tMDBPictureDTO);
    }

    /**
     * {@code DELETE  /tmdb-pictures/:id} : delete the "id" tMDBPicture.
     *
     * @param id the id of the tMDBPictureDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tmdb-pictures/{id}")
    public ResponseEntity<Void> deleteTMDBPicture(@PathVariable Long id) {
        log.debug("REST request to delete TMDBPicture : {}", id);
        tMDBPictureService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tmdb-pictures?query=:query} : search for the tMDBPicture corresponding
     * to the query.
     *
     * @param query the query of the tMDBPicture search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-pictures")
    public ResponseEntity<List<TMDBPictureDTO>> searchTMDBPictures(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TMDBPictures for query {}", query);
        Page<TMDBPictureDTO> page = tMDBPictureService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
