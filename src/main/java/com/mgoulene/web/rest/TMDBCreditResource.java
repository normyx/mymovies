package com.mgoulene.web.rest;

import com.mgoulene.service.TMDBCreditService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.TMDBCreditDTO;
import com.mgoulene.service.dto.TMDBCreditCriteria;
import com.mgoulene.service.TMDBCreditQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBCredit}.
 */
@RestController
@RequestMapping("/api")
public class TMDBCreditResource {

    private final Logger log = LoggerFactory.getLogger(TMDBCreditResource.class);

    private static final String ENTITY_NAME = "tMDBCredit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TMDBCreditService tMDBCreditService;

    private final TMDBCreditQueryService tMDBCreditQueryService;

    public TMDBCreditResource(TMDBCreditService tMDBCreditService, TMDBCreditQueryService tMDBCreditQueryService) {
        this.tMDBCreditService = tMDBCreditService;
        this.tMDBCreditQueryService = tMDBCreditQueryService;
    }

    /**
     * {@code POST  /tmdb-credits} : Create a new tMDBCredit.
     *
     * @param tMDBCreditDTO the tMDBCreditDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tMDBCreditDTO, or with status {@code 400 (Bad Request)} if the tMDBCredit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tmdb-credits")
    public ResponseEntity<TMDBCreditDTO> createTMDBCredit(@Valid @RequestBody TMDBCreditDTO tMDBCreditDTO) throws URISyntaxException {
        log.debug("REST request to save TMDBCredit : {}", tMDBCreditDTO);
        if (tMDBCreditDTO.getId() != null) {
            throw new BadRequestAlertException("A new tMDBCredit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TMDBCreditDTO result = tMDBCreditService.save(tMDBCreditDTO);
        return ResponseEntity.created(new URI("/api/tmdb-credits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tmdb-credits} : Updates an existing tMDBCredit.
     *
     * @param tMDBCreditDTO the tMDBCreditDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tMDBCreditDTO,
     * or with status {@code 400 (Bad Request)} if the tMDBCreditDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tMDBCreditDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tmdb-credits")
    public ResponseEntity<TMDBCreditDTO> updateTMDBCredit(@Valid @RequestBody TMDBCreditDTO tMDBCreditDTO) throws URISyntaxException {
        log.debug("REST request to update TMDBCredit : {}", tMDBCreditDTO);
        if (tMDBCreditDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TMDBCreditDTO result = tMDBCreditService.save(tMDBCreditDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tMDBCreditDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tmdb-credits} : get all the tMDBCredits.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tMDBCredits in body.
     */
    @GetMapping("/tmdb-credits")
    public ResponseEntity<List<TMDBCreditDTO>> getAllTMDBCredits(TMDBCreditCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TMDBCredits by criteria: {}", criteria);
        Page<TMDBCreditDTO> page = tMDBCreditQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tmdb-credits/count} : count all the tMDBCredits.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tmdb-credits/count")
    public ResponseEntity<Long> countTMDBCredits(TMDBCreditCriteria criteria) {
        log.debug("REST request to count TMDBCredits by criteria: {}", criteria);
        return ResponseEntity.ok().body(tMDBCreditQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tmdb-credits/:id} : get the "id" tMDBCredit.
     *
     * @param id the id of the tMDBCreditDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tMDBCreditDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tmdb-credits/{id}")
    public ResponseEntity<TMDBCreditDTO> getTMDBCredit(@PathVariable Long id) {
        log.debug("REST request to get TMDBCredit : {}", id);
        Optional<TMDBCreditDTO> tMDBCreditDTO = tMDBCreditService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tMDBCreditDTO);
    }

    /**
     * {@code DELETE  /tmdb-credits/:id} : delete the "id" tMDBCredit.
     *
     * @param id the id of the tMDBCreditDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tmdb-credits/{id}")
    public ResponseEntity<Void> deleteTMDBCredit(@PathVariable Long id) {
        log.debug("REST request to delete TMDBCredit : {}", id);
        tMDBCreditService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tmdb-credits?query=:query} : search for the tMDBCredit corresponding
     * to the query.
     *
     * @param query the query of the tMDBCredit search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-credits")
    public ResponseEntity<List<TMDBCreditDTO>> searchTMDBCredits(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TMDBCredits for query {}", query);
        Page<TMDBCreditDTO> page = tMDBCreditService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
