package com.mgoulene.web.rest;

import com.mgoulene.service.TMDBMovieService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.TMDBMovieDTO;
import com.mgoulene.tmdb.service.dto.TMDBMovieAPIDTO;
import com.mgoulene.service.dto.Response;
import com.mgoulene.service.dto.TMDBMovieCriteria;
import com.mgoulene.service.TMDBMovieQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class TMDBMovieExtResource {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieResource.class);

    private static final String ENTITY_NAME = "tMDBMovie";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    

    public TMDBMovieExtResource() {
        
    }

    

    

    

    /**
     * {@code GET  /tmdb-movies/count} : count all the tMDBMovies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    /* @GetMapping("/tmdb-movies-ext/count")
    public ResponseEntity<Long> countTMDBMovies(TMDBMovieCriteria criteria) {
        log.debug("REST request to count TMDBMovies by criteria: {}", criteria);
        return ResponseEntity.ok().body(tMDBMovieQueryService.countByCriteria(criteria));
    } */

    /**
     * {@code GET  /tmdb-movies/:id} : get the "id" tMDBMovie.
     *
     * @param id the id of the tMDBMovieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tMDBMovieDTO, or with status {@code 404 (Not Found)}.
     */
    /* @GetMapping("/tmdb-movies/{id}")
    public ResponseEntity<TMDBMovieDTO> getTMDBMovie(@PathVariable Long id) {
        log.debug("REST request to get TMDBMovie : {}", id);
        Optional<TMDBMovieDTO> tMDBMovieDTO = tMDBMovieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tMDBMovieDTO);
    }
 */
    

    /**
     * {@code SEARCH  /_search/tmdb-movies?query=:query} : search for the tMDBMovie corresponding
     * to the query.
     *
     * @param query the query of the tMDBMovie search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-movies-ext")
    public List<TMDBMovieAPIDTO> searchTMDBMovies(@RequestParam String query) {
        log.debug("REST request to search for TMDBMovies::searchTMDBMovies for query {}", query);
        RestTemplate restTemplate = new RestTemplate(); 
        Response movies = restTemplate.getForObject( "https://api.themoviedb.org/3/search/movie?api_key=c344516cac0ae134d50ea9ed99e6a42c&query="+query, Response.class);
        //System.out.println(movies);
        return Arrays.asList(movies.getResults());
        }
}
