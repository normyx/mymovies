package com.mgoulene.web.rest;

import com.mgoulene.service.TMDBPersonService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.TMDBPersonDTO;
import com.mgoulene.service.dto.TMDBPersonCriteria;
import com.mgoulene.service.TMDBPersonQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBPerson}.
 */
@RestController
@RequestMapping("/api")
public class TMDBPersonResource {

    private final Logger log = LoggerFactory.getLogger(TMDBPersonResource.class);

    private static final String ENTITY_NAME = "tMDBPerson";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TMDBPersonService tMDBPersonService;

    private final TMDBPersonQueryService tMDBPersonQueryService;

    public TMDBPersonResource(TMDBPersonService tMDBPersonService, TMDBPersonQueryService tMDBPersonQueryService) {
        this.tMDBPersonService = tMDBPersonService;
        this.tMDBPersonQueryService = tMDBPersonQueryService;
    }

    /**
     * {@code POST  /tmdb-people} : Create a new tMDBPerson.
     *
     * @param tMDBPersonDTO the tMDBPersonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tMDBPersonDTO, or with status {@code 400 (Bad Request)} if the tMDBPerson has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tmdb-people")
    public ResponseEntity<TMDBPersonDTO> createTMDBPerson(@Valid @RequestBody TMDBPersonDTO tMDBPersonDTO) throws URISyntaxException {
        log.debug("REST request to save TMDBPerson : {}", tMDBPersonDTO);
        if (tMDBPersonDTO.getId() != null) {
            throw new BadRequestAlertException("A new tMDBPerson cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TMDBPersonDTO result = tMDBPersonService.save(tMDBPersonDTO);
        return ResponseEntity.created(new URI("/api/tmdb-people/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tmdb-people} : Updates an existing tMDBPerson.
     *
     * @param tMDBPersonDTO the tMDBPersonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tMDBPersonDTO,
     * or with status {@code 400 (Bad Request)} if the tMDBPersonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tMDBPersonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tmdb-people")
    public ResponseEntity<TMDBPersonDTO> updateTMDBPerson(@Valid @RequestBody TMDBPersonDTO tMDBPersonDTO) throws URISyntaxException {
        log.debug("REST request to update TMDBPerson : {}", tMDBPersonDTO);
        if (tMDBPersonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TMDBPersonDTO result = tMDBPersonService.save(tMDBPersonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tMDBPersonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tmdb-people} : get all the tMDBPeople.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tMDBPeople in body.
     */
    @GetMapping("/tmdb-people")
    public ResponseEntity<List<TMDBPersonDTO>> getAllTMDBPeople(TMDBPersonCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TMDBPeople by criteria: {}", criteria);
        Page<TMDBPersonDTO> page = tMDBPersonQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tmdb-people/count} : count all the tMDBPeople.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tmdb-people/count")
    public ResponseEntity<Long> countTMDBPeople(TMDBPersonCriteria criteria) {
        log.debug("REST request to count TMDBPeople by criteria: {}", criteria);
        return ResponseEntity.ok().body(tMDBPersonQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tmdb-people/:id} : get the "id" tMDBPerson.
     *
     * @param id the id of the tMDBPersonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tMDBPersonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tmdb-people/{id}")
    public ResponseEntity<TMDBPersonDTO> getTMDBPerson(@PathVariable Long id) {
        log.debug("REST request to get TMDBPerson : {}", id);
        Optional<TMDBPersonDTO> tMDBPersonDTO = tMDBPersonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tMDBPersonDTO);
    }

    /**
     * {@code DELETE  /tmdb-people/:id} : delete the "id" tMDBPerson.
     *
     * @param id the id of the tMDBPersonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tmdb-people/{id}")
    public ResponseEntity<Void> deleteTMDBPerson(@PathVariable Long id) {
        log.debug("REST request to delete TMDBPerson : {}", id);
        tMDBPersonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tmdb-people?query=:query} : search for the tMDBPerson corresponding
     * to the query.
     *
     * @param query the query of the tMDBPerson search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-people")
    public ResponseEntity<List<TMDBPersonDTO>> searchTMDBPeople(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TMDBPeople for query {}", query);
        Page<TMDBPersonDTO> page = tMDBPersonService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
