package com.mgoulene.web.rest;

import com.mgoulene.service.TMDBMovieService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.TMDBMovieDTO;
import com.mgoulene.service.dto.Response;
import com.mgoulene.service.dto.TMDBMovieCriteria;
import com.mgoulene.service.TMDBMovieQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class TMDBMovieResource {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieResource.class);

    private static final String ENTITY_NAME = "tMDBMovie";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TMDBMovieService tMDBMovieService;

    private final TMDBMovieQueryService tMDBMovieQueryService;

    public TMDBMovieResource(TMDBMovieService tMDBMovieService, TMDBMovieQueryService tMDBMovieQueryService) {
        this.tMDBMovieService = tMDBMovieService;
        this.tMDBMovieQueryService = tMDBMovieQueryService;
    }

    /**
     * {@code POST  /tmdb-movies} : Create a new tMDBMovie.
     *
     * @param tMDBMovieDTO the tMDBMovieDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tMDBMovieDTO, or with status {@code 400 (Bad Request)} if the tMDBMovie has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tmdb-movies")
    public ResponseEntity<TMDBMovieDTO> createTMDBMovie(@Valid @RequestBody TMDBMovieDTO tMDBMovieDTO) throws URISyntaxException {
        log.debug("REST request to save TMDBMovie : {}", tMDBMovieDTO);
        if (tMDBMovieDTO.getId() != null) {
            throw new BadRequestAlertException("A new tMDBMovie cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TMDBMovieDTO result = tMDBMovieService.save(tMDBMovieDTO);
        return ResponseEntity.created(new URI("/api/tmdb-movies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tmdb-movies} : Updates an existing tMDBMovie.
     *
     * @param tMDBMovieDTO the tMDBMovieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tMDBMovieDTO,
     * or with status {@code 400 (Bad Request)} if the tMDBMovieDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tMDBMovieDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tmdb-movies")
    public ResponseEntity<TMDBMovieDTO> updateTMDBMovie(@Valid @RequestBody TMDBMovieDTO tMDBMovieDTO) throws URISyntaxException {
        log.debug("REST request to update TMDBMovie : {}", tMDBMovieDTO);
        if (tMDBMovieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TMDBMovieDTO result = tMDBMovieService.save(tMDBMovieDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tMDBMovieDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tmdb-movies} : get all the tMDBMovies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tMDBMovies in body.
     */
    @GetMapping("/tmdb-movies")
    public ResponseEntity<List<TMDBMovieDTO>> getAllTMDBMovies(TMDBMovieCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TMDBMovies by criteria: {}", criteria);
        Page<TMDBMovieDTO> page = tMDBMovieQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tmdb-movies/count} : count all the tMDBMovies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tmdb-movies/count")
    public ResponseEntity<Long> countTMDBMovies(TMDBMovieCriteria criteria) {
        log.debug("REST request to count TMDBMovies by criteria: {}", criteria);
        return ResponseEntity.ok().body(tMDBMovieQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tmdb-movies/:id} : get the "id" tMDBMovie.
     *
     * @param id the id of the tMDBMovieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tMDBMovieDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tmdb-movies/{id}")
    public ResponseEntity<TMDBMovieDTO> getTMDBMovie(@PathVariable Long id) {
        log.debug("REST request to get TMDBMovie : {}", id);
        Optional<TMDBMovieDTO> tMDBMovieDTO = tMDBMovieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tMDBMovieDTO);
    }

    /**
     * {@code DELETE  /tmdb-movies/:id} : delete the "id" tMDBMovie.
     *
     * @param id the id of the tMDBMovieDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tmdb-movies/{id}")
    public ResponseEntity<Void> deleteTMDBMovie(@PathVariable Long id) {
        log.debug("REST request to delete TMDBMovie : {}", id);
        tMDBMovieService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/tmdb-movies?query=:query} : search for the tMDBMovie corresponding
     * to the query.
     *
     * @param query the query of the tMDBMovie search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-movies")
    public ResponseEntity<List<TMDBMovieDTO>> searchTMDBMovies(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TMDBMovies for query {}", query);
        /* RestTemplate restTemplate = new RestTemplate(); 
        Response movies = restTemplate.getForObject( "https://api.themoviedb.org/3/search/movie?api_key=c344516cac0ae134d50ea9ed99e6a42c&query="+query, Response.class);
        System.out.println(movies); */



        Page<TMDBMovieDTO> page = tMDBMovieService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
        }
}
