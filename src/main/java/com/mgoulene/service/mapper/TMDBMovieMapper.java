package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.TMDBMovieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TMDBMovie} and its DTO {@link TMDBMovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {TMDBGenreMapper.class})
public interface TMDBMovieMapper extends EntityMapper<TMDBMovieDTO, TMDBMovie> {


    @Mapping(target = "credits", ignore = true)
    @Mapping(target = "removeCredits", ignore = true)
    @Mapping(target = "removeGenre", ignore = true)
    TMDBMovie toEntity(TMDBMovieDTO tMDBMovieDTO);

    default TMDBMovie fromId(Long id) {
        if (id == null) {
            return null;
        }
        TMDBMovie tMDBMovie = new TMDBMovie();
        tMDBMovie.setId(id);
        return tMDBMovie;
    }
}
