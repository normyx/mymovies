package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.TMDBPictureDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TMDBPicture} and its DTO {@link TMDBPictureDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBPictureMapper extends EntityMapper<TMDBPictureDTO, TMDBPicture> {



    default TMDBPicture fromId(Long id) {
        if (id == null) {
            return null;
        }
        TMDBPicture tMDBPicture = new TMDBPicture();
        tMDBPicture.setId(id);
        return tMDBPicture;
    }
}
