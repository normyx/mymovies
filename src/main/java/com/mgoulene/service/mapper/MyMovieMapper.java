package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.MyMovieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MyMovie} and its DTO {@link MyMovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {TMDBMovieMapper.class, UserMapper.class})
public interface MyMovieMapper extends EntityMapper<MyMovieDTO, MyMovie> {

    @Mapping(source = "movie.id", target = "movieId")
    @Mapping(source = "movie.title", target = "movieTitle")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    MyMovieDTO toDto(MyMovie myMovie);

    @Mapping(source = "movieId", target = "movie")
    @Mapping(source = "userId", target = "user")
    MyMovie toEntity(MyMovieDTO myMovieDTO);

    default MyMovie fromId(Long id) {
        if (id == null) {
            return null;
        }
        MyMovie myMovie = new MyMovie();
        myMovie.setId(id);
        return myMovie;
    }
}
