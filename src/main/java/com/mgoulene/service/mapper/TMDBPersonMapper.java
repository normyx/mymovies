package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.TMDBPersonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TMDBPerson} and its DTO {@link TMDBPersonDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBPersonMapper extends EntityMapper<TMDBPersonDTO, TMDBPerson> {



    default TMDBPerson fromId(Long id) {
        if (id == null) {
            return null;
        }
        TMDBPerson tMDBPerson = new TMDBPerson();
        tMDBPerson.setId(id);
        return tMDBPerson;
    }
}
