package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.TMDBGenreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TMDBGenre} and its DTO {@link TMDBGenreDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBGenreMapper extends EntityMapper<TMDBGenreDTO, TMDBGenre> {


    @Mapping(target = "movies", ignore = true)
    @Mapping(target = "removeMovie", ignore = true)
    TMDBGenre toEntity(TMDBGenreDTO tMDBGenreDTO);

    default TMDBGenre fromId(Long id) {
        if (id == null) {
            return null;
        }
        TMDBGenre tMDBGenre = new TMDBGenre();
        tMDBGenre.setId(id);
        return tMDBGenre;
    }
}
