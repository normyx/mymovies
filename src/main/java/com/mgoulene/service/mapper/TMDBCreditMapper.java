package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.TMDBCreditDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TMDBCredit} and its DTO {@link TMDBCreditDTO}.
 */
@Mapper(componentModel = "spring", uses = {TMDBPersonMapper.class, TMDBMovieMapper.class})
public interface TMDBCreditMapper extends EntityMapper<TMDBCreditDTO, TMDBCredit> {

    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.name", target = "personName")
    @Mapping(source = "movie.id", target = "movieId")
    @Mapping(source = "movie.title", target = "movieTitle")
    TMDBCreditDTO toDto(TMDBCredit tMDBCredit);

    @Mapping(source = "personId", target = "person")
    @Mapping(source = "movieId", target = "movie")
    TMDBCredit toEntity(TMDBCreditDTO tMDBCreditDTO);

    default TMDBCredit fromId(Long id) {
        if (id == null) {
            return null;
        }
        TMDBCredit tMDBCredit = new TMDBCredit();
        tMDBCredit.setId(id);
        return tMDBCredit;
    }
}
