package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.TMDBGenre;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.TMDBGenreRepository;
import com.mgoulene.repository.search.TMDBGenreSearchRepository;
import com.mgoulene.service.dto.TMDBGenreCriteria;
import com.mgoulene.service.dto.TMDBGenreDTO;
import com.mgoulene.service.mapper.TMDBGenreMapper;

/**
 * Service for executing complex queries for {@link TMDBGenre} entities in the database.
 * The main input is a {@link TMDBGenreCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TMDBGenreDTO} or a {@link Page} of {@link TMDBGenreDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TMDBGenreQueryService extends QueryService<TMDBGenre> {

    private final Logger log = LoggerFactory.getLogger(TMDBGenreQueryService.class);

    private final TMDBGenreRepository tMDBGenreRepository;

    private final TMDBGenreMapper tMDBGenreMapper;

    private final TMDBGenreSearchRepository tMDBGenreSearchRepository;

    public TMDBGenreQueryService(TMDBGenreRepository tMDBGenreRepository, TMDBGenreMapper tMDBGenreMapper, TMDBGenreSearchRepository tMDBGenreSearchRepository) {
        this.tMDBGenreRepository = tMDBGenreRepository;
        this.tMDBGenreMapper = tMDBGenreMapper;
        this.tMDBGenreSearchRepository = tMDBGenreSearchRepository;
    }

    /**
     * Return a {@link List} of {@link TMDBGenreDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TMDBGenreDTO> findByCriteria(TMDBGenreCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TMDBGenre> specification = createSpecification(criteria);
        return tMDBGenreMapper.toDto(tMDBGenreRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TMDBGenreDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBGenreDTO> findByCriteria(TMDBGenreCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TMDBGenre> specification = createSpecification(criteria);
        return tMDBGenreRepository.findAll(specification, page)
            .map(tMDBGenreMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TMDBGenreCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TMDBGenre> specification = createSpecification(criteria);
        return tMDBGenreRepository.count(specification);
    }

    /**
     * Function to convert {@link TMDBGenreCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TMDBGenre> createSpecification(TMDBGenreCriteria criteria) {
        Specification<TMDBGenre> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TMDBGenre_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTmdbId(), TMDBGenre_.tmdbId));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), TMDBGenre_.name));
            }
            if (criteria.getMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getMovieId(),
                    root -> root.join(TMDBGenre_.movies, JoinType.LEFT).get(TMDBMovie_.id)));
            }
        }
        return specification;
    }
}
