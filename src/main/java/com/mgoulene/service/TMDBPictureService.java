package com.mgoulene.service;

import com.mgoulene.domain.TMDBPicture;
import com.mgoulene.repository.TMDBPictureRepository;
import com.mgoulene.repository.search.TMDBPictureSearchRepository;
import com.mgoulene.service.dto.TMDBPictureDTO;
import com.mgoulene.service.mapper.TMDBPictureMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link TMDBPicture}.
 */
@Service
@Transactional
public class TMDBPictureService {

    private final Logger log = LoggerFactory.getLogger(TMDBPictureService.class);

    private final TMDBPictureRepository tMDBPictureRepository;

    private final TMDBPictureMapper tMDBPictureMapper;

    private final TMDBPictureSearchRepository tMDBPictureSearchRepository;

    public TMDBPictureService(TMDBPictureRepository tMDBPictureRepository, TMDBPictureMapper tMDBPictureMapper, TMDBPictureSearchRepository tMDBPictureSearchRepository) {
        this.tMDBPictureRepository = tMDBPictureRepository;
        this.tMDBPictureMapper = tMDBPictureMapper;
        this.tMDBPictureSearchRepository = tMDBPictureSearchRepository;
    }

    /**
     * Save a tMDBPicture.
     *
     * @param tMDBPictureDTO the entity to save.
     * @return the persisted entity.
     */
    public TMDBPictureDTO save(TMDBPictureDTO tMDBPictureDTO) {
        log.debug("Request to save TMDBPicture : {}", tMDBPictureDTO);
        TMDBPicture tMDBPicture = tMDBPictureMapper.toEntity(tMDBPictureDTO);
        tMDBPicture = tMDBPictureRepository.save(tMDBPicture);
        TMDBPictureDTO result = tMDBPictureMapper.toDto(tMDBPicture);
        tMDBPictureSearchRepository.save(tMDBPicture);
        return result;
    }

    /**
     * Get all the tMDBPictures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBPictureDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TMDBPictures");
        return tMDBPictureRepository.findAll(pageable)
            .map(tMDBPictureMapper::toDto);
    }


    /**
     * Get one tMDBPicture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TMDBPictureDTO> findOne(Long id) {
        log.debug("Request to get TMDBPicture : {}", id);
        return tMDBPictureRepository.findById(id)
            .map(tMDBPictureMapper::toDto);
    }

    /**
     * Delete the tMDBPicture by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TMDBPicture : {}", id);
        tMDBPictureRepository.deleteById(id);
        tMDBPictureSearchRepository.deleteById(id);
    }

    /**
     * Search for the tMDBPicture corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBPictureDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TMDBPictures for query {}", query);
        return tMDBPictureSearchRepository.search(queryStringQuery(query), pageable)
            .map(tMDBPictureMapper::toDto);
    }
}
