package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.TMDBPerson;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.TMDBPersonRepository;
import com.mgoulene.repository.search.TMDBPersonSearchRepository;
import com.mgoulene.service.dto.TMDBPersonCriteria;
import com.mgoulene.service.dto.TMDBPersonDTO;
import com.mgoulene.service.mapper.TMDBPersonMapper;

/**
 * Service for executing complex queries for {@link TMDBPerson} entities in the database.
 * The main input is a {@link TMDBPersonCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TMDBPersonDTO} or a {@link Page} of {@link TMDBPersonDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TMDBPersonQueryService extends QueryService<TMDBPerson> {

    private final Logger log = LoggerFactory.getLogger(TMDBPersonQueryService.class);

    private final TMDBPersonRepository tMDBPersonRepository;

    private final TMDBPersonMapper tMDBPersonMapper;

    private final TMDBPersonSearchRepository tMDBPersonSearchRepository;

    public TMDBPersonQueryService(TMDBPersonRepository tMDBPersonRepository, TMDBPersonMapper tMDBPersonMapper, TMDBPersonSearchRepository tMDBPersonSearchRepository) {
        this.tMDBPersonRepository = tMDBPersonRepository;
        this.tMDBPersonMapper = tMDBPersonMapper;
        this.tMDBPersonSearchRepository = tMDBPersonSearchRepository;
    }

    /**
     * Return a {@link List} of {@link TMDBPersonDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TMDBPersonDTO> findByCriteria(TMDBPersonCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TMDBPerson> specification = createSpecification(criteria);
        return tMDBPersonMapper.toDto(tMDBPersonRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TMDBPersonDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBPersonDTO> findByCriteria(TMDBPersonCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TMDBPerson> specification = createSpecification(criteria);
        return tMDBPersonRepository.findAll(specification, page)
            .map(tMDBPersonMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TMDBPersonCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TMDBPerson> specification = createSpecification(criteria);
        return tMDBPersonRepository.count(specification);
    }

    /**
     * Function to convert {@link TMDBPersonCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TMDBPerson> createSpecification(TMDBPersonCriteria criteria) {
        Specification<TMDBPerson> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TMDBPerson_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTmdbId(), TMDBPerson_.tmdbId));
            }
            if (criteria.getBirthday() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBirthday(), TMDBPerson_.birthday));
            }
            if (criteria.getDeathday() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeathday(), TMDBPerson_.deathday));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), TMDBPerson_.name));
            }
            if (criteria.getAka() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAka(), TMDBPerson_.aka));
            }
            if (criteria.getGender() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGender(), TMDBPerson_.gender));
            }
            if (criteria.getBiography() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBiography(), TMDBPerson_.biography));
            }
            if (criteria.getPlaceOfBirth() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPlaceOfBirth(), TMDBPerson_.placeOfBirth));
            }
            if (criteria.getHomepage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHomepage(), TMDBPerson_.homepage));
            }
        }
        return specification;
    }
}
