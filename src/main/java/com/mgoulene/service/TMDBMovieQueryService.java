package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.TMDBMovie;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.TMDBMovieRepository;
import com.mgoulene.repository.search.TMDBMovieSearchRepository;
import com.mgoulene.service.dto.TMDBMovieCriteria;
import com.mgoulene.service.dto.TMDBMovieDTO;
import com.mgoulene.service.mapper.TMDBMovieMapper;

/**
 * Service for executing complex queries for {@link TMDBMovie} entities in the database.
 * The main input is a {@link TMDBMovieCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TMDBMovieDTO} or a {@link Page} of {@link TMDBMovieDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TMDBMovieQueryService extends QueryService<TMDBMovie> {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieQueryService.class);

    private final TMDBMovieRepository tMDBMovieRepository;

    private final TMDBMovieMapper tMDBMovieMapper;

    private final TMDBMovieSearchRepository tMDBMovieSearchRepository;

    public TMDBMovieQueryService(TMDBMovieRepository tMDBMovieRepository, TMDBMovieMapper tMDBMovieMapper, TMDBMovieSearchRepository tMDBMovieSearchRepository) {
        this.tMDBMovieRepository = tMDBMovieRepository;
        this.tMDBMovieMapper = tMDBMovieMapper;
        this.tMDBMovieSearchRepository = tMDBMovieSearchRepository;
    }

    /**
     * Return a {@link List} of {@link TMDBMovieDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TMDBMovieDTO> findByCriteria(TMDBMovieCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TMDBMovie> specification = createSpecification(criteria);
        return tMDBMovieMapper.toDto(tMDBMovieRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TMDBMovieDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBMovieDTO> findByCriteria(TMDBMovieCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TMDBMovie> specification = createSpecification(criteria);
        return tMDBMovieRepository.findAll(specification, page)
            .map(tMDBMovieMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TMDBMovieCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TMDBMovie> specification = createSpecification(criteria);
        return tMDBMovieRepository.count(specification);
    }

    /**
     * Function to convert {@link TMDBMovieCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TMDBMovie> createSpecification(TMDBMovieCriteria criteria) {
        Specification<TMDBMovie> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TMDBMovie_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTmdbId(), TMDBMovie_.tmdbId));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), TMDBMovie_.title));
            }
            if (criteria.getForAdult() != null) {
                specification = specification.and(buildSpecification(criteria.getForAdult(), TMDBMovie_.forAdult));
            }
            if (criteria.getHomepage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHomepage(), TMDBMovie_.homepage));
            }
            if (criteria.getOriginalLangage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalLangage(), TMDBMovie_.originalLangage));
            }
            if (criteria.getOriginalTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalTitle(), TMDBMovie_.originalTitle));
            }
            if (criteria.getOverview() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOverview(), TMDBMovie_.overview));
            }
            if (criteria.getTagline() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTagline(), TMDBMovie_.tagline));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), TMDBMovie_.status));
            }
            if (criteria.getVoteAverage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoteAverage(), TMDBMovie_.voteAverage));
            }
            if (criteria.getVoteCount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoteCount(), TMDBMovie_.voteCount));
            }
            if (criteria.getReleaseDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReleaseDate(), TMDBMovie_.releaseDate));
            }
            if (criteria.getCreditsId() != null) {
                specification = specification.and(buildSpecification(criteria.getCreditsId(),
                    root -> root.join(TMDBMovie_.credits, JoinType.LEFT).get(TMDBCredit_.id)));
            }
            if (criteria.getGenreId() != null) {
                specification = specification.and(buildSpecification(criteria.getGenreId(),
                    root -> root.join(TMDBMovie_.genres, JoinType.LEFT).get(TMDBGenre_.id)));
            }
        }
        return specification;
    }
}
