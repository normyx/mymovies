package com.mgoulene.service;

import com.mgoulene.domain.TMDBMovie;
import com.mgoulene.repository.TMDBMovieRepository;
import com.mgoulene.repository.search.TMDBMovieSearchRepository;
import com.mgoulene.service.dto.TMDBMovieDTO;
import com.mgoulene.service.mapper.TMDBMovieMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link TMDBMovie}.
 */
@Service
@Transactional
public class TMDBMovieService {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieService.class);

    private final TMDBMovieRepository tMDBMovieRepository;

    private final TMDBMovieMapper tMDBMovieMapper;

    private final TMDBMovieSearchRepository tMDBMovieSearchRepository;

    public TMDBMovieService(TMDBMovieRepository tMDBMovieRepository, TMDBMovieMapper tMDBMovieMapper, TMDBMovieSearchRepository tMDBMovieSearchRepository) {
        this.tMDBMovieRepository = tMDBMovieRepository;
        this.tMDBMovieMapper = tMDBMovieMapper;
        this.tMDBMovieSearchRepository = tMDBMovieSearchRepository;
    }

    /**
     * Save a tMDBMovie.
     *
     * @param tMDBMovieDTO the entity to save.
     * @return the persisted entity.
     */
    public TMDBMovieDTO save(TMDBMovieDTO tMDBMovieDTO) {
        log.debug("Request to save TMDBMovie : {}", tMDBMovieDTO);
        TMDBMovie tMDBMovie = tMDBMovieMapper.toEntity(tMDBMovieDTO);
        tMDBMovie = tMDBMovieRepository.save(tMDBMovie);
        TMDBMovieDTO result = tMDBMovieMapper.toDto(tMDBMovie);
        tMDBMovieSearchRepository.save(tMDBMovie);
        return result;
    }

    /**
     * Get all the tMDBMovies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBMovieDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TMDBMovies");
        return tMDBMovieRepository.findAll(pageable)
            .map(tMDBMovieMapper::toDto);
    }


    /**
     * Get all the tMDBMovies with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<TMDBMovieDTO> findAllWithEagerRelationships(Pageable pageable) {
        return tMDBMovieRepository.findAllWithEagerRelationships(pageable).map(tMDBMovieMapper::toDto);
    }

    /**
     * Get one tMDBMovie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TMDBMovieDTO> findOne(Long id) {
        log.debug("Request to get TMDBMovie : {}", id);
        return tMDBMovieRepository.findOneWithEagerRelationships(id)
            .map(tMDBMovieMapper::toDto);
    }

    /**
     * Delete the tMDBMovie by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TMDBMovie : {}", id);
        tMDBMovieRepository.deleteById(id);
        tMDBMovieSearchRepository.deleteById(id);
    }

    /**
     * Search for the tMDBMovie corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBMovieDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TMDBMovies for query {}", query);
        return tMDBMovieSearchRepository.search(queryStringQuery(query), pageable)
            .map(tMDBMovieMapper::toDto);
    }
}
