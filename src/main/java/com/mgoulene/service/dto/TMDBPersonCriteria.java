package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.TMDBPerson} entity. This class is used
 * in {@link com.mgoulene.web.rest.TMDBPersonResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tmdb-people?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TMDBPersonCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter tmdbId;

    private LocalDateFilter birthday;

    private LocalDateFilter deathday;

    private StringFilter name;

    private StringFilter aka;

    private IntegerFilter gender;

    private StringFilter biography;

    private StringFilter placeOfBirth;

    private StringFilter homepage;

    public TMDBPersonCriteria() {
    }

    public TMDBPersonCriteria(TMDBPersonCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.birthday = other.birthday == null ? null : other.birthday.copy();
        this.deathday = other.deathday == null ? null : other.deathday.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.aka = other.aka == null ? null : other.aka.copy();
        this.gender = other.gender == null ? null : other.gender.copy();
        this.biography = other.biography == null ? null : other.biography.copy();
        this.placeOfBirth = other.placeOfBirth == null ? null : other.placeOfBirth.copy();
        this.homepage = other.homepage == null ? null : other.homepage.copy();
    }

    @Override
    public TMDBPersonCriteria copy() {
        return new TMDBPersonCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(IntegerFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public LocalDateFilter getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateFilter birthday) {
        this.birthday = birthday;
    }

    public LocalDateFilter getDeathday() {
        return deathday;
    }

    public void setDeathday(LocalDateFilter deathday) {
        this.deathday = deathday;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getAka() {
        return aka;
    }

    public void setAka(StringFilter aka) {
        this.aka = aka;
    }

    public IntegerFilter getGender() {
        return gender;
    }

    public void setGender(IntegerFilter gender) {
        this.gender = gender;
    }

    public StringFilter getBiography() {
        return biography;
    }

    public void setBiography(StringFilter biography) {
        this.biography = biography;
    }

    public StringFilter getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(StringFilter placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public StringFilter getHomepage() {
        return homepage;
    }

    public void setHomepage(StringFilter homepage) {
        this.homepage = homepage;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TMDBPersonCriteria that = (TMDBPersonCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(birthday, that.birthday) &&
            Objects.equals(deathday, that.deathday) &&
            Objects.equals(name, that.name) &&
            Objects.equals(aka, that.aka) &&
            Objects.equals(gender, that.gender) &&
            Objects.equals(biography, that.biography) &&
            Objects.equals(placeOfBirth, that.placeOfBirth) &&
            Objects.equals(homepage, that.homepage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tmdbId,
        birthday,
        deathday,
        name,
        aka,
        gender,
        biography,
        placeOfBirth,
        homepage
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBPersonCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (birthday != null ? "birthday=" + birthday + ", " : "") +
                (deathday != null ? "deathday=" + deathday + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (aka != null ? "aka=" + aka + ", " : "") +
                (gender != null ? "gender=" + gender + ", " : "") +
                (biography != null ? "biography=" + biography + ", " : "") +
                (placeOfBirth != null ? "placeOfBirth=" + placeOfBirth + ", " : "") +
                (homepage != null ? "homepage=" + homepage + ", " : "") +
            "}";
    }

}
