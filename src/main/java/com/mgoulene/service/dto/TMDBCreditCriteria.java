package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.TMDBCredit} entity. This class is used
 * in {@link com.mgoulene.web.rest.TMDBCreditResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tmdb-credits?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TMDBCreditCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tmdbId;

    private StringFilter character;

    private StringFilter creditType;

    private StringFilter department;

    private StringFilter job;

    private IntegerFilter order;

    private LongFilter personId;

    private LongFilter movieId;

    public TMDBCreditCriteria() {
    }

    public TMDBCreditCriteria(TMDBCreditCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.character = other.character == null ? null : other.character.copy();
        this.creditType = other.creditType == null ? null : other.creditType.copy();
        this.department = other.department == null ? null : other.department.copy();
        this.job = other.job == null ? null : other.job.copy();
        this.order = other.order == null ? null : other.order.copy();
        this.personId = other.personId == null ? null : other.personId.copy();
        this.movieId = other.movieId == null ? null : other.movieId.copy();
    }

    @Override
    public TMDBCreditCriteria copy() {
        return new TMDBCreditCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(StringFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public StringFilter getCharacter() {
        return character;
    }

    public void setCharacter(StringFilter character) {
        this.character = character;
    }

    public StringFilter getCreditType() {
        return creditType;
    }

    public void setCreditType(StringFilter creditType) {
        this.creditType = creditType;
    }

    public StringFilter getDepartment() {
        return department;
    }

    public void setDepartment(StringFilter department) {
        this.department = department;
    }

    public StringFilter getJob() {
        return job;
    }

    public void setJob(StringFilter job) {
        this.job = job;
    }

    public IntegerFilter getOrder() {
        return order;
    }

    public void setOrder(IntegerFilter order) {
        this.order = order;
    }

    public LongFilter getPersonId() {
        return personId;
    }

    public void setPersonId(LongFilter personId) {
        this.personId = personId;
    }

    public LongFilter getMovieId() {
        return movieId;
    }

    public void setMovieId(LongFilter movieId) {
        this.movieId = movieId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TMDBCreditCriteria that = (TMDBCreditCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(character, that.character) &&
            Objects.equals(creditType, that.creditType) &&
            Objects.equals(department, that.department) &&
            Objects.equals(job, that.job) &&
            Objects.equals(order, that.order) &&
            Objects.equals(personId, that.personId) &&
            Objects.equals(movieId, that.movieId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tmdbId,
        character,
        creditType,
        department,
        job,
        order,
        personId,
        movieId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBCreditCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (character != null ? "character=" + character + ", " : "") +
                (creditType != null ? "creditType=" + creditType + ", " : "") +
                (department != null ? "department=" + department + ", " : "") +
                (job != null ? "job=" + job + ", " : "") +
                (order != null ? "order=" + order + ", " : "") +
                (personId != null ? "personId=" + personId + ", " : "") +
                (movieId != null ? "movieId=" + movieId + ", " : "") +
            "}";
    }

}
