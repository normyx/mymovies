package com.mgoulene.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mgoulene.domain.MyMovie} entity.
 */
@ApiModel(description = "The Entity entity.\n@author A true hipster")
public class MyMovieDTO implements Serializable {
    
    private Long id;

    /**
     * fieldName
     */
    @Size(max = 4000)
    @ApiModelProperty(value = "fieldName")
    private String comments;

    @Min(value = 0)
    @Max(value = 5)
    private Integer vote;

    private LocalDate viewedDate;


    private Long movieId;

    private String movieTitle;

    private Long userId;

    private String userLogin;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public LocalDate getViewedDate() {
        return viewedDate;
    }

    public void setViewedDate(LocalDate viewedDate) {
        this.viewedDate = viewedDate;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long tMDBMovieId) {
        this.movieId = tMDBMovieId;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String tMDBMovieTitle) {
        this.movieTitle = tMDBMovieTitle;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MyMovieDTO)) {
            return false;
        }

        return id != null && id.equals(((MyMovieDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MyMovieDTO{" +
            "id=" + getId() +
            ", comments='" + getComments() + "'" +
            ", vote=" + getVote() +
            ", viewedDate='" + getViewedDate() + "'" +
            ", movieId=" + getMovieId() +
            ", movieTitle='" + getMovieTitle() + "'" +
            ", userId=" + getUserId() +
            ", userLogin='" + getUserLogin() + "'" +
            "}";
    }
}
