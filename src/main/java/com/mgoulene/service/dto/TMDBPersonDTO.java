package com.mgoulene.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mgoulene.domain.TMDBPerson} entity.
 */
@ApiModel(description = "The TMDBPerson entity.\n@author A true hipster")
public class TMDBPersonDTO implements Serializable {
    
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @ApiModelProperty(value = "fieldName", required = true)
    private Integer tmdbId;

    private LocalDate birthday;

    private LocalDate deathday;

    @Size(max = 200)
    private String name;

    @Size(max = 200)
    private String aka;

    private Integer gender;

    @Size(max = 4000)
    private String biography;

    @Size(max = 200)
    private String placeOfBirth;

    @Size(max = 200)
    private String homepage;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getDeathday() {
        return deathday;
    }

    public void setDeathday(LocalDate deathday) {
        this.deathday = deathday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAka() {
        return aka;
    }

    public void setAka(String aka) {
        this.aka = aka;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBPersonDTO)) {
            return false;
        }

        return id != null && id.equals(((TMDBPersonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBPersonDTO{" +
            "id=" + getId() +
            ", tmdbId=" + getTmdbId() +
            ", birthday='" + getBirthday() + "'" +
            ", deathday='" + getDeathday() + "'" +
            ", name='" + getName() + "'" +
            ", aka='" + getAka() + "'" +
            ", gender=" + getGender() +
            ", biography='" + getBiography() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", homepage='" + getHomepage() + "'" +
            "}";
    }
}
