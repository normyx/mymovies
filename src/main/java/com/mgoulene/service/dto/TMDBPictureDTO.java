package com.mgoulene.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.mgoulene.domain.TMDBPicture} entity.
 */
public class TMDBPictureDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String tmdbId;

    
    @Lob
    private byte[] picture;

    private String pictureContentType;
    @NotNull
    private String pictureSize;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public String getPictureSize() {
        return pictureSize;
    }

    public void setPictureSize(String pictureSize) {
        this.pictureSize = pictureSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBPictureDTO)) {
            return false;
        }

        return id != null && id.equals(((TMDBPictureDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBPictureDTO{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", picture='" + getPicture() + "'" +
            ", pictureSize='" + getPictureSize() + "'" +
            "}";
    }
}
