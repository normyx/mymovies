package com.mgoulene.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.mgoulene.domain.enumeration.TMDBMovieStatus;


/**
 * A DTO for the {@link com.mgoulene.domain.TMDBMovie} entity.
 */
@ApiModel(description = "The TMDBMovie entity.\n@author A true hipster")
public class TMDBMovieDTO implements Serializable {
    
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @ApiModelProperty(value = "fieldName", required = true)
    private Integer tmdbId;

    
    @Size(max = 200)
    private String title;

   
    private Boolean forAdult;

    private String homepage;

    
    private String originalLangage;

    private String originalTitle;

    @Size(max = 4000)
    private String overview;

    private String tagline;

    private TMDBMovieStatus status;

    @DecimalMin(value = "0")
    @DecimalMax(value = "10")
    private Float voteAverage;

    private Integer voteCount;

    private LocalDate releaseDate;

    private Set<TMDBGenreDTO> genres = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    
    public Boolean isForAdult() {
        return forAdult;
    }

    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    
    public String getOriginalLangage() {
        return originalLangage;
    }

    public void setOriginalLangage(String originalLangage) {
        this.originalLangage = originalLangage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public TMDBMovieStatus getStatus() {
        return status;
    }

    public void setStatus(TMDBMovieStatus status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Set<TMDBGenreDTO> getGenres() {
        return genres;
    }

    public void setGenres(Set<TMDBGenreDTO> tMDBGenres) {
        this.genres = tMDBGenres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBMovieDTO)) {
            return false;
        }

        return id != null && id.equals(((TMDBMovieDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBMovieDTO{" +
            "id=" + getId() +
            ", tmdbId=" + getTmdbId() +
            ", title='" + getTitle() + "'" +
            ", forAdult='" + isForAdult() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", originalLangage='" + getOriginalLangage() + "'" +
            ", originalTitle='" + getOriginalTitle() + "'" +
            ", overview='" + getOverview() + "'" +
            ", tagline='" + getTagline() + "'" +
            ", status='" + getStatus() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", releaseDate='" + getReleaseDate() + "'" +
            ", genres='" + getGenres() + "'" +
            "}";
    }
}
