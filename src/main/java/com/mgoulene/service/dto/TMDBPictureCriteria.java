package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.TMDBPicture} entity. This class is used
 * in {@link com.mgoulene.web.rest.TMDBPictureResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tmdb-pictures?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TMDBPictureCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tmdbId;

    private StringFilter pictureSize;

    public TMDBPictureCriteria() {
    }

    public TMDBPictureCriteria(TMDBPictureCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.pictureSize = other.pictureSize == null ? null : other.pictureSize.copy();
    }

    @Override
    public TMDBPictureCriteria copy() {
        return new TMDBPictureCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(StringFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public StringFilter getPictureSize() {
        return pictureSize;
    }

    public void setPictureSize(StringFilter pictureSize) {
        this.pictureSize = pictureSize;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TMDBPictureCriteria that = (TMDBPictureCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(pictureSize, that.pictureSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tmdbId,
        pictureSize
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBPictureCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (pictureSize != null ? "pictureSize=" + pictureSize + ", " : "") +
            "}";
    }

}
