package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.mgoulene.domain.enumeration.TMDBMovieStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.TMDBMovie} entity. This class is used
 * in {@link com.mgoulene.web.rest.TMDBMovieResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tmdb-movies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TMDBMovieCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TMDBMovieStatus
     */
    public static class TMDBMovieStatusFilter extends Filter<TMDBMovieStatus> {

        public TMDBMovieStatusFilter() {
        }

        public TMDBMovieStatusFilter(TMDBMovieStatusFilter filter) {
            super(filter);
        }

        @Override
        public TMDBMovieStatusFilter copy() {
            return new TMDBMovieStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter tmdbId;

    private StringFilter title;

    private BooleanFilter forAdult;

    private StringFilter homepage;

    private StringFilter originalLangage;

    private StringFilter originalTitle;

    private StringFilter overview;

    private StringFilter tagline;

    private TMDBMovieStatusFilter status;

    private FloatFilter voteAverage;

    private IntegerFilter voteCount;

    private LocalDateFilter releaseDate;

    private LongFilter creditsId;

    private LongFilter genreId;

    public TMDBMovieCriteria() {
    }

    public TMDBMovieCriteria(TMDBMovieCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.forAdult = other.forAdult == null ? null : other.forAdult.copy();
        this.homepage = other.homepage == null ? null : other.homepage.copy();
        this.originalLangage = other.originalLangage == null ? null : other.originalLangage.copy();
        this.originalTitle = other.originalTitle == null ? null : other.originalTitle.copy();
        this.overview = other.overview == null ? null : other.overview.copy();
        this.tagline = other.tagline == null ? null : other.tagline.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.voteAverage = other.voteAverage == null ? null : other.voteAverage.copy();
        this.voteCount = other.voteCount == null ? null : other.voteCount.copy();
        this.releaseDate = other.releaseDate == null ? null : other.releaseDate.copy();
        this.creditsId = other.creditsId == null ? null : other.creditsId.copy();
        this.genreId = other.genreId == null ? null : other.genreId.copy();
    }

    @Override
    public TMDBMovieCriteria copy() {
        return new TMDBMovieCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(IntegerFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public BooleanFilter getForAdult() {
        return forAdult;
    }

    public void setForAdult(BooleanFilter forAdult) {
        this.forAdult = forAdult;
    }

    public StringFilter getHomepage() {
        return homepage;
    }

    public void setHomepage(StringFilter homepage) {
        this.homepage = homepage;
    }

    public StringFilter getOriginalLangage() {
        return originalLangage;
    }

    public void setOriginalLangage(StringFilter originalLangage) {
        this.originalLangage = originalLangage;
    }

    public StringFilter getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(StringFilter originalTitle) {
        this.originalTitle = originalTitle;
    }

    public StringFilter getOverview() {
        return overview;
    }

    public void setOverview(StringFilter overview) {
        this.overview = overview;
    }

    public StringFilter getTagline() {
        return tagline;
    }

    public void setTagline(StringFilter tagline) {
        this.tagline = tagline;
    }

    public TMDBMovieStatusFilter getStatus() {
        return status;
    }

    public void setStatus(TMDBMovieStatusFilter status) {
        this.status = status;
    }

    public FloatFilter getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(FloatFilter voteAverage) {
        this.voteAverage = voteAverage;
    }

    public IntegerFilter getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(IntegerFilter voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDateFilter getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateFilter releaseDate) {
        this.releaseDate = releaseDate;
    }

    public LongFilter getCreditsId() {
        return creditsId;
    }

    public void setCreditsId(LongFilter creditsId) {
        this.creditsId = creditsId;
    }

    public LongFilter getGenreId() {
        return genreId;
    }

    public void setGenreId(LongFilter genreId) {
        this.genreId = genreId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TMDBMovieCriteria that = (TMDBMovieCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(title, that.title) &&
            Objects.equals(forAdult, that.forAdult) &&
            Objects.equals(homepage, that.homepage) &&
            Objects.equals(originalLangage, that.originalLangage) &&
            Objects.equals(originalTitle, that.originalTitle) &&
            Objects.equals(overview, that.overview) &&
            Objects.equals(tagline, that.tagline) &&
            Objects.equals(status, that.status) &&
            Objects.equals(voteAverage, that.voteAverage) &&
            Objects.equals(voteCount, that.voteCount) &&
            Objects.equals(releaseDate, that.releaseDate) &&
            Objects.equals(creditsId, that.creditsId) &&
            Objects.equals(genreId, that.genreId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tmdbId,
        title,
        forAdult,
        homepage,
        originalLangage,
        originalTitle,
        overview,
        tagline,
        status,
        voteAverage,
        voteCount,
        releaseDate,
        creditsId,
        genreId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBMovieCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (forAdult != null ? "forAdult=" + forAdult + ", " : "") +
                (homepage != null ? "homepage=" + homepage + ", " : "") +
                (originalLangage != null ? "originalLangage=" + originalLangage + ", " : "") +
                (originalTitle != null ? "originalTitle=" + originalTitle + ", " : "") +
                (overview != null ? "overview=" + overview + ", " : "") +
                (tagline != null ? "tagline=" + tagline + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (voteAverage != null ? "voteAverage=" + voteAverage + ", " : "") +
                (voteCount != null ? "voteCount=" + voteCount + ", " : "") +
                (releaseDate != null ? "releaseDate=" + releaseDate + ", " : "") +
                (creditsId != null ? "creditsId=" + creditsId + ", " : "") +
                (genreId != null ? "genreId=" + genreId + ", " : "") +
            "}";
    }

}
