package com.mgoulene.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mgoulene.domain.TMDBCredit} entity.
 */
@ApiModel(description = "The TMDBCredit entity.\n@author A true hipster")
public class TMDBCreditDTO implements Serializable {
    
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @ApiModelProperty(value = "fieldName", required = true)
    private String tmdbId;

    @Size(max = 200)
    private String character;

    private String creditType;

    private String department;

    private String job;

    private Integer order;


    private Long personId;

    private String personName;

    private Long movieId;

    private String movieTitle;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long tMDBPersonId) {
        this.personId = tMDBPersonId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String tMDBPersonName) {
        this.personName = tMDBPersonName;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long tMDBMovieId) {
        this.movieId = tMDBMovieId;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String tMDBMovieTitle) {
        this.movieTitle = tMDBMovieTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBCreditDTO)) {
            return false;
        }

        return id != null && id.equals(((TMDBCreditDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBCreditDTO{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", character='" + getCharacter() + "'" +
            ", creditType='" + getCreditType() + "'" +
            ", department='" + getDepartment() + "'" +
            ", job='" + getJob() + "'" +
            ", order=" + getOrder() +
            ", personId=" + getPersonId() +
            ", personName='" + getPersonName() + "'" +
            ", movieId=" + getMovieId() +
            ", movieTitle='" + getMovieTitle() + "'" +
            "}";
    }
}
