package com.mgoulene.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mgoulene.tmdb.service.dto.TMDBMovieAPIDTO;

public class Response {

  @JsonProperty
  private TMDBMovieAPIDTO[] results;
  
  public TMDBMovieAPIDTO[] getResults() {
    return results;
  }

  public void setResults(TMDBMovieAPIDTO[] results) {
    this.results = results;
  }

  public Response(){
    
  }

  @Override
  public String toString() {
      String res = "Response [results=\n";
      for (int i = 0; i < results.length; i++ ) {
          res += results[i]+"\n";
      }
      res += "]";
    return  res;
  }

}