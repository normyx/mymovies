package com.mgoulene.service;

import com.mgoulene.domain.TMDBCredit;
import com.mgoulene.repository.TMDBCreditRepository;
import com.mgoulene.repository.search.TMDBCreditSearchRepository;
import com.mgoulene.service.dto.TMDBCreditDTO;
import com.mgoulene.service.mapper.TMDBCreditMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link TMDBCredit}.
 */
@Service
@Transactional
public class TMDBCreditService {

    private final Logger log = LoggerFactory.getLogger(TMDBCreditService.class);

    private final TMDBCreditRepository tMDBCreditRepository;

    private final TMDBCreditMapper tMDBCreditMapper;

    private final TMDBCreditSearchRepository tMDBCreditSearchRepository;

    public TMDBCreditService(TMDBCreditRepository tMDBCreditRepository, TMDBCreditMapper tMDBCreditMapper, TMDBCreditSearchRepository tMDBCreditSearchRepository) {
        this.tMDBCreditRepository = tMDBCreditRepository;
        this.tMDBCreditMapper = tMDBCreditMapper;
        this.tMDBCreditSearchRepository = tMDBCreditSearchRepository;
    }

    /**
     * Save a tMDBCredit.
     *
     * @param tMDBCreditDTO the entity to save.
     * @return the persisted entity.
     */
    public TMDBCreditDTO save(TMDBCreditDTO tMDBCreditDTO) {
        log.debug("Request to save TMDBCredit : {}", tMDBCreditDTO);
        TMDBCredit tMDBCredit = tMDBCreditMapper.toEntity(tMDBCreditDTO);
        tMDBCredit = tMDBCreditRepository.save(tMDBCredit);
        TMDBCreditDTO result = tMDBCreditMapper.toDto(tMDBCredit);
        tMDBCreditSearchRepository.save(tMDBCredit);
        return result;
    }

    /**
     * Get all the tMDBCredits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBCreditDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TMDBCredits");
        return tMDBCreditRepository.findAll(pageable)
            .map(tMDBCreditMapper::toDto);
    }


    /**
     * Get one tMDBCredit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TMDBCreditDTO> findOne(Long id) {
        log.debug("Request to get TMDBCredit : {}", id);
        return tMDBCreditRepository.findById(id)
            .map(tMDBCreditMapper::toDto);
    }

    /**
     * Delete the tMDBCredit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TMDBCredit : {}", id);
        tMDBCreditRepository.deleteById(id);
        tMDBCreditSearchRepository.deleteById(id);
    }

    /**
     * Search for the tMDBCredit corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBCreditDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TMDBCredits for query {}", query);
        return tMDBCreditSearchRepository.search(queryStringQuery(query), pageable)
            .map(tMDBCreditMapper::toDto);
    }
}
