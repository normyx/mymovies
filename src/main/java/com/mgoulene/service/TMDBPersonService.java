package com.mgoulene.service;

import com.mgoulene.domain.TMDBPerson;
import com.mgoulene.repository.TMDBPersonRepository;
import com.mgoulene.repository.search.TMDBPersonSearchRepository;
import com.mgoulene.service.dto.TMDBPersonDTO;
import com.mgoulene.service.mapper.TMDBPersonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link TMDBPerson}.
 */
@Service
@Transactional
public class TMDBPersonService {

    private final Logger log = LoggerFactory.getLogger(TMDBPersonService.class);

    private final TMDBPersonRepository tMDBPersonRepository;

    private final TMDBPersonMapper tMDBPersonMapper;

    private final TMDBPersonSearchRepository tMDBPersonSearchRepository;

    public TMDBPersonService(TMDBPersonRepository tMDBPersonRepository, TMDBPersonMapper tMDBPersonMapper, TMDBPersonSearchRepository tMDBPersonSearchRepository) {
        this.tMDBPersonRepository = tMDBPersonRepository;
        this.tMDBPersonMapper = tMDBPersonMapper;
        this.tMDBPersonSearchRepository = tMDBPersonSearchRepository;
    }

    /**
     * Save a tMDBPerson.
     *
     * @param tMDBPersonDTO the entity to save.
     * @return the persisted entity.
     */
    public TMDBPersonDTO save(TMDBPersonDTO tMDBPersonDTO) {
        log.debug("Request to save TMDBPerson : {}", tMDBPersonDTO);
        TMDBPerson tMDBPerson = tMDBPersonMapper.toEntity(tMDBPersonDTO);
        tMDBPerson = tMDBPersonRepository.save(tMDBPerson);
        TMDBPersonDTO result = tMDBPersonMapper.toDto(tMDBPerson);
        tMDBPersonSearchRepository.save(tMDBPerson);
        return result;
    }

    /**
     * Get all the tMDBPeople.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBPersonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TMDBPeople");
        return tMDBPersonRepository.findAll(pageable)
            .map(tMDBPersonMapper::toDto);
    }


    /**
     * Get one tMDBPerson by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TMDBPersonDTO> findOne(Long id) {
        log.debug("Request to get TMDBPerson : {}", id);
        return tMDBPersonRepository.findById(id)
            .map(tMDBPersonMapper::toDto);
    }

    /**
     * Delete the tMDBPerson by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TMDBPerson : {}", id);
        tMDBPersonRepository.deleteById(id);
        tMDBPersonSearchRepository.deleteById(id);
    }

    /**
     * Search for the tMDBPerson corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBPersonDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TMDBPeople for query {}", query);
        return tMDBPersonSearchRepository.search(queryStringQuery(query), pageable)
            .map(tMDBPersonMapper::toDto);
    }
}
