package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.TMDBPicture;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.TMDBPictureRepository;
import com.mgoulene.repository.search.TMDBPictureSearchRepository;
import com.mgoulene.service.dto.TMDBPictureCriteria;
import com.mgoulene.service.dto.TMDBPictureDTO;
import com.mgoulene.service.mapper.TMDBPictureMapper;

/**
 * Service for executing complex queries for {@link TMDBPicture} entities in the database.
 * The main input is a {@link TMDBPictureCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TMDBPictureDTO} or a {@link Page} of {@link TMDBPictureDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TMDBPictureQueryService extends QueryService<TMDBPicture> {

    private final Logger log = LoggerFactory.getLogger(TMDBPictureQueryService.class);

    private final TMDBPictureRepository tMDBPictureRepository;

    private final TMDBPictureMapper tMDBPictureMapper;

    private final TMDBPictureSearchRepository tMDBPictureSearchRepository;

    public TMDBPictureQueryService(TMDBPictureRepository tMDBPictureRepository, TMDBPictureMapper tMDBPictureMapper, TMDBPictureSearchRepository tMDBPictureSearchRepository) {
        this.tMDBPictureRepository = tMDBPictureRepository;
        this.tMDBPictureMapper = tMDBPictureMapper;
        this.tMDBPictureSearchRepository = tMDBPictureSearchRepository;
    }

    /**
     * Return a {@link List} of {@link TMDBPictureDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TMDBPictureDTO> findByCriteria(TMDBPictureCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TMDBPicture> specification = createSpecification(criteria);
        return tMDBPictureMapper.toDto(tMDBPictureRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TMDBPictureDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBPictureDTO> findByCriteria(TMDBPictureCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TMDBPicture> specification = createSpecification(criteria);
        return tMDBPictureRepository.findAll(specification, page)
            .map(tMDBPictureMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TMDBPictureCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TMDBPicture> specification = createSpecification(criteria);
        return tMDBPictureRepository.count(specification);
    }

    /**
     * Function to convert {@link TMDBPictureCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TMDBPicture> createSpecification(TMDBPictureCriteria criteria) {
        Specification<TMDBPicture> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TMDBPicture_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTmdbId(), TMDBPicture_.tmdbId));
            }
            if (criteria.getPictureSize() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPictureSize(), TMDBPicture_.pictureSize));
            }
        }
        return specification;
    }
}
