package com.mgoulene.service;

import com.mgoulene.domain.MyMovie;
import com.mgoulene.repository.MyMovieRepository;
import com.mgoulene.repository.search.MyMovieSearchRepository;
import com.mgoulene.service.dto.MyMovieDTO;
import com.mgoulene.service.mapper.MyMovieMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link MyMovie}.
 */
@Service
@Transactional
public class MyMovieService {

    private final Logger log = LoggerFactory.getLogger(MyMovieService.class);

    private final MyMovieRepository myMovieRepository;

    private final MyMovieMapper myMovieMapper;

    private final MyMovieSearchRepository myMovieSearchRepository;

    public MyMovieService(MyMovieRepository myMovieRepository, MyMovieMapper myMovieMapper, MyMovieSearchRepository myMovieSearchRepository) {
        this.myMovieRepository = myMovieRepository;
        this.myMovieMapper = myMovieMapper;
        this.myMovieSearchRepository = myMovieSearchRepository;
    }

    /**
     * Save a myMovie.
     *
     * @param myMovieDTO the entity to save.
     * @return the persisted entity.
     */
    public MyMovieDTO save(MyMovieDTO myMovieDTO) {
        log.debug("Request to save MyMovie : {}", myMovieDTO);
        MyMovie myMovie = myMovieMapper.toEntity(myMovieDTO);
        myMovie = myMovieRepository.save(myMovie);
        MyMovieDTO result = myMovieMapper.toDto(myMovie);
        myMovieSearchRepository.save(myMovie);
        return result;
    }

    /**
     * Get all the myMovies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MyMovieDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MyMovies");
        return myMovieRepository.findAll(pageable)
            .map(myMovieMapper::toDto);
    }


    /**
     * Get one myMovie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MyMovieDTO> findOne(Long id) {
        log.debug("Request to get MyMovie : {}", id);
        return myMovieRepository.findById(id)
            .map(myMovieMapper::toDto);
    }

    /**
     * Delete the myMovie by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MyMovie : {}", id);
        myMovieRepository.deleteById(id);
        myMovieSearchRepository.deleteById(id);
    }

    /**
     * Search for the myMovie corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MyMovieDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MyMovies for query {}", query);
        return myMovieSearchRepository.search(queryStringQuery(query), pageable)
            .map(myMovieMapper::toDto);
    }
}
