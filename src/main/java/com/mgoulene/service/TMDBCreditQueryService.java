package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.TMDBCredit;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.TMDBCreditRepository;
import com.mgoulene.repository.search.TMDBCreditSearchRepository;
import com.mgoulene.service.dto.TMDBCreditCriteria;
import com.mgoulene.service.dto.TMDBCreditDTO;
import com.mgoulene.service.mapper.TMDBCreditMapper;

/**
 * Service for executing complex queries for {@link TMDBCredit} entities in the database.
 * The main input is a {@link TMDBCreditCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TMDBCreditDTO} or a {@link Page} of {@link TMDBCreditDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TMDBCreditQueryService extends QueryService<TMDBCredit> {

    private final Logger log = LoggerFactory.getLogger(TMDBCreditQueryService.class);

    private final TMDBCreditRepository tMDBCreditRepository;

    private final TMDBCreditMapper tMDBCreditMapper;

    private final TMDBCreditSearchRepository tMDBCreditSearchRepository;

    public TMDBCreditQueryService(TMDBCreditRepository tMDBCreditRepository, TMDBCreditMapper tMDBCreditMapper, TMDBCreditSearchRepository tMDBCreditSearchRepository) {
        this.tMDBCreditRepository = tMDBCreditRepository;
        this.tMDBCreditMapper = tMDBCreditMapper;
        this.tMDBCreditSearchRepository = tMDBCreditSearchRepository;
    }

    /**
     * Return a {@link List} of {@link TMDBCreditDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TMDBCreditDTO> findByCriteria(TMDBCreditCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TMDBCredit> specification = createSpecification(criteria);
        return tMDBCreditMapper.toDto(tMDBCreditRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TMDBCreditDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBCreditDTO> findByCriteria(TMDBCreditCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TMDBCredit> specification = createSpecification(criteria);
        return tMDBCreditRepository.findAll(specification, page)
            .map(tMDBCreditMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TMDBCreditCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TMDBCredit> specification = createSpecification(criteria);
        return tMDBCreditRepository.count(specification);
    }

    /**
     * Function to convert {@link TMDBCreditCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TMDBCredit> createSpecification(TMDBCreditCriteria criteria) {
        Specification<TMDBCredit> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TMDBCredit_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTmdbId(), TMDBCredit_.tmdbId));
            }
            if (criteria.getCharacter() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCharacter(), TMDBCredit_.character));
            }
            if (criteria.getCreditType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreditType(), TMDBCredit_.creditType));
            }
            if (criteria.getDepartment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDepartment(), TMDBCredit_.department));
            }
            if (criteria.getJob() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJob(), TMDBCredit_.job));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), TMDBCredit_.order));
            }
            if (criteria.getPersonId() != null) {
                specification = specification.and(buildSpecification(criteria.getPersonId(),
                    root -> root.join(TMDBCredit_.person, JoinType.LEFT).get(TMDBPerson_.id)));
            }
            if (criteria.getMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getMovieId(),
                    root -> root.join(TMDBCredit_.movie, JoinType.LEFT).get(TMDBMovie_.id)));
            }
        }
        return specification;
    }
}
