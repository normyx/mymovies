package com.mgoulene.service;

import com.mgoulene.domain.TMDBGenre;
import com.mgoulene.repository.TMDBGenreRepository;
import com.mgoulene.repository.search.TMDBGenreSearchRepository;
import com.mgoulene.service.dto.TMDBGenreDTO;
import com.mgoulene.service.mapper.TMDBGenreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link TMDBGenre}.
 */
@Service
@Transactional
public class TMDBGenreService {

    private final Logger log = LoggerFactory.getLogger(TMDBGenreService.class);

    private final TMDBGenreRepository tMDBGenreRepository;

    private final TMDBGenreMapper tMDBGenreMapper;

    private final TMDBGenreSearchRepository tMDBGenreSearchRepository;

    public TMDBGenreService(TMDBGenreRepository tMDBGenreRepository, TMDBGenreMapper tMDBGenreMapper, TMDBGenreSearchRepository tMDBGenreSearchRepository) {
        this.tMDBGenreRepository = tMDBGenreRepository;
        this.tMDBGenreMapper = tMDBGenreMapper;
        this.tMDBGenreSearchRepository = tMDBGenreSearchRepository;
    }

    /**
     * Save a tMDBGenre.
     *
     * @param tMDBGenreDTO the entity to save.
     * @return the persisted entity.
     */
    public TMDBGenreDTO save(TMDBGenreDTO tMDBGenreDTO) {
        log.debug("Request to save TMDBGenre : {}", tMDBGenreDTO);
        TMDBGenre tMDBGenre = tMDBGenreMapper.toEntity(tMDBGenreDTO);
        tMDBGenre = tMDBGenreRepository.save(tMDBGenre);
        TMDBGenreDTO result = tMDBGenreMapper.toDto(tMDBGenre);
        tMDBGenreSearchRepository.save(tMDBGenre);
        return result;
    }

    /**
     * Get all the tMDBGenres.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBGenreDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TMDBGenres");
        return tMDBGenreRepository.findAll(pageable)
            .map(tMDBGenreMapper::toDto);
    }


    /**
     * Get one tMDBGenre by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TMDBGenreDTO> findOne(Long id) {
        log.debug("Request to get TMDBGenre : {}", id);
        return tMDBGenreRepository.findById(id)
            .map(tMDBGenreMapper::toDto);
    }

    /**
     * Delete the tMDBGenre by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TMDBGenre : {}", id);
        tMDBGenreRepository.deleteById(id);
        tMDBGenreSearchRepository.deleteById(id);
    }

    /**
     * Search for the tMDBGenre corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TMDBGenreDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TMDBGenres for query {}", query);
        return tMDBGenreSearchRepository.search(queryStringQuery(query), pageable)
            .map(tMDBGenreMapper::toDto);
    }
}
