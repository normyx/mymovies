package com.mgoulene.repository;

import com.mgoulene.domain.TMDBMovie;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TMDBMovie entity.
 */
@Repository
public interface TMDBMovieRepository extends JpaRepository<TMDBMovie, Long>, JpaSpecificationExecutor<TMDBMovie> {

    @Query(value = "select distinct tMDBMovie from TMDBMovie tMDBMovie left join fetch tMDBMovie.genres",
        countQuery = "select count(distinct tMDBMovie) from TMDBMovie tMDBMovie")
    Page<TMDBMovie> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct tMDBMovie from TMDBMovie tMDBMovie left join fetch tMDBMovie.genres")
    List<TMDBMovie> findAllWithEagerRelationships();

    @Query("select tMDBMovie from TMDBMovie tMDBMovie left join fetch tMDBMovie.genres where tMDBMovie.id =:id")
    Optional<TMDBMovie> findOneWithEagerRelationships(@Param("id") Long id);
}
