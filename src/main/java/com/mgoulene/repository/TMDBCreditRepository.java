package com.mgoulene.repository;

import com.mgoulene.domain.TMDBCredit;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TMDBCredit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TMDBCreditRepository extends JpaRepository<TMDBCredit, Long>, JpaSpecificationExecutor<TMDBCredit> {
}
