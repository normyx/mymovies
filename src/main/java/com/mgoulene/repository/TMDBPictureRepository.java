package com.mgoulene.repository;

import com.mgoulene.domain.TMDBPicture;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TMDBPicture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TMDBPictureRepository extends JpaRepository<TMDBPicture, Long>, JpaSpecificationExecutor<TMDBPicture> {
}
