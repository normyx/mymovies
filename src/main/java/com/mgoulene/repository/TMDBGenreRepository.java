package com.mgoulene.repository;

import com.mgoulene.domain.TMDBGenre;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TMDBGenre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TMDBGenreRepository extends JpaRepository<TMDBGenre, Long>, JpaSpecificationExecutor<TMDBGenre> {
}
