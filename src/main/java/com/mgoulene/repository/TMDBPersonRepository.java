package com.mgoulene.repository;

import com.mgoulene.domain.TMDBPerson;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TMDBPerson entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TMDBPersonRepository extends JpaRepository<TMDBPerson, Long>, JpaSpecificationExecutor<TMDBPerson> {
}
