package com.mgoulene.repository.search;

import com.mgoulene.domain.TMDBMovie;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link TMDBMovie} entity.
 */
public interface TMDBMovieSearchRepository extends ElasticsearchRepository<TMDBMovie, Long> {
}
