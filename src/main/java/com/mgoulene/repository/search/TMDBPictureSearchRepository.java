package com.mgoulene.repository.search;

import com.mgoulene.domain.TMDBPicture;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link TMDBPicture} entity.
 */
public interface TMDBPictureSearchRepository extends ElasticsearchRepository<TMDBPicture, Long> {
}
