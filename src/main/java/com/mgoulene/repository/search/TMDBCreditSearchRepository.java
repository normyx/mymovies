package com.mgoulene.repository.search;

import com.mgoulene.domain.TMDBCredit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link TMDBCredit} entity.
 */
public interface TMDBCreditSearchRepository extends ElasticsearchRepository<TMDBCredit, Long> {
}
