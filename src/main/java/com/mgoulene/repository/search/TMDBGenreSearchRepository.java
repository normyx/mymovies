package com.mgoulene.repository.search;

import com.mgoulene.domain.TMDBGenre;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link TMDBGenre} entity.
 */
public interface TMDBGenreSearchRepository extends ElasticsearchRepository<TMDBGenre, Long> {
}
