package com.mgoulene.repository.search;

import com.mgoulene.domain.MyMovie;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link MyMovie} entity.
 */
public interface MyMovieSearchRepository extends ElasticsearchRepository<MyMovie, Long> {
}
