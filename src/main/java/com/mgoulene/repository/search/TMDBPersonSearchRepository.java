package com.mgoulene.repository.search;

import com.mgoulene.domain.TMDBPerson;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link TMDBPerson} entity.
 */
public interface TMDBPersonSearchRepository extends ElasticsearchRepository<TMDBPerson, Long> {
}
