package com.mgoulene.tmdb.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mgoulene.domain.enumeration.TMDBMovieStatus;


/**
 * A DTO for the {@link com.mgoulene.domain.TMDBMovie} entity.
 */
@ApiModel(description = "The TMDBMovie entity.\n@author A true hipster")
public class TMDBMovieAPIDTO implements Serializable {
    
    private Long id;

    private String title;

    @JsonProperty("adult")
    private Boolean forAdult;

    private String homepage;
    
    @JsonProperty("original_language")
    private String originalLangage;

    private String originalTitle;

    private String overview;

    private String tagline;

    private TMDBMovieStatus status;

    private Float voteAverage;

    private Integer voteCount;

    private LocalDate releaseDate;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    
    public Boolean isForAdult() {
        return forAdult;
    }

    
    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    
    public String getOriginalLangage() {
        return originalLangage;
    }

    
    public void setOriginalLangage(String originalLangage) {
        this.originalLangage = originalLangage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    
    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public TMDBMovieStatus getStatus() {
        return status;
    }

    public void setStatus(TMDBMovieStatus status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    
    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    
    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    
    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBMovieAPIDTO)) {
            return false;
        }

        return id != null && id.equals(((TMDBMovieAPIDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBMovieDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", forAdult='" + isForAdult() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", originalLangage='" + getOriginalLangage() + "'" +
            ", originalTitle='" + getOriginalTitle() + "'" +
            ", overview='" + getOverview() + "'" +
            ", tagline='" + getTagline() + "'" +
            ", status='" + getStatus() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", releaseDate='" + getReleaseDate() + "'" +
            "}";
    }
}
