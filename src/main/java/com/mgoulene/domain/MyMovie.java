package com.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Entity entity.\n@author A true hipster
 */
@Entity
@Table(name = "my_movie")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "mymovie")
public class MyMovie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @Size(max = 4000)
    @Column(name = "comments", length = 4000)
    private String comments;

    @Min(value = 0)
    @Max(value = 5)
    @Column(name = "vote")
    private Integer vote;

    @Column(name = "viewed_date")
    private LocalDate viewedDate;

    @ManyToOne
    @JsonIgnoreProperties(value = "myMovies", allowSetters = true)
    private TMDBMovie movie;

    @ManyToOne
    @JsonIgnoreProperties(value = "myMovies", allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public MyMovie comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getVote() {
        return vote;
    }

    public MyMovie vote(Integer vote) {
        this.vote = vote;
        return this;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public LocalDate getViewedDate() {
        return viewedDate;
    }

    public MyMovie viewedDate(LocalDate viewedDate) {
        this.viewedDate = viewedDate;
        return this;
    }

    public void setViewedDate(LocalDate viewedDate) {
        this.viewedDate = viewedDate;
    }

    public TMDBMovie getMovie() {
        return movie;
    }

    public MyMovie movie(TMDBMovie tMDBMovie) {
        this.movie = tMDBMovie;
        return this;
    }

    public void setMovie(TMDBMovie tMDBMovie) {
        this.movie = tMDBMovie;
    }

    public User getUser() {
        return user;
    }

    public MyMovie user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MyMovie)) {
            return false;
        }
        return id != null && id.equals(((MyMovie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MyMovie{" +
            "id=" + getId() +
            ", comments='" + getComments() + "'" +
            ", vote=" + getVote() +
            ", viewedDate='" + getViewedDate() + "'" +
            "}";
    }
}
