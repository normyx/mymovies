package com.mgoulene.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.mgoulene.domain.enumeration.TMDBMovieStatus;

/**
 * The TMDBMovie entity.\n@author A true hipster
 */
@Entity
@Table(name = "tmdb_movie")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "tmdbmovie")
public class TMDBMovie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Column(name = "tmdb_id", nullable = false)
    private Integer tmdbId;

    @Size(max = 200)
    @Column(name = "title", length = 200)
    private String title;

    @Column(name = "for_adult")
    private Boolean forAdult;

    @Column(name = "homepage")
    private String homepage;

    @Column(name = "original_langage")
    private String originalLangage;

    @Column(name = "original_title")
    private String originalTitle;

    @Size(max = 4000)
    @Column(name = "overview", length = 4000)
    private String overview;

    @Column(name = "tagline")
    private String tagline;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TMDBMovieStatus status;

    @DecimalMin(value = "0")
    @DecimalMax(value = "10")
    @Column(name = "vote_average")
    private Float voteAverage;

    @Column(name = "vote_count")
    private Integer voteCount;

    @Column(name = "release_date")
    private LocalDate releaseDate;

    @OneToMany(mappedBy = "movie")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<TMDBCredit> credits = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "tmdb_movie_genre",
               joinColumns = @JoinColumn(name = "tmdbmovie_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "id"))
    private Set<TMDBGenre> genres = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTmdbId() {
        return tmdbId;
    }

    public TMDBMovie tmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getTitle() {
        return title;
    }

    public TMDBMovie title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isForAdult() {
        return forAdult;
    }

    public TMDBMovie forAdult(Boolean forAdult) {
        this.forAdult = forAdult;
        return this;
    }

    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return homepage;
    }

    public TMDBMovie homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOriginalLangage() {
        return originalLangage;
    }

    public TMDBMovie originalLangage(String originalLangage) {
        this.originalLangage = originalLangage;
        return this;
    }

    public void setOriginalLangage(String originalLangage) {
        this.originalLangage = originalLangage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public TMDBMovie originalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
        return this;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public TMDBMovie overview(String overview) {
        this.overview = overview;
        return this;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public TMDBMovie tagline(String tagline) {
        this.tagline = tagline;
        return this;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public TMDBMovieStatus getStatus() {
        return status;
    }

    public TMDBMovie status(TMDBMovieStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(TMDBMovieStatus status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public TMDBMovie voteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public TMDBMovie voteCount(Integer voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public TMDBMovie releaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Set<TMDBCredit> getCredits() {
        return credits;
    }

    public TMDBMovie credits(Set<TMDBCredit> tMDBCredits) {
        this.credits = tMDBCredits;
        return this;
    }

    public TMDBMovie addCredits(TMDBCredit tMDBCredit) {
        this.credits.add(tMDBCredit);
        tMDBCredit.setMovie(this);
        return this;
    }

    public TMDBMovie removeCredits(TMDBCredit tMDBCredit) {
        this.credits.remove(tMDBCredit);
        tMDBCredit.setMovie(null);
        return this;
    }

    public void setCredits(Set<TMDBCredit> tMDBCredits) {
        this.credits = tMDBCredits;
    }

    public Set<TMDBGenre> getGenres() {
        return genres;
    }

    public TMDBMovie genres(Set<TMDBGenre> tMDBGenres) {
        this.genres = tMDBGenres;
        return this;
    }

    public TMDBMovie addGenre(TMDBGenre tMDBGenre) {
        this.genres.add(tMDBGenre);
        tMDBGenre.getMovies().add(this);
        return this;
    }

    public TMDBMovie removeGenre(TMDBGenre tMDBGenre) {
        this.genres.remove(tMDBGenre);
        tMDBGenre.getMovies().remove(this);
        return this;
    }

    public void setGenres(Set<TMDBGenre> tMDBGenres) {
        this.genres = tMDBGenres;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBMovie)) {
            return false;
        }
        return id != null && id.equals(((TMDBMovie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBMovie{" +
            "id=" + getId() +
            ", tmdbId=" + getTmdbId() +
            ", title='" + getTitle() + "'" +
            ", forAdult='" + isForAdult() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", originalLangage='" + getOriginalLangage() + "'" +
            ", originalTitle='" + getOriginalTitle() + "'" +
            ", overview='" + getOverview() + "'" +
            ", tagline='" + getTagline() + "'" +
            ", status='" + getStatus() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", releaseDate='" + getReleaseDate() + "'" +
            "}";
    }
}
