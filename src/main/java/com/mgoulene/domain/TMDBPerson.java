package com.mgoulene.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * The TMDBPerson entity.\n@author A true hipster
 */
@Entity
@Table(name = "tmdb_person")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "tmdbperson")
public class TMDBPerson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Column(name = "tmdb_id", nullable = false)
    private Integer tmdbId;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "deathday")
    private LocalDate deathday;

    @Size(max = 200)
    @Column(name = "name", length = 200)
    private String name;

    @Size(max = 200)
    @Column(name = "aka", length = 200)
    private String aka;

    @Column(name = "gender")
    private Integer gender;

    @Size(max = 4000)
    @Column(name = "biography", length = 4000)
    private String biography;

    @Size(max = 200)
    @Column(name = "place_of_birth", length = 200)
    private String placeOfBirth;

    @Size(max = 200)
    @Column(name = "homepage", length = 200)
    private String homepage;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTmdbId() {
        return tmdbId;
    }

    public TMDBPerson tmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public TMDBPerson birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getDeathday() {
        return deathday;
    }

    public TMDBPerson deathday(LocalDate deathday) {
        this.deathday = deathday;
        return this;
    }

    public void setDeathday(LocalDate deathday) {
        this.deathday = deathday;
    }

    public String getName() {
        return name;
    }

    public TMDBPerson name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAka() {
        return aka;
    }

    public TMDBPerson aka(String aka) {
        this.aka = aka;
        return this;
    }

    public void setAka(String aka) {
        this.aka = aka;
    }

    public Integer getGender() {
        return gender;
    }

    public TMDBPerson gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public TMDBPerson biography(String biography) {
        this.biography = biography;
        return this;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public TMDBPerson placeOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getHomepage() {
        return homepage;
    }

    public TMDBPerson homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBPerson)) {
            return false;
        }
        return id != null && id.equals(((TMDBPerson) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBPerson{" +
            "id=" + getId() +
            ", tmdbId=" + getTmdbId() +
            ", birthday='" + getBirthday() + "'" +
            ", deathday='" + getDeathday() + "'" +
            ", name='" + getName() + "'" +
            ", aka='" + getAka() + "'" +
            ", gender=" + getGender() +
            ", biography='" + getBiography() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", homepage='" + getHomepage() + "'" +
            "}";
    }
}
