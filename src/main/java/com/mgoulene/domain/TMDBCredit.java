package com.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * The TMDBCredit entity.\n@author A true hipster
 */
@Entity
@Table(name = "tmdb_credit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "tmdbcredit")
public class TMDBCredit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Column(name = "tmdb_id", nullable = false)
    private String tmdbId;

    @Size(max = 200)
    @Column(name = "character", length = 200)
    private String character;

    @Column(name = "credit_type")
    private String creditType;

    @Column(name = "department")
    private String department;

    @Column(name = "job")
    private String job;

    @Column(name = "jhi_order")
    private Integer order;

    @ManyToOne
    @JsonIgnoreProperties(value = "tMDBCredits", allowSetters = true)
    private TMDBPerson person;

    @ManyToOne
    @JsonIgnoreProperties(value = "credits", allowSetters = true)
    private TMDBMovie movie;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public TMDBCredit tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getCharacter() {
        return character;
    }

    public TMDBCredit character(String character) {
        this.character = character;
        return this;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCreditType() {
        return creditType;
    }

    public TMDBCredit creditType(String creditType) {
        this.creditType = creditType;
        return this;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getDepartment() {
        return department;
    }

    public TMDBCredit department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public TMDBCredit job(String job) {
        this.job = job;
        return this;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer getOrder() {
        return order;
    }

    public TMDBCredit order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public TMDBPerson getPerson() {
        return person;
    }

    public TMDBCredit person(TMDBPerson tMDBPerson) {
        this.person = tMDBPerson;
        return this;
    }

    public void setPerson(TMDBPerson tMDBPerson) {
        this.person = tMDBPerson;
    }

    public TMDBMovie getMovie() {
        return movie;
    }

    public TMDBCredit movie(TMDBMovie tMDBMovie) {
        this.movie = tMDBMovie;
        return this;
    }

    public void setMovie(TMDBMovie tMDBMovie) {
        this.movie = tMDBMovie;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBCredit)) {
            return false;
        }
        return id != null && id.equals(((TMDBCredit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBCredit{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", character='" + getCharacter() + "'" +
            ", creditType='" + getCreditType() + "'" +
            ", department='" + getDepartment() + "'" +
            ", job='" + getJob() + "'" +
            ", order=" + getOrder() +
            "}";
    }
}
