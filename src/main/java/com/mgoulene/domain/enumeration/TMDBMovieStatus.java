package com.mgoulene.domain.enumeration;

/**
 * The TMDBMovieStatus enumeration.
 */
public enum TMDBMovieStatus {
    RUMORED, PLANNED, IN_PRODUCTION, POST_PRODUCTION, RELEASED, CANCELED
}
