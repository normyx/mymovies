package com.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The TMDBGenre entity.\n@author A true hipster
 */
@Entity
@Table(name = "tmdb_genre")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "tmdbgenre")
public class TMDBGenre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Column(name = "tmdb_id", nullable = false)
    private Integer tmdbId;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany(mappedBy = "genres")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<TMDBMovie> movies = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTmdbId() {
        return tmdbId;
    }

    public TMDBGenre tmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(Integer tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getName() {
        return name;
    }

    public TMDBGenre name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TMDBMovie> getMovies() {
        return movies;
    }

    public TMDBGenre movies(Set<TMDBMovie> tMDBMovies) {
        this.movies = tMDBMovies;
        return this;
    }

    public TMDBGenre addMovie(TMDBMovie tMDBMovie) {
        this.movies.add(tMDBMovie);
        tMDBMovie.getGenres().add(this);
        return this;
    }

    public TMDBGenre removeMovie(TMDBMovie tMDBMovie) {
        this.movies.remove(tMDBMovie);
        tMDBMovie.getGenres().remove(this);
        return this;
    }

    public void setMovies(Set<TMDBMovie> tMDBMovies) {
        this.movies = tMDBMovies;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBGenre)) {
            return false;
        }
        return id != null && id.equals(((TMDBGenre) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TMDBGenre{" +
            "id=" + getId() +
            ", tmdbId=" + getTmdbId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
