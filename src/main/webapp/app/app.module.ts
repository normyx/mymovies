import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MymoviesSharedModule } from 'app/shared/shared.module';
import { MymoviesCoreModule } from 'app/core/core.module';
import { MymoviesAppRoutingModule } from './app-routing.module';
import { MymoviesHomeModule } from './home/home.module';
import { MymoviesEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MymoviesdbModule } from './mymoviesdb/mymoviesdb.module';

@NgModule({
  imports: [
    BrowserModule,
    MymoviesSharedModule,
    MymoviesCoreModule,
    MymoviesHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MymoviesEntityModule,
    MymoviesdbModule,
    MymoviesAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class MymoviesAppModule {}
