import { Moment } from 'moment';
import { ITMDBCredit } from 'app/shared/model/tmdb-credit.model';
import { ITMDBGenre } from 'app/shared/model/tmdb-genre.model';
import { TMDBMovieStatus } from 'app/shared/model/enumerations/tmdb-movie-status.model';

export interface ITMDBMovieAPI {
  id?: number;
  tmdbId?: number;
  title?: string;
  adult?: boolean;
  homepage?: string;
  original_language?: string;
  originalTitle?: string;
  overview?: string;
  tagline?: string;
  status?: TMDBMovieStatus;
  voteAverage?: number;
  voteCount?: number;
  releaseDate?: Moment;
  credits?: ITMDBCredit[];
  genres?: ITMDBGenre[];
}

export class TMDBMovieAPI implements ITMDBMovieAPI {
  constructor(
    public id?: number,
    public tmdbId?: number,
    public title?: string,
    public adult?: boolean,
    public homepage?: string,
    public original_language?: string,
    public originalTitle?: string,
    public overview?: string,
    public tagline?: string,
    public status?: TMDBMovieStatus,
    public voteAverage?: number,
    public voteCount?: number,
    public releaseDate?: Moment,
    public credits?: ITMDBCredit[],
    public genres?: ITMDBGenre[]
  ) {
    this.adult = this.adult || false;
  }
}
