import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';

@Component({
  selector: 'jhi-tmdb-movie-detail-ext',
  templateUrl: './tmdb-movie-detail-ext.component.html',
})
export class TMDBMovieDetailExtComponent implements OnInit {
  tMDBMovie: ITMDBMovie | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBMovie }) => (this.tMDBMovie = tMDBMovie));
  }

  previousState(): void {
    window.history.back();
  }
}
