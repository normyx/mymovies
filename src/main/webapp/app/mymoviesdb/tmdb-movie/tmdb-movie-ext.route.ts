import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITMDBMovie, TMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { TMDBMovieExtService } from './tmdb-movie-ext.service';
import { TMDBMovieExtComponent } from './tmdb-movie-ext.component';
import { TMDBMovieDetailExtComponent } from './tmdb-movie-detail-ext.component';

@Injectable({ providedIn: 'root' })
export class TMDBMovieExtResolve implements Resolve<ITMDBMovie> {
  constructor(private service: TMDBMovieExtService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITMDBMovie> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBMovie: HttpResponse<TMDBMovie>) => {
          if (tMDBMovie.body) {
            return of(tMDBMovie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TMDBMovie());
  }
}

export const tMDBMovieExtRoute: Routes = [
  {
    path: '',
    component: TMDBMovieExtComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBMovieDetailExtComponent,
    resolve: {
      tMDBMovie: TMDBMovieExtResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
