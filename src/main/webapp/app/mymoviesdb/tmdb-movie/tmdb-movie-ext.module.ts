import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesSharedModule } from 'app/shared/shared.module';
import { TMDBMovieExtComponent } from './tmdb-movie-ext.component';
import { TMDBMovieDetailExtComponent } from './tmdb-movie-detail-ext.component';
import { tMDBMovieExtRoute } from './tmdb-movie-ext.route';

@NgModule({
  imports: [MymoviesSharedModule, RouterModule.forChild(tMDBMovieExtRoute)],
  declarations: [TMDBMovieExtComponent, TMDBMovieDetailExtComponent],
  entryComponents: [],
})
export class MymoviesTMDBMovieExtModule {}
