import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { ITMDBMovieAPI } from '../model/tmdb-movie-api.model';
import { TMDBMovieExtService } from './tmdb-movie-ext.service';

@Component({
  selector: 'jhi-tmdb-movie-ext',
  templateUrl: './tmdb-movie-ext.component.html',
})
export class TMDBMovieExtComponent implements OnInit, OnDestroy {
  tMDBMovies: ITMDBMovieAPI[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected tMDBMovieService: TMDBMovieExtService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.tMDBMovies = [];
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.tMDBMovieService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ITMDBMovieAPI[]>) => (this.tMDBMovies = res.body || []));
      return;
    }
  }

  reset(): void {
    this.tMDBMovies = [];
    this.loadAll();
  }

  search(query: string): void {
    this.tMDBMovies = [];

    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTMDBMovies();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITMDBMovieAPI): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTMDBMovies(): void {
    this.eventSubscriber = this.eventManager.subscribe('tMDBMovieListModification', () => this.reset());
  }
}
