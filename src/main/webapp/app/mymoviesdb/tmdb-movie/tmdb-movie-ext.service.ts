import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ITMDBMovieAPI } from 'app/mymovies/model/tmdb-movie-api.model';

type EntityResponseType = HttpResponse<ITMDBMovie>;
type EntityArrayResponseType = HttpResponse<ITMDBMovie[]>;

@Injectable({ providedIn: 'root' })
export class TMDBMovieExtService {
  public resourceUrl = SERVER_API_URL + 'api/tmdb-movies-ext';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/tmdb-movies-ext';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITMDBMovie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITMDBMovie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  search(req: Search): Observable<HttpResponse<ITMDBMovieAPI[]>> {
    const options = createRequestOption(req);
    return this.http
      .get<ITMDBMovie[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: HttpResponse<ITMDBMovieAPI[]>) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(tMDBMovie: ITMDBMovie): ITMDBMovie {
    const copy: ITMDBMovie = Object.assign({}, tMDBMovie, {
      releaseDate: tMDBMovie.releaseDate && tMDBMovie.releaseDate.isValid() ? tMDBMovie.releaseDate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.releaseDate = res.body.releaseDate ? moment(res.body.releaseDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tMDBMovie: ITMDBMovie) => {
        tMDBMovie.releaseDate = tMDBMovie.releaseDate ? moment(tMDBMovie.releaseDate) : undefined;
      });
    }
    return res;
  }
}
