export const enum TMDBMovieStatus {
  RUMORED = 'RUMORED',

  PLANNED = 'PLANNED',

  IN_PRODUCTION = 'IN_PRODUCTION',

  POST_PRODUCTION = 'POST_PRODUCTION',

  RELEASED = 'RELEASED',

  CANCELED = 'CANCELED',
}
