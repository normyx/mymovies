import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';

export interface ITMDBGenre {
  id?: number;
  tmdbId?: number;
  name?: string;
  movies?: ITMDBMovie[];
}

export class TMDBGenre implements ITMDBGenre {
  constructor(public id?: number, public tmdbId?: number, public name?: string, public movies?: ITMDBMovie[]) {}
}
