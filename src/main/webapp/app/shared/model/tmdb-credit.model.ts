export interface ITMDBCredit {
  id?: number;
  tmdbId?: string;
  character?: string;
  creditType?: string;
  department?: string;
  job?: string;
  order?: number;
  personName?: string;
  personId?: number;
  movieTitle?: string;
  movieId?: number;
}

export class TMDBCredit implements ITMDBCredit {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public character?: string,
    public creditType?: string,
    public department?: string,
    public job?: string,
    public order?: number,
    public personName?: string,
    public personId?: number,
    public movieTitle?: string,
    public movieId?: number
  ) {}
}
