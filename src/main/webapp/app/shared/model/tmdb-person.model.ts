import { Moment } from 'moment';

export interface ITMDBPerson {
  id?: number;
  tmdbId?: number;
  birthday?: Moment;
  deathday?: Moment;
  name?: string;
  aka?: string;
  gender?: number;
  biography?: string;
  placeOfBirth?: string;
  homepage?: string;
}

export class TMDBPerson implements ITMDBPerson {
  constructor(
    public id?: number,
    public tmdbId?: number,
    public birthday?: Moment,
    public deathday?: Moment,
    public name?: string,
    public aka?: string,
    public gender?: number,
    public biography?: string,
    public placeOfBirth?: string,
    public homepage?: string
  ) {}
}
