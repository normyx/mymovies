export interface ITMDBPicture {
  id?: number;
  tmdbId?: string;
  pictureContentType?: string;
  picture?: any;
  pictureSize?: string;
}

export class TMDBPicture implements ITMDBPicture {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public pictureContentType?: string,
    public picture?: any,
    public pictureSize?: string
  ) {}
}
