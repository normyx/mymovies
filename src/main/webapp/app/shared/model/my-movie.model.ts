import { Moment } from 'moment';

export interface IMyMovie {
  id?: number;
  comments?: string;
  vote?: number;
  viewedDate?: Moment;
  movieTitle?: string;
  movieId?: number;
  userLogin?: string;
  userId?: number;
}

export class MyMovie implements IMyMovie {
  constructor(
    public id?: number,
    public comments?: string,
    public vote?: number,
    public viewedDate?: Moment,
    public movieTitle?: string,
    public movieId?: number,
    public userLogin?: string,
    public userId?: number
  ) {}
}
