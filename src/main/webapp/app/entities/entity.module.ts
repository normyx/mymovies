import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'tmdb-movie',
        loadChildren: () => import('./tmdb-movie/tmdb-movie.module').then(m => m.MymoviesTMDBMovieModule),
      },
      {
        path: 'tmdb-genre',
        loadChildren: () => import('./tmdb-genre/tmdb-genre.module').then(m => m.MymoviesTMDBGenreModule),
      },
      {
        path: 'tmdb-person',
        loadChildren: () => import('./tmdb-person/tmdb-person.module').then(m => m.MymoviesTMDBPersonModule),
      },
      {
        path: 'tmdb-credit',
        loadChildren: () => import('./tmdb-credit/tmdb-credit.module').then(m => m.MymoviesTMDBCreditModule),
      },
      {
        path: 'tmdb-picture',
        loadChildren: () => import('./tmdb-picture/tmdb-picture.module').then(m => m.MymoviesTMDBPictureModule),
      },
      {
        path: 'my-movie',
        loadChildren: () => import('./my-movie/my-movie.module').then(m => m.MymoviesMyMovieModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MymoviesEntityModule {}
