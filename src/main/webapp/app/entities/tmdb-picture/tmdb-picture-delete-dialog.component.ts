import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITMDBPicture } from 'app/shared/model/tmdb-picture.model';
import { TMDBPictureService } from './tmdb-picture.service';

@Component({
  templateUrl: './tmdb-picture-delete-dialog.component.html',
})
export class TMDBPictureDeleteDialogComponent {
  tMDBPicture?: ITMDBPicture;

  constructor(
    protected tMDBPictureService: TMDBPictureService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tMDBPictureService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tMDBPictureListModification');
      this.activeModal.close();
    });
  }
}
