import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITMDBPicture, TMDBPicture } from 'app/shared/model/tmdb-picture.model';
import { TMDBPictureService } from './tmdb-picture.service';
import { TMDBPictureComponent } from './tmdb-picture.component';
import { TMDBPictureDetailComponent } from './tmdb-picture-detail.component';
import { TMDBPictureUpdateComponent } from './tmdb-picture-update.component';

@Injectable({ providedIn: 'root' })
export class TMDBPictureResolve implements Resolve<ITMDBPicture> {
  constructor(private service: TMDBPictureService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITMDBPicture> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBPicture: HttpResponse<TMDBPicture>) => {
          if (tMDBPicture.body) {
            return of(tMDBPicture.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TMDBPicture());
  }
}

export const tMDBPictureRoute: Routes = [
  {
    path: '',
    component: TMDBPictureComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPicture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBPictureDetailComponent,
    resolve: {
      tMDBPicture: TMDBPictureResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPicture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TMDBPictureUpdateComponent,
    resolve: {
      tMDBPicture: TMDBPictureResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPicture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TMDBPictureUpdateComponent,
    resolve: {
      tMDBPicture: TMDBPictureResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPicture.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
