import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITMDBPicture } from 'app/shared/model/tmdb-picture.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TMDBPictureService } from './tmdb-picture.service';
import { TMDBPictureDeleteDialogComponent } from './tmdb-picture-delete-dialog.component';

@Component({
  selector: 'jhi-tmdb-picture',
  templateUrl: './tmdb-picture.component.html',
})
export class TMDBPictureComponent implements OnInit, OnDestroy {
  tMDBPictures: ITMDBPicture[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;
  currentSearch: string;

  constructor(
    protected tMDBPictureService: TMDBPictureService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.tMDBPictures = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.tMDBPictureService
        .search({
          query: this.currentSearch,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.sort(),
        })
        .subscribe((res: HttpResponse<ITMDBPicture[]>) => this.paginateTMDBPictures(res.body, res.headers));
      return;
    }

    this.tMDBPictureService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ITMDBPicture[]>) => this.paginateTMDBPictures(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.tMDBPictures = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  search(query: string): void {
    this.tMDBPictures = [];
    this.links = {
      last: 0,
    };
    this.page = 0;
    if (query) {
      this.predicate = '_score';
      this.ascending = false;
    } else {
      this.predicate = 'id';
      this.ascending = true;
    }
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTMDBPictures();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITMDBPicture): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInTMDBPictures(): void {
    this.eventSubscriber = this.eventManager.subscribe('tMDBPictureListModification', () => this.reset());
  }

  delete(tMDBPicture: ITMDBPicture): void {
    const modalRef = this.modalService.open(TMDBPictureDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tMDBPicture = tMDBPicture;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTMDBPictures(data: ITMDBPicture[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.tMDBPictures.push(data[i]);
      }
    }
  }
}
