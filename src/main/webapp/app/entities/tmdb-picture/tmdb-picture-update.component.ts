import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { ITMDBPicture, TMDBPicture } from 'app/shared/model/tmdb-picture.model';
import { TMDBPictureService } from './tmdb-picture.service';
import { AlertError } from 'app/shared/alert/alert-error.model';

@Component({
  selector: 'jhi-tmdb-picture-update',
  templateUrl: './tmdb-picture-update.component.html',
})
export class TMDBPictureUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    picture: [null, [Validators.required]],
    pictureContentType: [],
    pictureSize: [null, [Validators.required]],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected tMDBPictureService: TMDBPictureService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBPicture }) => {
      this.updateForm(tMDBPicture);
    });
  }

  updateForm(tMDBPicture: ITMDBPicture): void {
    this.editForm.patchValue({
      id: tMDBPicture.id,
      tmdbId: tMDBPicture.tmdbId,
      picture: tMDBPicture.picture,
      pictureContentType: tMDBPicture.pictureContentType,
      pictureSize: tMDBPicture.pictureSize,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('mymoviesApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tMDBPicture = this.createFromForm();
    if (tMDBPicture.id !== undefined) {
      this.subscribeToSaveResponse(this.tMDBPictureService.update(tMDBPicture));
    } else {
      this.subscribeToSaveResponse(this.tMDBPictureService.create(tMDBPicture));
    }
  }

  private createFromForm(): ITMDBPicture {
    return {
      ...new TMDBPicture(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      pictureContentType: this.editForm.get(['pictureContentType'])!.value,
      picture: this.editForm.get(['picture'])!.value,
      pictureSize: this.editForm.get(['pictureSize'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITMDBPicture>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
