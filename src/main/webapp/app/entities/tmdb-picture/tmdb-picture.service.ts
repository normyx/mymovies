import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { ITMDBPicture } from 'app/shared/model/tmdb-picture.model';

type EntityResponseType = HttpResponse<ITMDBPicture>;
type EntityArrayResponseType = HttpResponse<ITMDBPicture[]>;

@Injectable({ providedIn: 'root' })
export class TMDBPictureService {
  public resourceUrl = SERVER_API_URL + 'api/tmdb-pictures';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/tmdb-pictures';

  constructor(protected http: HttpClient) {}

  create(tMDBPicture: ITMDBPicture): Observable<EntityResponseType> {
    return this.http.post<ITMDBPicture>(this.resourceUrl, tMDBPicture, { observe: 'response' });
  }

  update(tMDBPicture: ITMDBPicture): Observable<EntityResponseType> {
    return this.http.put<ITMDBPicture>(this.resourceUrl, tMDBPicture, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITMDBPicture>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITMDBPicture[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITMDBPicture[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
