import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesSharedModule } from 'app/shared/shared.module';
import { TMDBPictureComponent } from './tmdb-picture.component';
import { TMDBPictureDetailComponent } from './tmdb-picture-detail.component';
import { TMDBPictureUpdateComponent } from './tmdb-picture-update.component';
import { TMDBPictureDeleteDialogComponent } from './tmdb-picture-delete-dialog.component';
import { tMDBPictureRoute } from './tmdb-picture.route';

@NgModule({
  imports: [MymoviesSharedModule, RouterModule.forChild(tMDBPictureRoute)],
  declarations: [TMDBPictureComponent, TMDBPictureDetailComponent, TMDBPictureUpdateComponent, TMDBPictureDeleteDialogComponent],
  entryComponents: [TMDBPictureDeleteDialogComponent],
})
export class MymoviesTMDBPictureModule {}
