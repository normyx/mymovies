import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITMDBCredit, TMDBCredit } from 'app/shared/model/tmdb-credit.model';
import { TMDBCreditService } from './tmdb-credit.service';
import { ITMDBPerson } from 'app/shared/model/tmdb-person.model';
import { TMDBPersonService } from 'app/entities/tmdb-person/tmdb-person.service';
import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { TMDBMovieService } from 'app/entities/tmdb-movie/tmdb-movie.service';

type SelectableEntity = ITMDBPerson | ITMDBMovie;

@Component({
  selector: 'jhi-tmdb-credit-update',
  templateUrl: './tmdb-credit-update.component.html',
})
export class TMDBCreditUpdateComponent implements OnInit {
  isSaving = false;
  tmdbpeople: ITMDBPerson[] = [];
  tmdbmovies: ITMDBMovie[] = [];

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    character: [null, [Validators.maxLength(200)]],
    creditType: [],
    department: [],
    job: [],
    order: [],
    personId: [],
    movieId: [],
  });

  constructor(
    protected tMDBCreditService: TMDBCreditService,
    protected tMDBPersonService: TMDBPersonService,
    protected tMDBMovieService: TMDBMovieService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBCredit }) => {
      this.updateForm(tMDBCredit);

      this.tMDBPersonService.query().subscribe((res: HttpResponse<ITMDBPerson[]>) => (this.tmdbpeople = res.body || []));

      this.tMDBMovieService.query().subscribe((res: HttpResponse<ITMDBMovie[]>) => (this.tmdbmovies = res.body || []));
    });
  }

  updateForm(tMDBCredit: ITMDBCredit): void {
    this.editForm.patchValue({
      id: tMDBCredit.id,
      tmdbId: tMDBCredit.tmdbId,
      character: tMDBCredit.character,
      creditType: tMDBCredit.creditType,
      department: tMDBCredit.department,
      job: tMDBCredit.job,
      order: tMDBCredit.order,
      personId: tMDBCredit.personId,
      movieId: tMDBCredit.movieId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tMDBCredit = this.createFromForm();
    if (tMDBCredit.id !== undefined) {
      this.subscribeToSaveResponse(this.tMDBCreditService.update(tMDBCredit));
    } else {
      this.subscribeToSaveResponse(this.tMDBCreditService.create(tMDBCredit));
    }
  }

  private createFromForm(): ITMDBCredit {
    return {
      ...new TMDBCredit(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      character: this.editForm.get(['character'])!.value,
      creditType: this.editForm.get(['creditType'])!.value,
      department: this.editForm.get(['department'])!.value,
      job: this.editForm.get(['job'])!.value,
      order: this.editForm.get(['order'])!.value,
      personId: this.editForm.get(['personId'])!.value,
      movieId: this.editForm.get(['movieId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITMDBCredit>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
