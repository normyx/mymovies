import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITMDBCredit, TMDBCredit } from 'app/shared/model/tmdb-credit.model';
import { TMDBCreditService } from './tmdb-credit.service';
import { TMDBCreditComponent } from './tmdb-credit.component';
import { TMDBCreditDetailComponent } from './tmdb-credit-detail.component';
import { TMDBCreditUpdateComponent } from './tmdb-credit-update.component';

@Injectable({ providedIn: 'root' })
export class TMDBCreditResolve implements Resolve<ITMDBCredit> {
  constructor(private service: TMDBCreditService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITMDBCredit> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBCredit: HttpResponse<TMDBCredit>) => {
          if (tMDBCredit.body) {
            return of(tMDBCredit.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TMDBCredit());
  }
}

export const tMDBCreditRoute: Routes = [
  {
    path: '',
    component: TMDBCreditComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBCredit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBCreditDetailComponent,
    resolve: {
      tMDBCredit: TMDBCreditResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBCredit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TMDBCreditUpdateComponent,
    resolve: {
      tMDBCredit: TMDBCreditResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBCredit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TMDBCreditUpdateComponent,
    resolve: {
      tMDBCredit: TMDBCreditResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBCredit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
