import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITMDBCredit } from 'app/shared/model/tmdb-credit.model';
import { TMDBCreditService } from './tmdb-credit.service';

@Component({
  templateUrl: './tmdb-credit-delete-dialog.component.html',
})
export class TMDBCreditDeleteDialogComponent {
  tMDBCredit?: ITMDBCredit;

  constructor(
    protected tMDBCreditService: TMDBCreditService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tMDBCreditService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tMDBCreditListModification');
      this.activeModal.close();
    });
  }
}
