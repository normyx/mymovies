import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { ITMDBCredit } from 'app/shared/model/tmdb-credit.model';

type EntityResponseType = HttpResponse<ITMDBCredit>;
type EntityArrayResponseType = HttpResponse<ITMDBCredit[]>;

@Injectable({ providedIn: 'root' })
export class TMDBCreditService {
  public resourceUrl = SERVER_API_URL + 'api/tmdb-credits';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/tmdb-credits';

  constructor(protected http: HttpClient) {}

  create(tMDBCredit: ITMDBCredit): Observable<EntityResponseType> {
    return this.http.post<ITMDBCredit>(this.resourceUrl, tMDBCredit, { observe: 'response' });
  }

  update(tMDBCredit: ITMDBCredit): Observable<EntityResponseType> {
    return this.http.put<ITMDBCredit>(this.resourceUrl, tMDBCredit, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITMDBCredit>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITMDBCredit[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITMDBCredit[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
