import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesSharedModule } from 'app/shared/shared.module';
import { TMDBCreditComponent } from './tmdb-credit.component';
import { TMDBCreditDetailComponent } from './tmdb-credit-detail.component';
import { TMDBCreditUpdateComponent } from './tmdb-credit-update.component';
import { TMDBCreditDeleteDialogComponent } from './tmdb-credit-delete-dialog.component';
import { tMDBCreditRoute } from './tmdb-credit.route';

@NgModule({
  imports: [MymoviesSharedModule, RouterModule.forChild(tMDBCreditRoute)],
  declarations: [TMDBCreditComponent, TMDBCreditDetailComponent, TMDBCreditUpdateComponent, TMDBCreditDeleteDialogComponent],
  entryComponents: [TMDBCreditDeleteDialogComponent],
})
export class MymoviesTMDBCreditModule {}
