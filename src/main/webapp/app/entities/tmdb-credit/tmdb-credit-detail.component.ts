import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITMDBCredit } from 'app/shared/model/tmdb-credit.model';

@Component({
  selector: 'jhi-tmdb-credit-detail',
  templateUrl: './tmdb-credit-detail.component.html',
})
export class TMDBCreditDetailComponent implements OnInit {
  tMDBCredit: ITMDBCredit | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBCredit }) => (this.tMDBCredit = tMDBCredit));
  }

  previousState(): void {
    window.history.back();
  }
}
