import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesSharedModule } from 'app/shared/shared.module';
import { TMDBGenreComponent } from './tmdb-genre.component';
import { TMDBGenreDetailComponent } from './tmdb-genre-detail.component';
import { TMDBGenreUpdateComponent } from './tmdb-genre-update.component';
import { TMDBGenreDeleteDialogComponent } from './tmdb-genre-delete-dialog.component';
import { tMDBGenreRoute } from './tmdb-genre.route';

@NgModule({
  imports: [MymoviesSharedModule, RouterModule.forChild(tMDBGenreRoute)],
  declarations: [TMDBGenreComponent, TMDBGenreDetailComponent, TMDBGenreUpdateComponent, TMDBGenreDeleteDialogComponent],
  entryComponents: [TMDBGenreDeleteDialogComponent],
})
export class MymoviesTMDBGenreModule {}
