import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { ITMDBGenre } from 'app/shared/model/tmdb-genre.model';

type EntityResponseType = HttpResponse<ITMDBGenre>;
type EntityArrayResponseType = HttpResponse<ITMDBGenre[]>;

@Injectable({ providedIn: 'root' })
export class TMDBGenreService {
  public resourceUrl = SERVER_API_URL + 'api/tmdb-genres';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/tmdb-genres';

  constructor(protected http: HttpClient) {}

  create(tMDBGenre: ITMDBGenre): Observable<EntityResponseType> {
    return this.http.post<ITMDBGenre>(this.resourceUrl, tMDBGenre, { observe: 'response' });
  }

  update(tMDBGenre: ITMDBGenre): Observable<EntityResponseType> {
    return this.http.put<ITMDBGenre>(this.resourceUrl, tMDBGenre, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITMDBGenre>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITMDBGenre[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITMDBGenre[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
