import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITMDBGenre } from 'app/shared/model/tmdb-genre.model';
import { TMDBGenreService } from './tmdb-genre.service';

@Component({
  templateUrl: './tmdb-genre-delete-dialog.component.html',
})
export class TMDBGenreDeleteDialogComponent {
  tMDBGenre?: ITMDBGenre;

  constructor(protected tMDBGenreService: TMDBGenreService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tMDBGenreService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tMDBGenreListModification');
      this.activeModal.close();
    });
  }
}
