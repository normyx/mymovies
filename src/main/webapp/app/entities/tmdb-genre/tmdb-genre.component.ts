import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITMDBGenre } from 'app/shared/model/tmdb-genre.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TMDBGenreService } from './tmdb-genre.service';
import { TMDBGenreDeleteDialogComponent } from './tmdb-genre-delete-dialog.component';

@Component({
  selector: 'jhi-tmdb-genre',
  templateUrl: './tmdb-genre.component.html',
})
export class TMDBGenreComponent implements OnInit, OnDestroy {
  tMDBGenres: ITMDBGenre[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;
  currentSearch: string;

  constructor(
    protected tMDBGenreService: TMDBGenreService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.tMDBGenres = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.tMDBGenreService
        .search({
          query: this.currentSearch,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.sort(),
        })
        .subscribe((res: HttpResponse<ITMDBGenre[]>) => this.paginateTMDBGenres(res.body, res.headers));
      return;
    }

    this.tMDBGenreService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ITMDBGenre[]>) => this.paginateTMDBGenres(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.tMDBGenres = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  search(query: string): void {
    this.tMDBGenres = [];
    this.links = {
      last: 0,
    };
    this.page = 0;
    if (query) {
      this.predicate = '_score';
      this.ascending = false;
    } else {
      this.predicate = 'id';
      this.ascending = true;
    }
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTMDBGenres();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITMDBGenre): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTMDBGenres(): void {
    this.eventSubscriber = this.eventManager.subscribe('tMDBGenreListModification', () => this.reset());
  }

  delete(tMDBGenre: ITMDBGenre): void {
    const modalRef = this.modalService.open(TMDBGenreDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tMDBGenre = tMDBGenre;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTMDBGenres(data: ITMDBGenre[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.tMDBGenres.push(data[i]);
      }
    }
  }
}
