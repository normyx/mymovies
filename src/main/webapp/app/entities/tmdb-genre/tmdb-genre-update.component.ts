import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITMDBGenre, TMDBGenre } from 'app/shared/model/tmdb-genre.model';
import { TMDBGenreService } from './tmdb-genre.service';

@Component({
  selector: 'jhi-tmdb-genre-update',
  templateUrl: './tmdb-genre-update.component.html',
})
export class TMDBGenreUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    name: [null, [Validators.required]],
  });

  constructor(protected tMDBGenreService: TMDBGenreService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBGenre }) => {
      this.updateForm(tMDBGenre);
    });
  }

  updateForm(tMDBGenre: ITMDBGenre): void {
    this.editForm.patchValue({
      id: tMDBGenre.id,
      tmdbId: tMDBGenre.tmdbId,
      name: tMDBGenre.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tMDBGenre = this.createFromForm();
    if (tMDBGenre.id !== undefined) {
      this.subscribeToSaveResponse(this.tMDBGenreService.update(tMDBGenre));
    } else {
      this.subscribeToSaveResponse(this.tMDBGenreService.create(tMDBGenre));
    }
  }

  private createFromForm(): ITMDBGenre {
    return {
      ...new TMDBGenre(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITMDBGenre>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
