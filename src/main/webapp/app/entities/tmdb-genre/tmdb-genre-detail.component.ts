import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITMDBGenre } from 'app/shared/model/tmdb-genre.model';

@Component({
  selector: 'jhi-tmdb-genre-detail',
  templateUrl: './tmdb-genre-detail.component.html',
})
export class TMDBGenreDetailComponent implements OnInit {
  tMDBGenre: ITMDBGenre | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBGenre }) => (this.tMDBGenre = tMDBGenre));
  }

  previousState(): void {
    window.history.back();
  }
}
