import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITMDBGenre, TMDBGenre } from 'app/shared/model/tmdb-genre.model';
import { TMDBGenreService } from './tmdb-genre.service';
import { TMDBGenreComponent } from './tmdb-genre.component';
import { TMDBGenreDetailComponent } from './tmdb-genre-detail.component';
import { TMDBGenreUpdateComponent } from './tmdb-genre-update.component';

@Injectable({ providedIn: 'root' })
export class TMDBGenreResolve implements Resolve<ITMDBGenre> {
  constructor(private service: TMDBGenreService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITMDBGenre> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBGenre: HttpResponse<TMDBGenre>) => {
          if (tMDBGenre.body) {
            return of(tMDBGenre.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TMDBGenre());
  }
}

export const tMDBGenreRoute: Routes = [
  {
    path: '',
    component: TMDBGenreComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBGenre.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBGenreDetailComponent,
    resolve: {
      tMDBGenre: TMDBGenreResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBGenre.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TMDBGenreUpdateComponent,
    resolve: {
      tMDBGenre: TMDBGenreResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBGenre.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TMDBGenreUpdateComponent,
    resolve: {
      tMDBGenre: TMDBGenreResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBGenre.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
