import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';

@Component({
  selector: 'jhi-tmdb-movie-detail',
  templateUrl: './tmdb-movie-detail.component.html',
})
export class TMDBMovieDetailComponent implements OnInit {
  tMDBMovie: ITMDBMovie | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBMovie }) => (this.tMDBMovie = tMDBMovie));
  }

  previousState(): void {
    window.history.back();
  }
}
