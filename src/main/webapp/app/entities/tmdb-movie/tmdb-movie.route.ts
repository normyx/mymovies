import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITMDBMovie, TMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { TMDBMovieService } from './tmdb-movie.service';
import { TMDBMovieComponent } from './tmdb-movie.component';
import { TMDBMovieDetailComponent } from './tmdb-movie-detail.component';
import { TMDBMovieUpdateComponent } from './tmdb-movie-update.component';

@Injectable({ providedIn: 'root' })
export class TMDBMovieResolve implements Resolve<ITMDBMovie> {
  constructor(private service: TMDBMovieService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITMDBMovie> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBMovie: HttpResponse<TMDBMovie>) => {
          if (tMDBMovie.body) {
            return of(tMDBMovie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TMDBMovie());
  }
}

export const tMDBMovieRoute: Routes = [
  {
    path: '',
    component: TMDBMovieComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBMovieDetailComponent,
    resolve: {
      tMDBMovie: TMDBMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TMDBMovieUpdateComponent,
    resolve: {
      tMDBMovie: TMDBMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TMDBMovieUpdateComponent,
    resolve: {
      tMDBMovie: TMDBMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
