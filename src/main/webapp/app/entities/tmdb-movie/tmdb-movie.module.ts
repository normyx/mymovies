import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesSharedModule } from 'app/shared/shared.module';
import { TMDBMovieComponent } from './tmdb-movie.component';
import { TMDBMovieDetailComponent } from './tmdb-movie-detail.component';
import { TMDBMovieUpdateComponent } from './tmdb-movie-update.component';
import { TMDBMovieDeleteDialogComponent } from './tmdb-movie-delete-dialog.component';
import { tMDBMovieRoute } from './tmdb-movie.route';

@NgModule({
  imports: [MymoviesSharedModule, RouterModule.forChild(tMDBMovieRoute)],
  declarations: [TMDBMovieComponent, TMDBMovieDetailComponent, TMDBMovieUpdateComponent, TMDBMovieDeleteDialogComponent],
  entryComponents: [TMDBMovieDeleteDialogComponent],
})
export class MymoviesTMDBMovieModule {}
