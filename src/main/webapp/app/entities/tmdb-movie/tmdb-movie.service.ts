import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';

type EntityResponseType = HttpResponse<ITMDBMovie>;
type EntityArrayResponseType = HttpResponse<ITMDBMovie[]>;

@Injectable({ providedIn: 'root' })
export class TMDBMovieService {
  public resourceUrl = SERVER_API_URL + 'api/tmdb-movies';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/tmdb-movies';

  constructor(protected http: HttpClient) {}

  create(tMDBMovie: ITMDBMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tMDBMovie);
    return this.http
      .post<ITMDBMovie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tMDBMovie: ITMDBMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tMDBMovie);
    return this.http
      .put<ITMDBMovie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITMDBMovie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITMDBMovie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITMDBMovie[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(tMDBMovie: ITMDBMovie): ITMDBMovie {
    const copy: ITMDBMovie = Object.assign({}, tMDBMovie, {
      releaseDate: tMDBMovie.releaseDate && tMDBMovie.releaseDate.isValid() ? tMDBMovie.releaseDate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.releaseDate = res.body.releaseDate ? moment(res.body.releaseDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tMDBMovie: ITMDBMovie) => {
        tMDBMovie.releaseDate = tMDBMovie.releaseDate ? moment(tMDBMovie.releaseDate) : undefined;
      });
    }
    return res;
  }
}
