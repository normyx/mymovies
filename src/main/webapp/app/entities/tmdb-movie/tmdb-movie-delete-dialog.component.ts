import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { TMDBMovieService } from './tmdb-movie.service';

@Component({
  templateUrl: './tmdb-movie-delete-dialog.component.html',
})
export class TMDBMovieDeleteDialogComponent {
  tMDBMovie?: ITMDBMovie;

  constructor(protected tMDBMovieService: TMDBMovieService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tMDBMovieService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tMDBMovieListModification');
      this.activeModal.close();
    });
  }
}
