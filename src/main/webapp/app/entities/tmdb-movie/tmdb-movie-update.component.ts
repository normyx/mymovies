import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITMDBMovie, TMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { TMDBMovieService } from './tmdb-movie.service';
import { ITMDBGenre } from 'app/shared/model/tmdb-genre.model';
import { TMDBGenreService } from 'app/entities/tmdb-genre/tmdb-genre.service';

@Component({
  selector: 'jhi-tmdb-movie-update',
  templateUrl: './tmdb-movie-update.component.html',
})
export class TMDBMovieUpdateComponent implements OnInit {
  isSaving = false;
  tmdbgenres: ITMDBGenre[] = [];
  releaseDateDp: any;

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    title: [null, [Validators.maxLength(200)]],
    forAdult: [],
    homepage: [],
    originalLangage: [],
    originalTitle: [],
    overview: [null, [Validators.maxLength(4000)]],
    tagline: [],
    status: [],
    voteAverage: [null, [Validators.min(0), Validators.max(10)]],
    voteCount: [],
    releaseDate: [],
    genres: [],
  });

  constructor(
    protected tMDBMovieService: TMDBMovieService,
    protected tMDBGenreService: TMDBGenreService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBMovie }) => {
      this.updateForm(tMDBMovie);

      this.tMDBGenreService.query().subscribe((res: HttpResponse<ITMDBGenre[]>) => (this.tmdbgenres = res.body || []));
    });
  }

  updateForm(tMDBMovie: ITMDBMovie): void {
    this.editForm.patchValue({
      id: tMDBMovie.id,
      tmdbId: tMDBMovie.tmdbId,
      title: tMDBMovie.title,
      forAdult: tMDBMovie.forAdult,
      homepage: tMDBMovie.homepage,
      originalLangage: tMDBMovie.originalLangage,
      originalTitle: tMDBMovie.originalTitle,
      overview: tMDBMovie.overview,
      tagline: tMDBMovie.tagline,
      status: tMDBMovie.status,
      voteAverage: tMDBMovie.voteAverage,
      voteCount: tMDBMovie.voteCount,
      releaseDate: tMDBMovie.releaseDate,
      genres: tMDBMovie.genres,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tMDBMovie = this.createFromForm();
    if (tMDBMovie.id !== undefined) {
      this.subscribeToSaveResponse(this.tMDBMovieService.update(tMDBMovie));
    } else {
      this.subscribeToSaveResponse(this.tMDBMovieService.create(tMDBMovie));
    }
  }

  private createFromForm(): ITMDBMovie {
    return {
      ...new TMDBMovie(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      title: this.editForm.get(['title'])!.value,
      forAdult: this.editForm.get(['forAdult'])!.value,
      homepage: this.editForm.get(['homepage'])!.value,
      originalLangage: this.editForm.get(['originalLangage'])!.value,
      originalTitle: this.editForm.get(['originalTitle'])!.value,
      overview: this.editForm.get(['overview'])!.value,
      tagline: this.editForm.get(['tagline'])!.value,
      status: this.editForm.get(['status'])!.value,
      voteAverage: this.editForm.get(['voteAverage'])!.value,
      voteCount: this.editForm.get(['voteCount'])!.value,
      releaseDate: this.editForm.get(['releaseDate'])!.value,
      genres: this.editForm.get(['genres'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITMDBMovie>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITMDBGenre): any {
    return item.id;
  }

  getSelected(selectedVals: ITMDBGenre[], option: ITMDBGenre): ITMDBGenre {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
