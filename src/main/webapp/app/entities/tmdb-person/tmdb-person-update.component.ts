import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITMDBPerson, TMDBPerson } from 'app/shared/model/tmdb-person.model';
import { TMDBPersonService } from './tmdb-person.service';

@Component({
  selector: 'jhi-tmdb-person-update',
  templateUrl: './tmdb-person-update.component.html',
})
export class TMDBPersonUpdateComponent implements OnInit {
  isSaving = false;
  birthdayDp: any;
  deathdayDp: any;

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    birthday: [],
    deathday: [],
    name: [null, [Validators.maxLength(200)]],
    aka: [null, [Validators.maxLength(200)]],
    gender: [],
    biography: [null, [Validators.maxLength(4000)]],
    placeOfBirth: [null, [Validators.maxLength(200)]],
    homepage: [null, [Validators.maxLength(200)]],
  });

  constructor(protected tMDBPersonService: TMDBPersonService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBPerson }) => {
      this.updateForm(tMDBPerson);
    });
  }

  updateForm(tMDBPerson: ITMDBPerson): void {
    this.editForm.patchValue({
      id: tMDBPerson.id,
      tmdbId: tMDBPerson.tmdbId,
      birthday: tMDBPerson.birthday,
      deathday: tMDBPerson.deathday,
      name: tMDBPerson.name,
      aka: tMDBPerson.aka,
      gender: tMDBPerson.gender,
      biography: tMDBPerson.biography,
      placeOfBirth: tMDBPerson.placeOfBirth,
      homepage: tMDBPerson.homepage,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tMDBPerson = this.createFromForm();
    if (tMDBPerson.id !== undefined) {
      this.subscribeToSaveResponse(this.tMDBPersonService.update(tMDBPerson));
    } else {
      this.subscribeToSaveResponse(this.tMDBPersonService.create(tMDBPerson));
    }
  }

  private createFromForm(): ITMDBPerson {
    return {
      ...new TMDBPerson(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      birthday: this.editForm.get(['birthday'])!.value,
      deathday: this.editForm.get(['deathday'])!.value,
      name: this.editForm.get(['name'])!.value,
      aka: this.editForm.get(['aka'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      biography: this.editForm.get(['biography'])!.value,
      placeOfBirth: this.editForm.get(['placeOfBirth'])!.value,
      homepage: this.editForm.get(['homepage'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITMDBPerson>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
