import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITMDBPerson } from 'app/shared/model/tmdb-person.model';
import { TMDBPersonService } from './tmdb-person.service';

@Component({
  templateUrl: './tmdb-person-delete-dialog.component.html',
})
export class TMDBPersonDeleteDialogComponent {
  tMDBPerson?: ITMDBPerson;

  constructor(
    protected tMDBPersonService: TMDBPersonService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tMDBPersonService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tMDBPersonListModification');
      this.activeModal.close();
    });
  }
}
