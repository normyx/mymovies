import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITMDBPerson } from 'app/shared/model/tmdb-person.model';

@Component({
  selector: 'jhi-tmdb-person-detail',
  templateUrl: './tmdb-person-detail.component.html',
})
export class TMDBPersonDetailComponent implements OnInit {
  tMDBPerson: ITMDBPerson | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tMDBPerson }) => (this.tMDBPerson = tMDBPerson));
  }

  previousState(): void {
    window.history.back();
  }
}
