import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITMDBPerson } from 'app/shared/model/tmdb-person.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TMDBPersonService } from './tmdb-person.service';
import { TMDBPersonDeleteDialogComponent } from './tmdb-person-delete-dialog.component';

@Component({
  selector: 'jhi-tmdb-person',
  templateUrl: './tmdb-person.component.html',
})
export class TMDBPersonComponent implements OnInit, OnDestroy {
  tMDBPeople: ITMDBPerson[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;
  currentSearch: string;

  constructor(
    protected tMDBPersonService: TMDBPersonService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.tMDBPeople = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.tMDBPersonService
        .search({
          query: this.currentSearch,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.sort(),
        })
        .subscribe((res: HttpResponse<ITMDBPerson[]>) => this.paginateTMDBPeople(res.body, res.headers));
      return;
    }

    this.tMDBPersonService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ITMDBPerson[]>) => this.paginateTMDBPeople(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.tMDBPeople = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  search(query: string): void {
    this.tMDBPeople = [];
    this.links = {
      last: 0,
    };
    this.page = 0;
    if (query) {
      this.predicate = '_score';
      this.ascending = false;
    } else {
      this.predicate = 'id';
      this.ascending = true;
    }
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTMDBPeople();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITMDBPerson): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTMDBPeople(): void {
    this.eventSubscriber = this.eventManager.subscribe('tMDBPersonListModification', () => this.reset());
  }

  delete(tMDBPerson: ITMDBPerson): void {
    const modalRef = this.modalService.open(TMDBPersonDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tMDBPerson = tMDBPerson;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTMDBPeople(data: ITMDBPerson[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.tMDBPeople.push(data[i]);
      }
    }
  }
}
