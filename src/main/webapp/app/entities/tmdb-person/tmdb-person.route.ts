import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITMDBPerson, TMDBPerson } from 'app/shared/model/tmdb-person.model';
import { TMDBPersonService } from './tmdb-person.service';
import { TMDBPersonComponent } from './tmdb-person.component';
import { TMDBPersonDetailComponent } from './tmdb-person-detail.component';
import { TMDBPersonUpdateComponent } from './tmdb-person-update.component';

@Injectable({ providedIn: 'root' })
export class TMDBPersonResolve implements Resolve<ITMDBPerson> {
  constructor(private service: TMDBPersonService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITMDBPerson> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBPerson: HttpResponse<TMDBPerson>) => {
          if (tMDBPerson.body) {
            return of(tMDBPerson.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TMDBPerson());
  }
}

export const tMDBPersonRoute: Routes = [
  {
    path: '',
    component: TMDBPersonComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPerson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBPersonDetailComponent,
    resolve: {
      tMDBPerson: TMDBPersonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPerson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TMDBPersonUpdateComponent,
    resolve: {
      tMDBPerson: TMDBPersonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPerson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TMDBPersonUpdateComponent,
    resolve: {
      tMDBPerson: TMDBPersonResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBPerson.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
