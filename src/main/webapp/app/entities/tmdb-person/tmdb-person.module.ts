import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesSharedModule } from 'app/shared/shared.module';
import { TMDBPersonComponent } from './tmdb-person.component';
import { TMDBPersonDetailComponent } from './tmdb-person-detail.component';
import { TMDBPersonUpdateComponent } from './tmdb-person-update.component';
import { TMDBPersonDeleteDialogComponent } from './tmdb-person-delete-dialog.component';
import { tMDBPersonRoute } from './tmdb-person.route';

@NgModule({
  imports: [MymoviesSharedModule, RouterModule.forChild(tMDBPersonRoute)],
  declarations: [TMDBPersonComponent, TMDBPersonDetailComponent, TMDBPersonUpdateComponent, TMDBPersonDeleteDialogComponent],
  entryComponents: [TMDBPersonDeleteDialogComponent],
})
export class MymoviesTMDBPersonModule {}
