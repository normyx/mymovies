import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { ITMDBPerson } from 'app/shared/model/tmdb-person.model';

type EntityResponseType = HttpResponse<ITMDBPerson>;
type EntityArrayResponseType = HttpResponse<ITMDBPerson[]>;

@Injectable({ providedIn: 'root' })
export class TMDBPersonService {
  public resourceUrl = SERVER_API_URL + 'api/tmdb-people';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/tmdb-people';

  constructor(protected http: HttpClient) {}

  create(tMDBPerson: ITMDBPerson): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tMDBPerson);
    return this.http
      .post<ITMDBPerson>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tMDBPerson: ITMDBPerson): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tMDBPerson);
    return this.http
      .put<ITMDBPerson>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITMDBPerson>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITMDBPerson[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITMDBPerson[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(tMDBPerson: ITMDBPerson): ITMDBPerson {
    const copy: ITMDBPerson = Object.assign({}, tMDBPerson, {
      birthday: tMDBPerson.birthday && tMDBPerson.birthday.isValid() ? tMDBPerson.birthday.format(DATE_FORMAT) : undefined,
      deathday: tMDBPerson.deathday && tMDBPerson.deathday.isValid() ? tMDBPerson.deathday.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.birthday = res.body.birthday ? moment(res.body.birthday) : undefined;
      res.body.deathday = res.body.deathday ? moment(res.body.deathday) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tMDBPerson: ITMDBPerson) => {
        tMDBPerson.birthday = tMDBPerson.birthday ? moment(tMDBPerson.birthday) : undefined;
        tMDBPerson.deathday = tMDBPerson.deathday ? moment(tMDBPerson.deathday) : undefined;
      });
    }
    return res;
  }
}
