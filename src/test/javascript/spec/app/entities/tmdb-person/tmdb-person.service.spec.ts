import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { TMDBPersonService } from 'app/entities/tmdb-person/tmdb-person.service';
import { ITMDBPerson, TMDBPerson } from 'app/shared/model/tmdb-person.model';

describe('Service Tests', () => {
  describe('TMDBPerson Service', () => {
    let injector: TestBed;
    let service: TMDBPersonService;
    let httpMock: HttpTestingController;
    let elemDefault: ITMDBPerson;
    let expectedResult: ITMDBPerson | ITMDBPerson[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TMDBPersonService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new TMDBPerson(0, 0, currentDate, currentDate, 'AAAAAAA', 'AAAAAAA', 0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            birthday: currentDate.format(DATE_FORMAT),
            deathday: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a TMDBPerson', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            birthday: currentDate.format(DATE_FORMAT),
            deathday: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            birthday: currentDate,
            deathday: currentDate,
          },
          returnedFromService
        );

        service.create(new TMDBPerson()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a TMDBPerson', () => {
        const returnedFromService = Object.assign(
          {
            tmdbId: 1,
            birthday: currentDate.format(DATE_FORMAT),
            deathday: currentDate.format(DATE_FORMAT),
            name: 'BBBBBB',
            aka: 'BBBBBB',
            gender: 1,
            biography: 'BBBBBB',
            placeOfBirth: 'BBBBBB',
            homepage: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            birthday: currentDate,
            deathday: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of TMDBPerson', () => {
        const returnedFromService = Object.assign(
          {
            tmdbId: 1,
            birthday: currentDate.format(DATE_FORMAT),
            deathday: currentDate.format(DATE_FORMAT),
            name: 'BBBBBB',
            aka: 'BBBBBB',
            gender: 1,
            biography: 'BBBBBB',
            placeOfBirth: 'BBBBBB',
            homepage: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            birthday: currentDate,
            deathday: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TMDBPerson', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
