import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBPersonDetailComponent } from 'app/entities/tmdb-person/tmdb-person-detail.component';
import { TMDBPerson } from 'app/shared/model/tmdb-person.model';

describe('Component Tests', () => {
  describe('TMDBPerson Management Detail Component', () => {
    let comp: TMDBPersonDetailComponent;
    let fixture: ComponentFixture<TMDBPersonDetailComponent>;
    const route = ({ data: of({ tMDBPerson: new TMDBPerson(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBPersonDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TMDBPersonDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TMDBPersonDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tMDBPerson on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tMDBPerson).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
