import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBPersonUpdateComponent } from 'app/entities/tmdb-person/tmdb-person-update.component';
import { TMDBPersonService } from 'app/entities/tmdb-person/tmdb-person.service';
import { TMDBPerson } from 'app/shared/model/tmdb-person.model';

describe('Component Tests', () => {
  describe('TMDBPerson Management Update Component', () => {
    let comp: TMDBPersonUpdateComponent;
    let fixture: ComponentFixture<TMDBPersonUpdateComponent>;
    let service: TMDBPersonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBPersonUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TMDBPersonUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TMDBPersonUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TMDBPersonService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBPerson(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBPerson();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
