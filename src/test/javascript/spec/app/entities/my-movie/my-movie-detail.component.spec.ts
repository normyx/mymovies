import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { MyMovieDetailComponent } from 'app/entities/my-movie/my-movie-detail.component';
import { MyMovie } from 'app/shared/model/my-movie.model';

describe('Component Tests', () => {
  describe('MyMovie Management Detail Component', () => {
    let comp: MyMovieDetailComponent;
    let fixture: ComponentFixture<MyMovieDetailComponent>;
    const route = ({ data: of({ myMovie: new MyMovie(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [MyMovieDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MyMovieDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MyMovieDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load myMovie on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.myMovie).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
