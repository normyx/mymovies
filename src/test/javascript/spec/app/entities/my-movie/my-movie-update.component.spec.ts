import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { MyMovieUpdateComponent } from 'app/entities/my-movie/my-movie-update.component';
import { MyMovieService } from 'app/entities/my-movie/my-movie.service';
import { MyMovie } from 'app/shared/model/my-movie.model';

describe('Component Tests', () => {
  describe('MyMovie Management Update Component', () => {
    let comp: MyMovieUpdateComponent;
    let fixture: ComponentFixture<MyMovieUpdateComponent>;
    let service: MyMovieService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [MyMovieUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MyMovieUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MyMovieUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MyMovieService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MyMovie(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MyMovie();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
