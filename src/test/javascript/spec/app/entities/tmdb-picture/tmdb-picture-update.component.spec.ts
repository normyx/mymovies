import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBPictureUpdateComponent } from 'app/entities/tmdb-picture/tmdb-picture-update.component';
import { TMDBPictureService } from 'app/entities/tmdb-picture/tmdb-picture.service';
import { TMDBPicture } from 'app/shared/model/tmdb-picture.model';

describe('Component Tests', () => {
  describe('TMDBPicture Management Update Component', () => {
    let comp: TMDBPictureUpdateComponent;
    let fixture: ComponentFixture<TMDBPictureUpdateComponent>;
    let service: TMDBPictureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBPictureUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TMDBPictureUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TMDBPictureUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TMDBPictureService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBPicture(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBPicture();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
