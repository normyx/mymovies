import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBPictureDetailComponent } from 'app/entities/tmdb-picture/tmdb-picture-detail.component';
import { TMDBPicture } from 'app/shared/model/tmdb-picture.model';

describe('Component Tests', () => {
  describe('TMDBPicture Management Detail Component', () => {
    let comp: TMDBPictureDetailComponent;
    let fixture: ComponentFixture<TMDBPictureDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ tMDBPicture: new TMDBPicture(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBPictureDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TMDBPictureDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TMDBPictureDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load tMDBPicture on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tMDBPicture).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
