import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { TMDBMovieService } from 'app/entities/tmdb-movie/tmdb-movie.service';
import { ITMDBMovie, TMDBMovie } from 'app/shared/model/tmdb-movie.model';
import { TMDBMovieStatus } from 'app/shared/model/enumerations/tmdb-movie-status.model';

describe('Service Tests', () => {
  describe('TMDBMovie Service', () => {
    let injector: TestBed;
    let service: TMDBMovieService;
    let httpMock: HttpTestingController;
    let elemDefault: ITMDBMovie;
    let expectedResult: ITMDBMovie | ITMDBMovie[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TMDBMovieService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new TMDBMovie(
        0,
        0,
        'AAAAAAA',
        false,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        TMDBMovieStatus.RUMORED,
        0,
        0,
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            releaseDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a TMDBMovie', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            releaseDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            releaseDate: currentDate,
          },
          returnedFromService
        );

        service.create(new TMDBMovie()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a TMDBMovie', () => {
        const returnedFromService = Object.assign(
          {
            tmdbId: 1,
            title: 'BBBBBB',
            forAdult: true,
            homepage: 'BBBBBB',
            originalLangage: 'BBBBBB',
            originalTitle: 'BBBBBB',
            overview: 'BBBBBB',
            tagline: 'BBBBBB',
            status: 'BBBBBB',
            voteAverage: 1,
            voteCount: 1,
            releaseDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            releaseDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of TMDBMovie', () => {
        const returnedFromService = Object.assign(
          {
            tmdbId: 1,
            title: 'BBBBBB',
            forAdult: true,
            homepage: 'BBBBBB',
            originalLangage: 'BBBBBB',
            originalTitle: 'BBBBBB',
            overview: 'BBBBBB',
            tagline: 'BBBBBB',
            status: 'BBBBBB',
            voteAverage: 1,
            voteCount: 1,
            releaseDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            releaseDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TMDBMovie', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
