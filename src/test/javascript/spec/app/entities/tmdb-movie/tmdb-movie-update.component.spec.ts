import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBMovieUpdateComponent } from 'app/entities/tmdb-movie/tmdb-movie-update.component';
import { TMDBMovieService } from 'app/entities/tmdb-movie/tmdb-movie.service';
import { TMDBMovie } from 'app/shared/model/tmdb-movie.model';

describe('Component Tests', () => {
  describe('TMDBMovie Management Update Component', () => {
    let comp: TMDBMovieUpdateComponent;
    let fixture: ComponentFixture<TMDBMovieUpdateComponent>;
    let service: TMDBMovieService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBMovieUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TMDBMovieUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TMDBMovieUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TMDBMovieService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBMovie(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBMovie();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
