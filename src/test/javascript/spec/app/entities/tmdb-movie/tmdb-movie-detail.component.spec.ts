import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBMovieDetailComponent } from 'app/entities/tmdb-movie/tmdb-movie-detail.component';
import { TMDBMovie } from 'app/shared/model/tmdb-movie.model';

describe('Component Tests', () => {
  describe('TMDBMovie Management Detail Component', () => {
    let comp: TMDBMovieDetailComponent;
    let fixture: ComponentFixture<TMDBMovieDetailComponent>;
    const route = ({ data: of({ tMDBMovie: new TMDBMovie(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBMovieDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TMDBMovieDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TMDBMovieDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tMDBMovie on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tMDBMovie).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
