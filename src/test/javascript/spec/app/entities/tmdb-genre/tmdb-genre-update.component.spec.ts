import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBGenreUpdateComponent } from 'app/entities/tmdb-genre/tmdb-genre-update.component';
import { TMDBGenreService } from 'app/entities/tmdb-genre/tmdb-genre.service';
import { TMDBGenre } from 'app/shared/model/tmdb-genre.model';

describe('Component Tests', () => {
  describe('TMDBGenre Management Update Component', () => {
    let comp: TMDBGenreUpdateComponent;
    let fixture: ComponentFixture<TMDBGenreUpdateComponent>;
    let service: TMDBGenreService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBGenreUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TMDBGenreUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TMDBGenreUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TMDBGenreService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBGenre(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBGenre();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
