import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBGenreDetailComponent } from 'app/entities/tmdb-genre/tmdb-genre-detail.component';
import { TMDBGenre } from 'app/shared/model/tmdb-genre.model';

describe('Component Tests', () => {
  describe('TMDBGenre Management Detail Component', () => {
    let comp: TMDBGenreDetailComponent;
    let fixture: ComponentFixture<TMDBGenreDetailComponent>;
    const route = ({ data: of({ tMDBGenre: new TMDBGenre(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBGenreDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TMDBGenreDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TMDBGenreDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tMDBGenre on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tMDBGenre).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
