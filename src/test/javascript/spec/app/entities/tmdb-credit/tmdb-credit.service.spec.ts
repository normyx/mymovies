import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TMDBCreditService } from 'app/entities/tmdb-credit/tmdb-credit.service';
import { ITMDBCredit, TMDBCredit } from 'app/shared/model/tmdb-credit.model';

describe('Service Tests', () => {
  describe('TMDBCredit Service', () => {
    let injector: TestBed;
    let service: TMDBCreditService;
    let httpMock: HttpTestingController;
    let elemDefault: ITMDBCredit;
    let expectedResult: ITMDBCredit | ITMDBCredit[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TMDBCreditService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new TMDBCredit(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a TMDBCredit', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new TMDBCredit()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a TMDBCredit', () => {
        const returnedFromService = Object.assign(
          {
            tmdbId: 'BBBBBB',
            character: 'BBBBBB',
            creditType: 'BBBBBB',
            department: 'BBBBBB',
            job: 'BBBBBB',
            order: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of TMDBCredit', () => {
        const returnedFromService = Object.assign(
          {
            tmdbId: 'BBBBBB',
            character: 'BBBBBB',
            creditType: 'BBBBBB',
            department: 'BBBBBB',
            job: 'BBBBBB',
            order: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TMDBCredit', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
