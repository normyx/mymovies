import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBCreditUpdateComponent } from 'app/entities/tmdb-credit/tmdb-credit-update.component';
import { TMDBCreditService } from 'app/entities/tmdb-credit/tmdb-credit.service';
import { TMDBCredit } from 'app/shared/model/tmdb-credit.model';

describe('Component Tests', () => {
  describe('TMDBCredit Management Update Component', () => {
    let comp: TMDBCreditUpdateComponent;
    let fixture: ComponentFixture<TMDBCreditUpdateComponent>;
    let service: TMDBCreditService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBCreditUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TMDBCreditUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TMDBCreditUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TMDBCreditService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBCredit(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TMDBCredit();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
