import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MymoviesTestModule } from '../../../test.module';
import { TMDBCreditDetailComponent } from 'app/entities/tmdb-credit/tmdb-credit-detail.component';
import { TMDBCredit } from 'app/shared/model/tmdb-credit.model';

describe('Component Tests', () => {
  describe('TMDBCredit Management Detail Component', () => {
    let comp: TMDBCreditDetailComponent;
    let fixture: ComponentFixture<TMDBCreditDetailComponent>;
    const route = ({ data: of({ tMDBCredit: new TMDBCredit(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MymoviesTestModule],
        declarations: [TMDBCreditDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TMDBCreditDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TMDBCreditDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tMDBCredit on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tMDBCredit).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
