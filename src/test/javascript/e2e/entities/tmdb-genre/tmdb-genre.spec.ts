import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TMDBGenreComponentsPage, TMDBGenreDeleteDialog, TMDBGenreUpdatePage } from './tmdb-genre.page-object';

const expect = chai.expect;

describe('TMDBGenre e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let tMDBGenreComponentsPage: TMDBGenreComponentsPage;
  let tMDBGenreUpdatePage: TMDBGenreUpdatePage;
  let tMDBGenreDeleteDialog: TMDBGenreDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TMDBGenres', async () => {
    await navBarPage.goToEntity('tmdb-genre');
    tMDBGenreComponentsPage = new TMDBGenreComponentsPage();
    await browser.wait(ec.visibilityOf(tMDBGenreComponentsPage.title), 5000);
    expect(await tMDBGenreComponentsPage.getTitle()).to.eq('mymoviesApp.tMDBGenre.home.title');
    await browser.wait(ec.or(ec.visibilityOf(tMDBGenreComponentsPage.entities), ec.visibilityOf(tMDBGenreComponentsPage.noResult)), 1000);
  });

  it('should load create TMDBGenre page', async () => {
    await tMDBGenreComponentsPage.clickOnCreateButton();
    tMDBGenreUpdatePage = new TMDBGenreUpdatePage();
    expect(await tMDBGenreUpdatePage.getPageTitle()).to.eq('mymoviesApp.tMDBGenre.home.createOrEditLabel');
    await tMDBGenreUpdatePage.cancel();
  });

  it('should create and save TMDBGenres', async () => {
    const nbButtonsBeforeCreate = await tMDBGenreComponentsPage.countDeleteButtons();

    await tMDBGenreComponentsPage.clickOnCreateButton();

    await promise.all([tMDBGenreUpdatePage.setTmdbIdInput('5'), tMDBGenreUpdatePage.setNameInput('name')]);

    expect(await tMDBGenreUpdatePage.getTmdbIdInput()).to.eq('5', 'Expected tmdbId value to be equals to 5');
    expect(await tMDBGenreUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');

    await tMDBGenreUpdatePage.save();
    expect(await tMDBGenreUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await tMDBGenreComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TMDBGenre', async () => {
    const nbButtonsBeforeDelete = await tMDBGenreComponentsPage.countDeleteButtons();
    await tMDBGenreComponentsPage.clickOnLastDeleteButton();

    tMDBGenreDeleteDialog = new TMDBGenreDeleteDialog();
    expect(await tMDBGenreDeleteDialog.getDialogTitle()).to.eq('mymoviesApp.tMDBGenre.delete.question');
    await tMDBGenreDeleteDialog.clickOnConfirmButton();

    expect(await tMDBGenreComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
