import { element, by, ElementFinder } from 'protractor';

export class TMDBGenreComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-tmdb-genre div table .btn-danger'));
  title = element.all(by.css('jhi-tmdb-genre div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TMDBGenreUpdatePage {
  pageTitle = element(by.id('jhi-tmdb-genre-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  tmdbIdInput = element(by.id('field_tmdbId'));
  nameInput = element(by.id('field_name'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TMDBGenreDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-tMDBGenre-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-tMDBGenre'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
