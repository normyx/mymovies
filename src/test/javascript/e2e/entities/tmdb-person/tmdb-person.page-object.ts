import { element, by, ElementFinder } from 'protractor';

export class TMDBPersonComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-tmdb-person div table .btn-danger'));
  title = element.all(by.css('jhi-tmdb-person div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TMDBPersonUpdatePage {
  pageTitle = element(by.id('jhi-tmdb-person-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  tmdbIdInput = element(by.id('field_tmdbId'));
  birthdayInput = element(by.id('field_birthday'));
  deathdayInput = element(by.id('field_deathday'));
  nameInput = element(by.id('field_name'));
  akaInput = element(by.id('field_aka'));
  genderInput = element(by.id('field_gender'));
  biographyInput = element(by.id('field_biography'));
  placeOfBirthInput = element(by.id('field_placeOfBirth'));
  homepageInput = element(by.id('field_homepage'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setBirthdayInput(birthday: string): Promise<void> {
    await this.birthdayInput.sendKeys(birthday);
  }

  async getBirthdayInput(): Promise<string> {
    return await this.birthdayInput.getAttribute('value');
  }

  async setDeathdayInput(deathday: string): Promise<void> {
    await this.deathdayInput.sendKeys(deathday);
  }

  async getDeathdayInput(): Promise<string> {
    return await this.deathdayInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setAkaInput(aka: string): Promise<void> {
    await this.akaInput.sendKeys(aka);
  }

  async getAkaInput(): Promise<string> {
    return await this.akaInput.getAttribute('value');
  }

  async setGenderInput(gender: string): Promise<void> {
    await this.genderInput.sendKeys(gender);
  }

  async getGenderInput(): Promise<string> {
    return await this.genderInput.getAttribute('value');
  }

  async setBiographyInput(biography: string): Promise<void> {
    await this.biographyInput.sendKeys(biography);
  }

  async getBiographyInput(): Promise<string> {
    return await this.biographyInput.getAttribute('value');
  }

  async setPlaceOfBirthInput(placeOfBirth: string): Promise<void> {
    await this.placeOfBirthInput.sendKeys(placeOfBirth);
  }

  async getPlaceOfBirthInput(): Promise<string> {
    return await this.placeOfBirthInput.getAttribute('value');
  }

  async setHomepageInput(homepage: string): Promise<void> {
    await this.homepageInput.sendKeys(homepage);
  }

  async getHomepageInput(): Promise<string> {
    return await this.homepageInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TMDBPersonDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-tMDBPerson-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-tMDBPerson'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
