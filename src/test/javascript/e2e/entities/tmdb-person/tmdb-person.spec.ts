import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TMDBPersonComponentsPage, TMDBPersonDeleteDialog, TMDBPersonUpdatePage } from './tmdb-person.page-object';

const expect = chai.expect;

describe('TMDBPerson e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let tMDBPersonComponentsPage: TMDBPersonComponentsPage;
  let tMDBPersonUpdatePage: TMDBPersonUpdatePage;
  let tMDBPersonDeleteDialog: TMDBPersonDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TMDBPeople', async () => {
    await navBarPage.goToEntity('tmdb-person');
    tMDBPersonComponentsPage = new TMDBPersonComponentsPage();
    await browser.wait(ec.visibilityOf(tMDBPersonComponentsPage.title), 5000);
    expect(await tMDBPersonComponentsPage.getTitle()).to.eq('mymoviesApp.tMDBPerson.home.title');
    await browser.wait(ec.or(ec.visibilityOf(tMDBPersonComponentsPage.entities), ec.visibilityOf(tMDBPersonComponentsPage.noResult)), 1000);
  });

  it('should load create TMDBPerson page', async () => {
    await tMDBPersonComponentsPage.clickOnCreateButton();
    tMDBPersonUpdatePage = new TMDBPersonUpdatePage();
    expect(await tMDBPersonUpdatePage.getPageTitle()).to.eq('mymoviesApp.tMDBPerson.home.createOrEditLabel');
    await tMDBPersonUpdatePage.cancel();
  });

  it('should create and save TMDBPeople', async () => {
    const nbButtonsBeforeCreate = await tMDBPersonComponentsPage.countDeleteButtons();

    await tMDBPersonComponentsPage.clickOnCreateButton();

    await promise.all([
      tMDBPersonUpdatePage.setTmdbIdInput('5'),
      tMDBPersonUpdatePage.setBirthdayInput('2000-12-31'),
      tMDBPersonUpdatePage.setDeathdayInput('2000-12-31'),
      tMDBPersonUpdatePage.setNameInput('name'),
      tMDBPersonUpdatePage.setAkaInput('aka'),
      tMDBPersonUpdatePage.setGenderInput('5'),
      tMDBPersonUpdatePage.setBiographyInput('biography'),
      tMDBPersonUpdatePage.setPlaceOfBirthInput('placeOfBirth'),
      tMDBPersonUpdatePage.setHomepageInput('homepage'),
    ]);

    expect(await tMDBPersonUpdatePage.getTmdbIdInput()).to.eq('5', 'Expected tmdbId value to be equals to 5');
    expect(await tMDBPersonUpdatePage.getBirthdayInput()).to.eq('2000-12-31', 'Expected birthday value to be equals to 2000-12-31');
    expect(await tMDBPersonUpdatePage.getDeathdayInput()).to.eq('2000-12-31', 'Expected deathday value to be equals to 2000-12-31');
    expect(await tMDBPersonUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await tMDBPersonUpdatePage.getAkaInput()).to.eq('aka', 'Expected Aka value to be equals to aka');
    expect(await tMDBPersonUpdatePage.getGenderInput()).to.eq('5', 'Expected gender value to be equals to 5');
    expect(await tMDBPersonUpdatePage.getBiographyInput()).to.eq('biography', 'Expected Biography value to be equals to biography');
    expect(await tMDBPersonUpdatePage.getPlaceOfBirthInput()).to.eq(
      'placeOfBirth',
      'Expected PlaceOfBirth value to be equals to placeOfBirth'
    );
    expect(await tMDBPersonUpdatePage.getHomepageInput()).to.eq('homepage', 'Expected Homepage value to be equals to homepage');

    await tMDBPersonUpdatePage.save();
    expect(await tMDBPersonUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await tMDBPersonComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TMDBPerson', async () => {
    const nbButtonsBeforeDelete = await tMDBPersonComponentsPage.countDeleteButtons();
    await tMDBPersonComponentsPage.clickOnLastDeleteButton();

    tMDBPersonDeleteDialog = new TMDBPersonDeleteDialog();
    expect(await tMDBPersonDeleteDialog.getDialogTitle()).to.eq('mymoviesApp.tMDBPerson.delete.question');
    await tMDBPersonDeleteDialog.clickOnConfirmButton();

    expect(await tMDBPersonComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
