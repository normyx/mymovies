import { element, by, ElementFinder } from 'protractor';

export class TMDBPictureComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-tmdb-picture div table .btn-danger'));
  title = element.all(by.css('jhi-tmdb-picture div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TMDBPictureUpdatePage {
  pageTitle = element(by.id('jhi-tmdb-picture-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  tmdbIdInput = element(by.id('field_tmdbId'));
  pictureInput = element(by.id('file_picture'));
  pictureSizeInput = element(by.id('field_pictureSize'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setPictureInput(picture: string): Promise<void> {
    await this.pictureInput.sendKeys(picture);
  }

  async getPictureInput(): Promise<string> {
    return await this.pictureInput.getAttribute('value');
  }

  async setPictureSizeInput(pictureSize: string): Promise<void> {
    await this.pictureSizeInput.sendKeys(pictureSize);
  }

  async getPictureSizeInput(): Promise<string> {
    return await this.pictureSizeInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TMDBPictureDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-tMDBPicture-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-tMDBPicture'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
