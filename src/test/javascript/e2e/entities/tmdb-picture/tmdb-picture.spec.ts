import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TMDBPictureComponentsPage, TMDBPictureDeleteDialog, TMDBPictureUpdatePage } from './tmdb-picture.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('TMDBPicture e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let tMDBPictureComponentsPage: TMDBPictureComponentsPage;
  let tMDBPictureUpdatePage: TMDBPictureUpdatePage;
  let tMDBPictureDeleteDialog: TMDBPictureDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TMDBPictures', async () => {
    await navBarPage.goToEntity('tmdb-picture');
    tMDBPictureComponentsPage = new TMDBPictureComponentsPage();
    await browser.wait(ec.visibilityOf(tMDBPictureComponentsPage.title), 5000);
    expect(await tMDBPictureComponentsPage.getTitle()).to.eq('mymoviesApp.tMDBPicture.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(tMDBPictureComponentsPage.entities), ec.visibilityOf(tMDBPictureComponentsPage.noResult)),
      1000
    );
  });

  it('should load create TMDBPicture page', async () => {
    await tMDBPictureComponentsPage.clickOnCreateButton();
    tMDBPictureUpdatePage = new TMDBPictureUpdatePage();
    expect(await tMDBPictureUpdatePage.getPageTitle()).to.eq('mymoviesApp.tMDBPicture.home.createOrEditLabel');
    await tMDBPictureUpdatePage.cancel();
  });

  it('should create and save TMDBPictures', async () => {
    const nbButtonsBeforeCreate = await tMDBPictureComponentsPage.countDeleteButtons();

    await tMDBPictureComponentsPage.clickOnCreateButton();

    await promise.all([
      tMDBPictureUpdatePage.setTmdbIdInput('tmdbId'),
      tMDBPictureUpdatePage.setPictureInput(absolutePath),
      tMDBPictureUpdatePage.setPictureSizeInput('pictureSize'),
    ]);

    expect(await tMDBPictureUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');
    expect(await tMDBPictureUpdatePage.getPictureInput()).to.endsWith(
      fileNameToUpload,
      'Expected Picture value to be end with ' + fileNameToUpload
    );
    expect(await tMDBPictureUpdatePage.getPictureSizeInput()).to.eq(
      'pictureSize',
      'Expected PictureSize value to be equals to pictureSize'
    );

    await tMDBPictureUpdatePage.save();
    expect(await tMDBPictureUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await tMDBPictureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TMDBPicture', async () => {
    const nbButtonsBeforeDelete = await tMDBPictureComponentsPage.countDeleteButtons();
    await tMDBPictureComponentsPage.clickOnLastDeleteButton();

    tMDBPictureDeleteDialog = new TMDBPictureDeleteDialog();
    expect(await tMDBPictureDeleteDialog.getDialogTitle()).to.eq('mymoviesApp.tMDBPicture.delete.question');
    await tMDBPictureDeleteDialog.clickOnConfirmButton();

    expect(await tMDBPictureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
