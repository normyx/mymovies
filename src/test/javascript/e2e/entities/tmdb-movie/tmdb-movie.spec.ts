import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TMDBMovieComponentsPage, TMDBMovieDeleteDialog, TMDBMovieUpdatePage } from './tmdb-movie.page-object';

const expect = chai.expect;

describe('TMDBMovie e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let tMDBMovieComponentsPage: TMDBMovieComponentsPage;
  let tMDBMovieUpdatePage: TMDBMovieUpdatePage;
  let tMDBMovieDeleteDialog: TMDBMovieDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TMDBMovies', async () => {
    await navBarPage.goToEntity('tmdb-movie');
    tMDBMovieComponentsPage = new TMDBMovieComponentsPage();
    await browser.wait(ec.visibilityOf(tMDBMovieComponentsPage.title), 5000);
    expect(await tMDBMovieComponentsPage.getTitle()).to.eq('mymoviesApp.tMDBMovie.home.title');
    await browser.wait(ec.or(ec.visibilityOf(tMDBMovieComponentsPage.entities), ec.visibilityOf(tMDBMovieComponentsPage.noResult)), 1000);
  });

  it('should load create TMDBMovie page', async () => {
    await tMDBMovieComponentsPage.clickOnCreateButton();
    tMDBMovieUpdatePage = new TMDBMovieUpdatePage();
    expect(await tMDBMovieUpdatePage.getPageTitle()).to.eq('mymoviesApp.tMDBMovie.home.createOrEditLabel');
    await tMDBMovieUpdatePage.cancel();
  });

  it('should create and save TMDBMovies', async () => {
    const nbButtonsBeforeCreate = await tMDBMovieComponentsPage.countDeleteButtons();

    await tMDBMovieComponentsPage.clickOnCreateButton();

    await promise.all([
      tMDBMovieUpdatePage.setTmdbIdInput('5'),
      tMDBMovieUpdatePage.setTitleInput('title'),
      tMDBMovieUpdatePage.setHomepageInput('homepage'),
      tMDBMovieUpdatePage.setOriginalLangageInput('originalLangage'),
      tMDBMovieUpdatePage.setOriginalTitleInput('originalTitle'),
      tMDBMovieUpdatePage.setOverviewInput('overview'),
      tMDBMovieUpdatePage.setTaglineInput('tagline'),
      tMDBMovieUpdatePage.statusSelectLastOption(),
      tMDBMovieUpdatePage.setVoteAverageInput('5'),
      tMDBMovieUpdatePage.setVoteCountInput('5'),
      tMDBMovieUpdatePage.setReleaseDateInput('2000-12-31'),
      // tMDBMovieUpdatePage.genreSelectLastOption(),
    ]);

    expect(await tMDBMovieUpdatePage.getTmdbIdInput()).to.eq('5', 'Expected tmdbId value to be equals to 5');
    expect(await tMDBMovieUpdatePage.getTitleInput()).to.eq('title', 'Expected Title value to be equals to title');
    const selectedForAdult = tMDBMovieUpdatePage.getForAdultInput();
    if (await selectedForAdult.isSelected()) {
      await tMDBMovieUpdatePage.getForAdultInput().click();
      expect(await tMDBMovieUpdatePage.getForAdultInput().isSelected(), 'Expected forAdult not to be selected').to.be.false;
    } else {
      await tMDBMovieUpdatePage.getForAdultInput().click();
      expect(await tMDBMovieUpdatePage.getForAdultInput().isSelected(), 'Expected forAdult to be selected').to.be.true;
    }
    expect(await tMDBMovieUpdatePage.getHomepageInput()).to.eq('homepage', 'Expected Homepage value to be equals to homepage');
    expect(await tMDBMovieUpdatePage.getOriginalLangageInput()).to.eq(
      'originalLangage',
      'Expected OriginalLangage value to be equals to originalLangage'
    );
    expect(await tMDBMovieUpdatePage.getOriginalTitleInput()).to.eq(
      'originalTitle',
      'Expected OriginalTitle value to be equals to originalTitle'
    );
    expect(await tMDBMovieUpdatePage.getOverviewInput()).to.eq('overview', 'Expected Overview value to be equals to overview');
    expect(await tMDBMovieUpdatePage.getTaglineInput()).to.eq('tagline', 'Expected Tagline value to be equals to tagline');
    expect(await tMDBMovieUpdatePage.getVoteAverageInput()).to.eq('5', 'Expected voteAverage value to be equals to 5');
    expect(await tMDBMovieUpdatePage.getVoteCountInput()).to.eq('5', 'Expected voteCount value to be equals to 5');
    expect(await tMDBMovieUpdatePage.getReleaseDateInput()).to.eq('2000-12-31', 'Expected releaseDate value to be equals to 2000-12-31');

    await tMDBMovieUpdatePage.save();
    expect(await tMDBMovieUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await tMDBMovieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TMDBMovie', async () => {
    const nbButtonsBeforeDelete = await tMDBMovieComponentsPage.countDeleteButtons();
    await tMDBMovieComponentsPage.clickOnLastDeleteButton();

    tMDBMovieDeleteDialog = new TMDBMovieDeleteDialog();
    expect(await tMDBMovieDeleteDialog.getDialogTitle()).to.eq('mymoviesApp.tMDBMovie.delete.question');
    await tMDBMovieDeleteDialog.clickOnConfirmButton();

    expect(await tMDBMovieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
