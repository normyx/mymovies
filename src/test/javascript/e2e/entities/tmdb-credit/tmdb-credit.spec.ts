import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TMDBCreditComponentsPage, TMDBCreditDeleteDialog, TMDBCreditUpdatePage } from './tmdb-credit.page-object';

const expect = chai.expect;

describe('TMDBCredit e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let tMDBCreditComponentsPage: TMDBCreditComponentsPage;
  let tMDBCreditUpdatePage: TMDBCreditUpdatePage;
  let tMDBCreditDeleteDialog: TMDBCreditDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TMDBCredits', async () => {
    await navBarPage.goToEntity('tmdb-credit');
    tMDBCreditComponentsPage = new TMDBCreditComponentsPage();
    await browser.wait(ec.visibilityOf(tMDBCreditComponentsPage.title), 5000);
    expect(await tMDBCreditComponentsPage.getTitle()).to.eq('mymoviesApp.tMDBCredit.home.title');
    await browser.wait(ec.or(ec.visibilityOf(tMDBCreditComponentsPage.entities), ec.visibilityOf(tMDBCreditComponentsPage.noResult)), 1000);
  });

  it('should load create TMDBCredit page', async () => {
    await tMDBCreditComponentsPage.clickOnCreateButton();
    tMDBCreditUpdatePage = new TMDBCreditUpdatePage();
    expect(await tMDBCreditUpdatePage.getPageTitle()).to.eq('mymoviesApp.tMDBCredit.home.createOrEditLabel');
    await tMDBCreditUpdatePage.cancel();
  });

  it('should create and save TMDBCredits', async () => {
    const nbButtonsBeforeCreate = await tMDBCreditComponentsPage.countDeleteButtons();

    await tMDBCreditComponentsPage.clickOnCreateButton();

    await promise.all([
      tMDBCreditUpdatePage.setTmdbIdInput('tmdbId'),
      tMDBCreditUpdatePage.setCharacterInput('character'),
      tMDBCreditUpdatePage.setCreditTypeInput('creditType'),
      tMDBCreditUpdatePage.setDepartmentInput('department'),
      tMDBCreditUpdatePage.setJobInput('job'),
      tMDBCreditUpdatePage.setOrderInput('5'),
      tMDBCreditUpdatePage.personSelectLastOption(),
      tMDBCreditUpdatePage.movieSelectLastOption(),
    ]);

    expect(await tMDBCreditUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');
    expect(await tMDBCreditUpdatePage.getCharacterInput()).to.eq('character', 'Expected Character value to be equals to character');
    expect(await tMDBCreditUpdatePage.getCreditTypeInput()).to.eq('creditType', 'Expected CreditType value to be equals to creditType');
    expect(await tMDBCreditUpdatePage.getDepartmentInput()).to.eq('department', 'Expected Department value to be equals to department');
    expect(await tMDBCreditUpdatePage.getJobInput()).to.eq('job', 'Expected Job value to be equals to job');
    expect(await tMDBCreditUpdatePage.getOrderInput()).to.eq('5', 'Expected order value to be equals to 5');

    await tMDBCreditUpdatePage.save();
    expect(await tMDBCreditUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await tMDBCreditComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TMDBCredit', async () => {
    const nbButtonsBeforeDelete = await tMDBCreditComponentsPage.countDeleteButtons();
    await tMDBCreditComponentsPage.clickOnLastDeleteButton();

    tMDBCreditDeleteDialog = new TMDBCreditDeleteDialog();
    expect(await tMDBCreditDeleteDialog.getDialogTitle()).to.eq('mymoviesApp.tMDBCredit.delete.question');
    await tMDBCreditDeleteDialog.clickOnConfirmButton();

    expect(await tMDBCreditComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
