import { element, by, ElementFinder } from 'protractor';

export class TMDBCreditComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-tmdb-credit div table .btn-danger'));
  title = element.all(by.css('jhi-tmdb-credit div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TMDBCreditUpdatePage {
  pageTitle = element(by.id('jhi-tmdb-credit-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  tmdbIdInput = element(by.id('field_tmdbId'));
  characterInput = element(by.id('field_character'));
  creditTypeInput = element(by.id('field_creditType'));
  departmentInput = element(by.id('field_department'));
  jobInput = element(by.id('field_job'));
  orderInput = element(by.id('field_order'));

  personSelect = element(by.id('field_person'));
  movieSelect = element(by.id('field_movie'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setCharacterInput(character: string): Promise<void> {
    await this.characterInput.sendKeys(character);
  }

  async getCharacterInput(): Promise<string> {
    return await this.characterInput.getAttribute('value');
  }

  async setCreditTypeInput(creditType: string): Promise<void> {
    await this.creditTypeInput.sendKeys(creditType);
  }

  async getCreditTypeInput(): Promise<string> {
    return await this.creditTypeInput.getAttribute('value');
  }

  async setDepartmentInput(department: string): Promise<void> {
    await this.departmentInput.sendKeys(department);
  }

  async getDepartmentInput(): Promise<string> {
    return await this.departmentInput.getAttribute('value');
  }

  async setJobInput(job: string): Promise<void> {
    await this.jobInput.sendKeys(job);
  }

  async getJobInput(): Promise<string> {
    return await this.jobInput.getAttribute('value');
  }

  async setOrderInput(order: string): Promise<void> {
    await this.orderInput.sendKeys(order);
  }

  async getOrderInput(): Promise<string> {
    return await this.orderInput.getAttribute('value');
  }

  async personSelectLastOption(): Promise<void> {
    await this.personSelect.all(by.tagName('option')).last().click();
  }

  async personSelectOption(option: string): Promise<void> {
    await this.personSelect.sendKeys(option);
  }

  getPersonSelect(): ElementFinder {
    return this.personSelect;
  }

  async getPersonSelectedOption(): Promise<string> {
    return await this.personSelect.element(by.css('option:checked')).getText();
  }

  async movieSelectLastOption(): Promise<void> {
    await this.movieSelect.all(by.tagName('option')).last().click();
  }

  async movieSelectOption(option: string): Promise<void> {
    await this.movieSelect.sendKeys(option);
  }

  getMovieSelect(): ElementFinder {
    return this.movieSelect;
  }

  async getMovieSelectedOption(): Promise<string> {
    return await this.movieSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TMDBCreditDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-tMDBCredit-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-tMDBCredit'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
