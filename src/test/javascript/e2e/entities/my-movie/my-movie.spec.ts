import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MyMovieComponentsPage, MyMovieDeleteDialog, MyMovieUpdatePage } from './my-movie.page-object';

const expect = chai.expect;

describe('MyMovie e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let myMovieComponentsPage: MyMovieComponentsPage;
  let myMovieUpdatePage: MyMovieUpdatePage;
  let myMovieDeleteDialog: MyMovieDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load MyMovies', async () => {
    await navBarPage.goToEntity('my-movie');
    myMovieComponentsPage = new MyMovieComponentsPage();
    await browser.wait(ec.visibilityOf(myMovieComponentsPage.title), 5000);
    expect(await myMovieComponentsPage.getTitle()).to.eq('mymoviesApp.myMovie.home.title');
    await browser.wait(ec.or(ec.visibilityOf(myMovieComponentsPage.entities), ec.visibilityOf(myMovieComponentsPage.noResult)), 1000);
  });

  it('should load create MyMovie page', async () => {
    await myMovieComponentsPage.clickOnCreateButton();
    myMovieUpdatePage = new MyMovieUpdatePage();
    expect(await myMovieUpdatePage.getPageTitle()).to.eq('mymoviesApp.myMovie.home.createOrEditLabel');
    await myMovieUpdatePage.cancel();
  });

  it('should create and save MyMovies', async () => {
    const nbButtonsBeforeCreate = await myMovieComponentsPage.countDeleteButtons();

    await myMovieComponentsPage.clickOnCreateButton();

    await promise.all([
      myMovieUpdatePage.setCommentsInput('comments'),
      myMovieUpdatePage.setVoteInput('5'),
      myMovieUpdatePage.setViewedDateInput('2000-12-31'),
      myMovieUpdatePage.movieSelectLastOption(),
      myMovieUpdatePage.userSelectLastOption(),
    ]);

    expect(await myMovieUpdatePage.getCommentsInput()).to.eq('comments', 'Expected Comments value to be equals to comments');
    expect(await myMovieUpdatePage.getVoteInput()).to.eq('5', 'Expected vote value to be equals to 5');
    expect(await myMovieUpdatePage.getViewedDateInput()).to.eq('2000-12-31', 'Expected viewedDate value to be equals to 2000-12-31');

    await myMovieUpdatePage.save();
    expect(await myMovieUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await myMovieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last MyMovie', async () => {
    const nbButtonsBeforeDelete = await myMovieComponentsPage.countDeleteButtons();
    await myMovieComponentsPage.clickOnLastDeleteButton();

    myMovieDeleteDialog = new MyMovieDeleteDialog();
    expect(await myMovieDeleteDialog.getDialogTitle()).to.eq('mymoviesApp.myMovie.delete.question');
    await myMovieDeleteDialog.clickOnConfirmButton();

    expect(await myMovieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
