package com.mgoulene.web.rest;

import com.mgoulene.MymoviesApp;
import com.mgoulene.domain.TMDBCredit;
import com.mgoulene.domain.TMDBPerson;
import com.mgoulene.domain.TMDBMovie;
import com.mgoulene.repository.TMDBCreditRepository;
import com.mgoulene.repository.search.TMDBCreditSearchRepository;
import com.mgoulene.service.TMDBCreditService;
import com.mgoulene.service.dto.TMDBCreditDTO;
import com.mgoulene.service.mapper.TMDBCreditMapper;
import com.mgoulene.service.dto.TMDBCreditCriteria;
import com.mgoulene.service.TMDBCreditQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TMDBCreditResource} REST controller.
 */
@SpringBootTest(classes = MymoviesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TMDBCreditResourceIT {

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CHARACTER = "AAAAAAAAAA";
    private static final String UPDATED_CHARACTER = "BBBBBBBBBB";

    private static final String DEFAULT_CREDIT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CREDIT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final String DEFAULT_JOB = "AAAAAAAAAA";
    private static final String UPDATED_JOB = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;
    private static final Integer SMALLER_ORDER = 1 - 1;

    @Autowired
    private TMDBCreditRepository tMDBCreditRepository;

    @Autowired
    private TMDBCreditMapper tMDBCreditMapper;

    @Autowired
    private TMDBCreditService tMDBCreditService;

    /**
     * This repository is mocked in the com.mgoulene.repository.search test package.
     *
     * @see com.mgoulene.repository.search.TMDBCreditSearchRepositoryMockConfiguration
     */
    @Autowired
    private TMDBCreditSearchRepository mockTMDBCreditSearchRepository;

    @Autowired
    private TMDBCreditQueryService tMDBCreditQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTMDBCreditMockMvc;

    private TMDBCredit tMDBCredit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBCredit createEntity(EntityManager em) {
        TMDBCredit tMDBCredit = new TMDBCredit()
            .tmdbId(DEFAULT_TMDB_ID)
            .character(DEFAULT_CHARACTER)
            .creditType(DEFAULT_CREDIT_TYPE)
            .department(DEFAULT_DEPARTMENT)
            .job(DEFAULT_JOB)
            .order(DEFAULT_ORDER);
        return tMDBCredit;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBCredit createUpdatedEntity(EntityManager em) {
        TMDBCredit tMDBCredit = new TMDBCredit()
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER);
        return tMDBCredit;
    }

    @BeforeEach
    public void initTest() {
        tMDBCredit = createEntity(em);
    }

    @Test
    @Transactional
    public void createTMDBCredit() throws Exception {
        int databaseSizeBeforeCreate = tMDBCreditRepository.findAll().size();
        // Create the TMDBCredit
        TMDBCreditDTO tMDBCreditDTO = tMDBCreditMapper.toDto(tMDBCredit);
        restTMDBCreditMockMvc.perform(post("/api/tmdb-credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBCreditDTO)))
            .andExpect(status().isCreated());

        // Validate the TMDBCredit in the database
        List<TMDBCredit> tMDBCreditList = tMDBCreditRepository.findAll();
        assertThat(tMDBCreditList).hasSize(databaseSizeBeforeCreate + 1);
        TMDBCredit testTMDBCredit = tMDBCreditList.get(tMDBCreditList.size() - 1);
        assertThat(testTMDBCredit.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testTMDBCredit.getCharacter()).isEqualTo(DEFAULT_CHARACTER);
        assertThat(testTMDBCredit.getCreditType()).isEqualTo(DEFAULT_CREDIT_TYPE);
        assertThat(testTMDBCredit.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testTMDBCredit.getJob()).isEqualTo(DEFAULT_JOB);
        assertThat(testTMDBCredit.getOrder()).isEqualTo(DEFAULT_ORDER);

        // Validate the TMDBCredit in Elasticsearch
        verify(mockTMDBCreditSearchRepository, times(1)).save(testTMDBCredit);
    }

    @Test
    @Transactional
    public void createTMDBCreditWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tMDBCreditRepository.findAll().size();

        // Create the TMDBCredit with an existing ID
        tMDBCredit.setId(1L);
        TMDBCreditDTO tMDBCreditDTO = tMDBCreditMapper.toDto(tMDBCredit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTMDBCreditMockMvc.perform(post("/api/tmdb-credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBCreditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBCredit in the database
        List<TMDBCredit> tMDBCreditList = tMDBCreditRepository.findAll();
        assertThat(tMDBCreditList).hasSize(databaseSizeBeforeCreate);

        // Validate the TMDBCredit in Elasticsearch
        verify(mockTMDBCreditSearchRepository, times(0)).save(tMDBCredit);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBCreditRepository.findAll().size();
        // set the field null
        tMDBCredit.setTmdbId(null);

        // Create the TMDBCredit, which fails.
        TMDBCreditDTO tMDBCreditDTO = tMDBCreditMapper.toDto(tMDBCredit);


        restTMDBCreditMockMvc.perform(post("/api/tmdb-credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBCreditDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBCredit> tMDBCreditList = tMDBCreditRepository.findAll();
        assertThat(tMDBCreditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTMDBCredits() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBCredit.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].character").value(hasItem(DEFAULT_CHARACTER)))
            .andExpect(jsonPath("$.[*].creditType").value(hasItem(DEFAULT_CREDIT_TYPE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].job").value(hasItem(DEFAULT_JOB)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));
    }
    
    @Test
    @Transactional
    public void getTMDBCredit() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get the tMDBCredit
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits/{id}", tMDBCredit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tMDBCredit.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.character").value(DEFAULT_CHARACTER))
            .andExpect(jsonPath("$.creditType").value(DEFAULT_CREDIT_TYPE))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.job").value(DEFAULT_JOB))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER));
    }


    @Test
    @Transactional
    public void getTMDBCreditsByIdFiltering() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        Long id = tMDBCredit.getId();

        defaultTMDBCreditShouldBeFound("id.equals=" + id);
        defaultTMDBCreditShouldNotBeFound("id.notEquals=" + id);

        defaultTMDBCreditShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTMDBCreditShouldNotBeFound("id.greaterThan=" + id);

        defaultTMDBCreditShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTMDBCreditShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where tmdbId equals to DEFAULT_TMDB_ID
        defaultTMDBCreditShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBCreditList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBCreditShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultTMDBCreditShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBCreditList where tmdbId not equals to UPDATED_TMDB_ID
        defaultTMDBCreditShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultTMDBCreditShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the tMDBCreditList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBCreditShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where tmdbId is not null
        defaultTMDBCreditShouldBeFound("tmdbId.specified=true");

        // Get all the tMDBCreditList where tmdbId is null
        defaultTMDBCreditShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBCreditsByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where tmdbId contains DEFAULT_TMDB_ID
        defaultTMDBCreditShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the tMDBCreditList where tmdbId contains UPDATED_TMDB_ID
        defaultTMDBCreditShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultTMDBCreditShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the tMDBCreditList where tmdbId does not contain UPDATED_TMDB_ID
        defaultTMDBCreditShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByCharacterIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where character equals to DEFAULT_CHARACTER
        defaultTMDBCreditShouldBeFound("character.equals=" + DEFAULT_CHARACTER);

        // Get all the tMDBCreditList where character equals to UPDATED_CHARACTER
        defaultTMDBCreditShouldNotBeFound("character.equals=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCharacterIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where character not equals to DEFAULT_CHARACTER
        defaultTMDBCreditShouldNotBeFound("character.notEquals=" + DEFAULT_CHARACTER);

        // Get all the tMDBCreditList where character not equals to UPDATED_CHARACTER
        defaultTMDBCreditShouldBeFound("character.notEquals=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCharacterIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where character in DEFAULT_CHARACTER or UPDATED_CHARACTER
        defaultTMDBCreditShouldBeFound("character.in=" + DEFAULT_CHARACTER + "," + UPDATED_CHARACTER);

        // Get all the tMDBCreditList where character equals to UPDATED_CHARACTER
        defaultTMDBCreditShouldNotBeFound("character.in=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCharacterIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where character is not null
        defaultTMDBCreditShouldBeFound("character.specified=true");

        // Get all the tMDBCreditList where character is null
        defaultTMDBCreditShouldNotBeFound("character.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBCreditsByCharacterContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where character contains DEFAULT_CHARACTER
        defaultTMDBCreditShouldBeFound("character.contains=" + DEFAULT_CHARACTER);

        // Get all the tMDBCreditList where character contains UPDATED_CHARACTER
        defaultTMDBCreditShouldNotBeFound("character.contains=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCharacterNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where character does not contain DEFAULT_CHARACTER
        defaultTMDBCreditShouldNotBeFound("character.doesNotContain=" + DEFAULT_CHARACTER);

        // Get all the tMDBCreditList where character does not contain UPDATED_CHARACTER
        defaultTMDBCreditShouldBeFound("character.doesNotContain=" + UPDATED_CHARACTER);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByCreditTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where creditType equals to DEFAULT_CREDIT_TYPE
        defaultTMDBCreditShouldBeFound("creditType.equals=" + DEFAULT_CREDIT_TYPE);

        // Get all the tMDBCreditList where creditType equals to UPDATED_CREDIT_TYPE
        defaultTMDBCreditShouldNotBeFound("creditType.equals=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCreditTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where creditType not equals to DEFAULT_CREDIT_TYPE
        defaultTMDBCreditShouldNotBeFound("creditType.notEquals=" + DEFAULT_CREDIT_TYPE);

        // Get all the tMDBCreditList where creditType not equals to UPDATED_CREDIT_TYPE
        defaultTMDBCreditShouldBeFound("creditType.notEquals=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCreditTypeIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where creditType in DEFAULT_CREDIT_TYPE or UPDATED_CREDIT_TYPE
        defaultTMDBCreditShouldBeFound("creditType.in=" + DEFAULT_CREDIT_TYPE + "," + UPDATED_CREDIT_TYPE);

        // Get all the tMDBCreditList where creditType equals to UPDATED_CREDIT_TYPE
        defaultTMDBCreditShouldNotBeFound("creditType.in=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCreditTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where creditType is not null
        defaultTMDBCreditShouldBeFound("creditType.specified=true");

        // Get all the tMDBCreditList where creditType is null
        defaultTMDBCreditShouldNotBeFound("creditType.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBCreditsByCreditTypeContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where creditType contains DEFAULT_CREDIT_TYPE
        defaultTMDBCreditShouldBeFound("creditType.contains=" + DEFAULT_CREDIT_TYPE);

        // Get all the tMDBCreditList where creditType contains UPDATED_CREDIT_TYPE
        defaultTMDBCreditShouldNotBeFound("creditType.contains=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByCreditTypeNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where creditType does not contain DEFAULT_CREDIT_TYPE
        defaultTMDBCreditShouldNotBeFound("creditType.doesNotContain=" + DEFAULT_CREDIT_TYPE);

        // Get all the tMDBCreditList where creditType does not contain UPDATED_CREDIT_TYPE
        defaultTMDBCreditShouldBeFound("creditType.doesNotContain=" + UPDATED_CREDIT_TYPE);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByDepartmentIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where department equals to DEFAULT_DEPARTMENT
        defaultTMDBCreditShouldBeFound("department.equals=" + DEFAULT_DEPARTMENT);

        // Get all the tMDBCreditList where department equals to UPDATED_DEPARTMENT
        defaultTMDBCreditShouldNotBeFound("department.equals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByDepartmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where department not equals to DEFAULT_DEPARTMENT
        defaultTMDBCreditShouldNotBeFound("department.notEquals=" + DEFAULT_DEPARTMENT);

        // Get all the tMDBCreditList where department not equals to UPDATED_DEPARTMENT
        defaultTMDBCreditShouldBeFound("department.notEquals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByDepartmentIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where department in DEFAULT_DEPARTMENT or UPDATED_DEPARTMENT
        defaultTMDBCreditShouldBeFound("department.in=" + DEFAULT_DEPARTMENT + "," + UPDATED_DEPARTMENT);

        // Get all the tMDBCreditList where department equals to UPDATED_DEPARTMENT
        defaultTMDBCreditShouldNotBeFound("department.in=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByDepartmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where department is not null
        defaultTMDBCreditShouldBeFound("department.specified=true");

        // Get all the tMDBCreditList where department is null
        defaultTMDBCreditShouldNotBeFound("department.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBCreditsByDepartmentContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where department contains DEFAULT_DEPARTMENT
        defaultTMDBCreditShouldBeFound("department.contains=" + DEFAULT_DEPARTMENT);

        // Get all the tMDBCreditList where department contains UPDATED_DEPARTMENT
        defaultTMDBCreditShouldNotBeFound("department.contains=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByDepartmentNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where department does not contain DEFAULT_DEPARTMENT
        defaultTMDBCreditShouldNotBeFound("department.doesNotContain=" + DEFAULT_DEPARTMENT);

        // Get all the tMDBCreditList where department does not contain UPDATED_DEPARTMENT
        defaultTMDBCreditShouldBeFound("department.doesNotContain=" + UPDATED_DEPARTMENT);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByJobIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where job equals to DEFAULT_JOB
        defaultTMDBCreditShouldBeFound("job.equals=" + DEFAULT_JOB);

        // Get all the tMDBCreditList where job equals to UPDATED_JOB
        defaultTMDBCreditShouldNotBeFound("job.equals=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByJobIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where job not equals to DEFAULT_JOB
        defaultTMDBCreditShouldNotBeFound("job.notEquals=" + DEFAULT_JOB);

        // Get all the tMDBCreditList where job not equals to UPDATED_JOB
        defaultTMDBCreditShouldBeFound("job.notEquals=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByJobIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where job in DEFAULT_JOB or UPDATED_JOB
        defaultTMDBCreditShouldBeFound("job.in=" + DEFAULT_JOB + "," + UPDATED_JOB);

        // Get all the tMDBCreditList where job equals to UPDATED_JOB
        defaultTMDBCreditShouldNotBeFound("job.in=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByJobIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where job is not null
        defaultTMDBCreditShouldBeFound("job.specified=true");

        // Get all the tMDBCreditList where job is null
        defaultTMDBCreditShouldNotBeFound("job.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBCreditsByJobContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where job contains DEFAULT_JOB
        defaultTMDBCreditShouldBeFound("job.contains=" + DEFAULT_JOB);

        // Get all the tMDBCreditList where job contains UPDATED_JOB
        defaultTMDBCreditShouldNotBeFound("job.contains=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByJobNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where job does not contain DEFAULT_JOB
        defaultTMDBCreditShouldNotBeFound("job.doesNotContain=" + DEFAULT_JOB);

        // Get all the tMDBCreditList where job does not contain UPDATED_JOB
        defaultTMDBCreditShouldBeFound("job.doesNotContain=" + UPDATED_JOB);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order equals to DEFAULT_ORDER
        defaultTMDBCreditShouldBeFound("order.equals=" + DEFAULT_ORDER);

        // Get all the tMDBCreditList where order equals to UPDATED_ORDER
        defaultTMDBCreditShouldNotBeFound("order.equals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order not equals to DEFAULT_ORDER
        defaultTMDBCreditShouldNotBeFound("order.notEquals=" + DEFAULT_ORDER);

        // Get all the tMDBCreditList where order not equals to UPDATED_ORDER
        defaultTMDBCreditShouldBeFound("order.notEquals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order in DEFAULT_ORDER or UPDATED_ORDER
        defaultTMDBCreditShouldBeFound("order.in=" + DEFAULT_ORDER + "," + UPDATED_ORDER);

        // Get all the tMDBCreditList where order equals to UPDATED_ORDER
        defaultTMDBCreditShouldNotBeFound("order.in=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order is not null
        defaultTMDBCreditShouldBeFound("order.specified=true");

        // Get all the tMDBCreditList where order is null
        defaultTMDBCreditShouldNotBeFound("order.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order is greater than or equal to DEFAULT_ORDER
        defaultTMDBCreditShouldBeFound("order.greaterThanOrEqual=" + DEFAULT_ORDER);

        // Get all the tMDBCreditList where order is greater than or equal to UPDATED_ORDER
        defaultTMDBCreditShouldNotBeFound("order.greaterThanOrEqual=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order is less than or equal to DEFAULT_ORDER
        defaultTMDBCreditShouldBeFound("order.lessThanOrEqual=" + DEFAULT_ORDER);

        // Get all the tMDBCreditList where order is less than or equal to SMALLER_ORDER
        defaultTMDBCreditShouldNotBeFound("order.lessThanOrEqual=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order is less than DEFAULT_ORDER
        defaultTMDBCreditShouldNotBeFound("order.lessThan=" + DEFAULT_ORDER);

        // Get all the tMDBCreditList where order is less than UPDATED_ORDER
        defaultTMDBCreditShouldBeFound("order.lessThan=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllTMDBCreditsByOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        // Get all the tMDBCreditList where order is greater than DEFAULT_ORDER
        defaultTMDBCreditShouldNotBeFound("order.greaterThan=" + DEFAULT_ORDER);

        // Get all the tMDBCreditList where order is greater than SMALLER_ORDER
        defaultTMDBCreditShouldBeFound("order.greaterThan=" + SMALLER_ORDER);
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByPersonIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);
        TMDBPerson person = TMDBPersonResourceIT.createEntity(em);
        em.persist(person);
        em.flush();
        tMDBCredit.setPerson(person);
        tMDBCreditRepository.saveAndFlush(tMDBCredit);
        Long personId = person.getId();

        // Get all the tMDBCreditList where person equals to personId
        defaultTMDBCreditShouldBeFound("personId.equals=" + personId);

        // Get all the tMDBCreditList where person equals to personId + 1
        defaultTMDBCreditShouldNotBeFound("personId.equals=" + (personId + 1));
    }


    @Test
    @Transactional
    public void getAllTMDBCreditsByMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);
        TMDBMovie movie = TMDBMovieResourceIT.createEntity(em);
        em.persist(movie);
        em.flush();
        tMDBCredit.setMovie(movie);
        tMDBCreditRepository.saveAndFlush(tMDBCredit);
        Long movieId = movie.getId();

        // Get all the tMDBCreditList where movie equals to movieId
        defaultTMDBCreditShouldBeFound("movieId.equals=" + movieId);

        // Get all the tMDBCreditList where movie equals to movieId + 1
        defaultTMDBCreditShouldNotBeFound("movieId.equals=" + (movieId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTMDBCreditShouldBeFound(String filter) throws Exception {
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBCredit.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].character").value(hasItem(DEFAULT_CHARACTER)))
            .andExpect(jsonPath("$.[*].creditType").value(hasItem(DEFAULT_CREDIT_TYPE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].job").value(hasItem(DEFAULT_JOB)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));

        // Check, that the count call also returns 1
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTMDBCreditShouldNotBeFound(String filter) throws Exception {
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTMDBCredit() throws Exception {
        // Get the tMDBCredit
        restTMDBCreditMockMvc.perform(get("/api/tmdb-credits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTMDBCredit() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        int databaseSizeBeforeUpdate = tMDBCreditRepository.findAll().size();

        // Update the tMDBCredit
        TMDBCredit updatedTMDBCredit = tMDBCreditRepository.findById(tMDBCredit.getId()).get();
        // Disconnect from session so that the updates on updatedTMDBCredit are not directly saved in db
        em.detach(updatedTMDBCredit);
        updatedTMDBCredit
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER);
        TMDBCreditDTO tMDBCreditDTO = tMDBCreditMapper.toDto(updatedTMDBCredit);

        restTMDBCreditMockMvc.perform(put("/api/tmdb-credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBCreditDTO)))
            .andExpect(status().isOk());

        // Validate the TMDBCredit in the database
        List<TMDBCredit> tMDBCreditList = tMDBCreditRepository.findAll();
        assertThat(tMDBCreditList).hasSize(databaseSizeBeforeUpdate);
        TMDBCredit testTMDBCredit = tMDBCreditList.get(tMDBCreditList.size() - 1);
        assertThat(testTMDBCredit.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testTMDBCredit.getCharacter()).isEqualTo(UPDATED_CHARACTER);
        assertThat(testTMDBCredit.getCreditType()).isEqualTo(UPDATED_CREDIT_TYPE);
        assertThat(testTMDBCredit.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testTMDBCredit.getJob()).isEqualTo(UPDATED_JOB);
        assertThat(testTMDBCredit.getOrder()).isEqualTo(UPDATED_ORDER);

        // Validate the TMDBCredit in Elasticsearch
        verify(mockTMDBCreditSearchRepository, times(1)).save(testTMDBCredit);
    }

    @Test
    @Transactional
    public void updateNonExistingTMDBCredit() throws Exception {
        int databaseSizeBeforeUpdate = tMDBCreditRepository.findAll().size();

        // Create the TMDBCredit
        TMDBCreditDTO tMDBCreditDTO = tMDBCreditMapper.toDto(tMDBCredit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTMDBCreditMockMvc.perform(put("/api/tmdb-credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBCreditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBCredit in the database
        List<TMDBCredit> tMDBCreditList = tMDBCreditRepository.findAll();
        assertThat(tMDBCreditList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TMDBCredit in Elasticsearch
        verify(mockTMDBCreditSearchRepository, times(0)).save(tMDBCredit);
    }

    @Test
    @Transactional
    public void deleteTMDBCredit() throws Exception {
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);

        int databaseSizeBeforeDelete = tMDBCreditRepository.findAll().size();

        // Delete the tMDBCredit
        restTMDBCreditMockMvc.perform(delete("/api/tmdb-credits/{id}", tMDBCredit.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TMDBCredit> tMDBCreditList = tMDBCreditRepository.findAll();
        assertThat(tMDBCreditList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TMDBCredit in Elasticsearch
        verify(mockTMDBCreditSearchRepository, times(1)).deleteById(tMDBCredit.getId());
    }

    @Test
    @Transactional
    public void searchTMDBCredit() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tMDBCreditRepository.saveAndFlush(tMDBCredit);
        when(mockTMDBCreditSearchRepository.search(queryStringQuery("id:" + tMDBCredit.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(tMDBCredit), PageRequest.of(0, 1), 1));

        // Search the tMDBCredit
        restTMDBCreditMockMvc.perform(get("/api/_search/tmdb-credits?query=id:" + tMDBCredit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBCredit.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].character").value(hasItem(DEFAULT_CHARACTER)))
            .andExpect(jsonPath("$.[*].creditType").value(hasItem(DEFAULT_CREDIT_TYPE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].job").value(hasItem(DEFAULT_JOB)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));
    }
}
