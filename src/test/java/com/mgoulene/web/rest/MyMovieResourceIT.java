package com.mgoulene.web.rest;

import com.mgoulene.MymoviesApp;
import com.mgoulene.domain.MyMovie;
import com.mgoulene.domain.TMDBMovie;
import com.mgoulene.domain.User;
import com.mgoulene.repository.MyMovieRepository;
import com.mgoulene.repository.search.MyMovieSearchRepository;
import com.mgoulene.service.MyMovieService;
import com.mgoulene.service.dto.MyMovieDTO;
import com.mgoulene.service.mapper.MyMovieMapper;
import com.mgoulene.service.dto.MyMovieCriteria;
import com.mgoulene.service.MyMovieQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MyMovieResource} REST controller.
 */
@SpringBootTest(classes = MymoviesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class MyMovieResourceIT {

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final Integer DEFAULT_VOTE = 0;
    private static final Integer UPDATED_VOTE = 1;
    private static final Integer SMALLER_VOTE = 0 - 1;

    private static final LocalDate DEFAULT_VIEWED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VIEWED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_VIEWED_DATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private MyMovieRepository myMovieRepository;

    @Autowired
    private MyMovieMapper myMovieMapper;

    @Autowired
    private MyMovieService myMovieService;

    /**
     * This repository is mocked in the com.mgoulene.repository.search test package.
     *
     * @see com.mgoulene.repository.search.MyMovieSearchRepositoryMockConfiguration
     */
    @Autowired
    private MyMovieSearchRepository mockMyMovieSearchRepository;

    @Autowired
    private MyMovieQueryService myMovieQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMyMovieMockMvc;

    private MyMovie myMovie;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MyMovie createEntity(EntityManager em) {
        MyMovie myMovie = new MyMovie()
            .comments(DEFAULT_COMMENTS)
            .vote(DEFAULT_VOTE)
            .viewedDate(DEFAULT_VIEWED_DATE);
        return myMovie;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MyMovie createUpdatedEntity(EntityManager em) {
        MyMovie myMovie = new MyMovie()
            .comments(UPDATED_COMMENTS)
            .vote(UPDATED_VOTE)
            .viewedDate(UPDATED_VIEWED_DATE);
        return myMovie;
    }

    @BeforeEach
    public void initTest() {
        myMovie = createEntity(em);
    }

    @Test
    @Transactional
    public void createMyMovie() throws Exception {
        int databaseSizeBeforeCreate = myMovieRepository.findAll().size();
        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);
        restMyMovieMockMvc.perform(post("/api/my-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(myMovieDTO)))
            .andExpect(status().isCreated());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeCreate + 1);
        MyMovie testMyMovie = myMovieList.get(myMovieList.size() - 1);
        assertThat(testMyMovie.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testMyMovie.getVote()).isEqualTo(DEFAULT_VOTE);
        assertThat(testMyMovie.getViewedDate()).isEqualTo(DEFAULT_VIEWED_DATE);

        // Validate the MyMovie in Elasticsearch
        verify(mockMyMovieSearchRepository, times(1)).save(testMyMovie);
    }

    @Test
    @Transactional
    public void createMyMovieWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = myMovieRepository.findAll().size();

        // Create the MyMovie with an existing ID
        myMovie.setId(1L);
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMyMovieMockMvc.perform(post("/api/my-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(myMovieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeCreate);

        // Validate the MyMovie in Elasticsearch
        verify(mockMyMovieSearchRepository, times(0)).save(myMovie);
    }


    @Test
    @Transactional
    public void getAllMyMovies() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList
        restMyMovieMockMvc.perform(get("/api/my-movies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(myMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].vote").value(hasItem(DEFAULT_VOTE)))
            .andExpect(jsonPath("$.[*].viewedDate").value(hasItem(DEFAULT_VIEWED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getMyMovie() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get the myMovie
        restMyMovieMockMvc.perform(get("/api/my-movies/{id}", myMovie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(myMovie.getId().intValue()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.vote").value(DEFAULT_VOTE))
            .andExpect(jsonPath("$.viewedDate").value(DEFAULT_VIEWED_DATE.toString()));
    }


    @Test
    @Transactional
    public void getMyMoviesByIdFiltering() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        Long id = myMovie.getId();

        defaultMyMovieShouldBeFound("id.equals=" + id);
        defaultMyMovieShouldNotBeFound("id.notEquals=" + id);

        defaultMyMovieShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMyMovieShouldNotBeFound("id.greaterThan=" + id);

        defaultMyMovieShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMyMovieShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllMyMoviesByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments equals to DEFAULT_COMMENTS
        defaultMyMovieShouldBeFound("comments.equals=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments equals to UPDATED_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.equals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments not equals to DEFAULT_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.notEquals=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments not equals to UPDATED_COMMENTS
        defaultMyMovieShouldBeFound("comments.notEquals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments in DEFAULT_COMMENTS or UPDATED_COMMENTS
        defaultMyMovieShouldBeFound("comments.in=" + DEFAULT_COMMENTS + "," + UPDATED_COMMENTS);

        // Get all the myMovieList where comments equals to UPDATED_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.in=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments is not null
        defaultMyMovieShouldBeFound("comments.specified=true");

        // Get all the myMovieList where comments is null
        defaultMyMovieShouldNotBeFound("comments.specified=false");
    }
                @Test
    @Transactional
    public void getAllMyMoviesByCommentsContainsSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments contains DEFAULT_COMMENTS
        defaultMyMovieShouldBeFound("comments.contains=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments contains UPDATED_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.contains=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments does not contain DEFAULT_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.doesNotContain=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments does not contain UPDATED_COMMENTS
        defaultMyMovieShouldBeFound("comments.doesNotContain=" + UPDATED_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote equals to DEFAULT_VOTE
        defaultMyMovieShouldBeFound("vote.equals=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote equals to UPDATED_VOTE
        defaultMyMovieShouldNotBeFound("vote.equals=" + UPDATED_VOTE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote not equals to DEFAULT_VOTE
        defaultMyMovieShouldNotBeFound("vote.notEquals=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote not equals to UPDATED_VOTE
        defaultMyMovieShouldBeFound("vote.notEquals=" + UPDATED_VOTE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote in DEFAULT_VOTE or UPDATED_VOTE
        defaultMyMovieShouldBeFound("vote.in=" + DEFAULT_VOTE + "," + UPDATED_VOTE);

        // Get all the myMovieList where vote equals to UPDATED_VOTE
        defaultMyMovieShouldNotBeFound("vote.in=" + UPDATED_VOTE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is not null
        defaultMyMovieShouldBeFound("vote.specified=true");

        // Get all the myMovieList where vote is null
        defaultMyMovieShouldNotBeFound("vote.specified=false");
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is greater than or equal to DEFAULT_VOTE
        defaultMyMovieShouldBeFound("vote.greaterThanOrEqual=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is greater than or equal to (DEFAULT_VOTE + 1)
        defaultMyMovieShouldNotBeFound("vote.greaterThanOrEqual=" + (DEFAULT_VOTE + 1));
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is less than or equal to DEFAULT_VOTE
        defaultMyMovieShouldBeFound("vote.lessThanOrEqual=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is less than or equal to SMALLER_VOTE
        defaultMyMovieShouldNotBeFound("vote.lessThanOrEqual=" + SMALLER_VOTE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsLessThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is less than DEFAULT_VOTE
        defaultMyMovieShouldNotBeFound("vote.lessThan=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is less than (DEFAULT_VOTE + 1)
        defaultMyMovieShouldBeFound("vote.lessThan=" + (DEFAULT_VOTE + 1));
    }

    @Test
    @Transactional
    public void getAllMyMoviesByVoteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is greater than DEFAULT_VOTE
        defaultMyMovieShouldNotBeFound("vote.greaterThan=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is greater than SMALLER_VOTE
        defaultMyMovieShouldBeFound("vote.greaterThan=" + SMALLER_VOTE);
    }


    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate equals to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.equals=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate equals to UPDATED_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.equals=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate not equals to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.notEquals=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate not equals to UPDATED_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.notEquals=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate in DEFAULT_VIEWED_DATE or UPDATED_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.in=" + DEFAULT_VIEWED_DATE + "," + UPDATED_VIEWED_DATE);

        // Get all the myMovieList where viewedDate equals to UPDATED_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.in=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is not null
        defaultMyMovieShouldBeFound("viewedDate.specified=true");

        // Get all the myMovieList where viewedDate is null
        defaultMyMovieShouldNotBeFound("viewedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is greater than or equal to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.greaterThanOrEqual=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is greater than or equal to UPDATED_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.greaterThanOrEqual=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is less than or equal to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.lessThanOrEqual=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is less than or equal to SMALLER_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.lessThanOrEqual=" + SMALLER_VIEWED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is less than DEFAULT_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.lessThan=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is less than UPDATED_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.lessThan=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    public void getAllMyMoviesByViewedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is greater than DEFAULT_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.greaterThan=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is greater than SMALLER_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.greaterThan=" + SMALLER_VIEWED_DATE);
    }


    @Test
    @Transactional
    public void getAllMyMoviesByMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);
        TMDBMovie movie = TMDBMovieResourceIT.createEntity(em);
        em.persist(movie);
        em.flush();
        myMovie.setMovie(movie);
        myMovieRepository.saveAndFlush(myMovie);
        Long movieId = movie.getId();

        // Get all the myMovieList where movie equals to movieId
        defaultMyMovieShouldBeFound("movieId.equals=" + movieId);

        // Get all the myMovieList where movie equals to movieId + 1
        defaultMyMovieShouldNotBeFound("movieId.equals=" + (movieId + 1));
    }


    @Test
    @Transactional
    public void getAllMyMoviesByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        myMovie.setUser(user);
        myMovieRepository.saveAndFlush(myMovie);
        Long userId = user.getId();

        // Get all the myMovieList where user equals to userId
        defaultMyMovieShouldBeFound("userId.equals=" + userId);

        // Get all the myMovieList where user equals to userId + 1
        defaultMyMovieShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMyMovieShouldBeFound(String filter) throws Exception {
        restMyMovieMockMvc.perform(get("/api/my-movies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(myMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].vote").value(hasItem(DEFAULT_VOTE)))
            .andExpect(jsonPath("$.[*].viewedDate").value(hasItem(DEFAULT_VIEWED_DATE.toString())));

        // Check, that the count call also returns 1
        restMyMovieMockMvc.perform(get("/api/my-movies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMyMovieShouldNotBeFound(String filter) throws Exception {
        restMyMovieMockMvc.perform(get("/api/my-movies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMyMovieMockMvc.perform(get("/api/my-movies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingMyMovie() throws Exception {
        // Get the myMovie
        restMyMovieMockMvc.perform(get("/api/my-movies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMyMovie() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();

        // Update the myMovie
        MyMovie updatedMyMovie = myMovieRepository.findById(myMovie.getId()).get();
        // Disconnect from session so that the updates on updatedMyMovie are not directly saved in db
        em.detach(updatedMyMovie);
        updatedMyMovie
            .comments(UPDATED_COMMENTS)
            .vote(UPDATED_VOTE)
            .viewedDate(UPDATED_VIEWED_DATE);
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(updatedMyMovie);

        restMyMovieMockMvc.perform(put("/api/my-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(myMovieDTO)))
            .andExpect(status().isOk());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
        MyMovie testMyMovie = myMovieList.get(myMovieList.size() - 1);
        assertThat(testMyMovie.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testMyMovie.getVote()).isEqualTo(UPDATED_VOTE);
        assertThat(testMyMovie.getViewedDate()).isEqualTo(UPDATED_VIEWED_DATE);

        // Validate the MyMovie in Elasticsearch
        verify(mockMyMovieSearchRepository, times(1)).save(testMyMovie);
    }

    @Test
    @Transactional
    public void updateNonExistingMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMyMovieMockMvc.perform(put("/api/my-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(myMovieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);

        // Validate the MyMovie in Elasticsearch
        verify(mockMyMovieSearchRepository, times(0)).save(myMovie);
    }

    @Test
    @Transactional
    public void deleteMyMovie() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        int databaseSizeBeforeDelete = myMovieRepository.findAll().size();

        // Delete the myMovie
        restMyMovieMockMvc.perform(delete("/api/my-movies/{id}", myMovie.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the MyMovie in Elasticsearch
        verify(mockMyMovieSearchRepository, times(1)).deleteById(myMovie.getId());
    }

    @Test
    @Transactional
    public void searchMyMovie() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);
        when(mockMyMovieSearchRepository.search(queryStringQuery("id:" + myMovie.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(myMovie), PageRequest.of(0, 1), 1));

        // Search the myMovie
        restMyMovieMockMvc.perform(get("/api/_search/my-movies?query=id:" + myMovie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(myMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].vote").value(hasItem(DEFAULT_VOTE)))
            .andExpect(jsonPath("$.[*].viewedDate").value(hasItem(DEFAULT_VIEWED_DATE.toString())));
    }
}
