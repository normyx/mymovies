package com.mgoulene.web.rest;

import com.mgoulene.MymoviesApp;
import com.mgoulene.domain.TMDBPicture;
import com.mgoulene.repository.TMDBPictureRepository;
import com.mgoulene.repository.search.TMDBPictureSearchRepository;
import com.mgoulene.service.TMDBPictureService;
import com.mgoulene.service.dto.TMDBPictureDTO;
import com.mgoulene.service.mapper.TMDBPictureMapper;
import com.mgoulene.service.dto.TMDBPictureCriteria;
import com.mgoulene.service.TMDBPictureQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TMDBPictureResource} REST controller.
 */
@SpringBootTest(classes = MymoviesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TMDBPictureResourceIT {

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PICTURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PICTURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PICTURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PICTURE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_PICTURE_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_PICTURE_SIZE = "BBBBBBBBBB";

    @Autowired
    private TMDBPictureRepository tMDBPictureRepository;

    @Autowired
    private TMDBPictureMapper tMDBPictureMapper;

    @Autowired
    private TMDBPictureService tMDBPictureService;

    /**
     * This repository is mocked in the com.mgoulene.repository.search test package.
     *
     * @see com.mgoulene.repository.search.TMDBPictureSearchRepositoryMockConfiguration
     */
    @Autowired
    private TMDBPictureSearchRepository mockTMDBPictureSearchRepository;

    @Autowired
    private TMDBPictureQueryService tMDBPictureQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTMDBPictureMockMvc;

    private TMDBPicture tMDBPicture;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBPicture createEntity(EntityManager em) {
        TMDBPicture tMDBPicture = new TMDBPicture()
            .tmdbId(DEFAULT_TMDB_ID)
            .picture(DEFAULT_PICTURE)
            .pictureContentType(DEFAULT_PICTURE_CONTENT_TYPE)
            .pictureSize(DEFAULT_PICTURE_SIZE);
        return tMDBPicture;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBPicture createUpdatedEntity(EntityManager em) {
        TMDBPicture tMDBPicture = new TMDBPicture()
            .tmdbId(UPDATED_TMDB_ID)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .pictureSize(UPDATED_PICTURE_SIZE);
        return tMDBPicture;
    }

    @BeforeEach
    public void initTest() {
        tMDBPicture = createEntity(em);
    }

    @Test
    @Transactional
    public void createTMDBPicture() throws Exception {
        int databaseSizeBeforeCreate = tMDBPictureRepository.findAll().size();
        // Create the TMDBPicture
        TMDBPictureDTO tMDBPictureDTO = tMDBPictureMapper.toDto(tMDBPicture);
        restTMDBPictureMockMvc.perform(post("/api/tmdb-pictures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPictureDTO)))
            .andExpect(status().isCreated());

        // Validate the TMDBPicture in the database
        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeCreate + 1);
        TMDBPicture testTMDBPicture = tMDBPictureList.get(tMDBPictureList.size() - 1);
        assertThat(testTMDBPicture.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testTMDBPicture.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testTMDBPicture.getPictureContentType()).isEqualTo(DEFAULT_PICTURE_CONTENT_TYPE);
        assertThat(testTMDBPicture.getPictureSize()).isEqualTo(DEFAULT_PICTURE_SIZE);

        // Validate the TMDBPicture in Elasticsearch
        verify(mockTMDBPictureSearchRepository, times(1)).save(testTMDBPicture);
    }

    @Test
    @Transactional
    public void createTMDBPictureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tMDBPictureRepository.findAll().size();

        // Create the TMDBPicture with an existing ID
        tMDBPicture.setId(1L);
        TMDBPictureDTO tMDBPictureDTO = tMDBPictureMapper.toDto(tMDBPicture);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTMDBPictureMockMvc.perform(post("/api/tmdb-pictures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPictureDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBPicture in the database
        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeCreate);

        // Validate the TMDBPicture in Elasticsearch
        verify(mockTMDBPictureSearchRepository, times(0)).save(tMDBPicture);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBPictureRepository.findAll().size();
        // set the field null
        tMDBPicture.setTmdbId(null);

        // Create the TMDBPicture, which fails.
        TMDBPictureDTO tMDBPictureDTO = tMDBPictureMapper.toDto(tMDBPicture);


        restTMDBPictureMockMvc.perform(post("/api/tmdb-pictures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPictureDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPictureSizeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBPictureRepository.findAll().size();
        // set the field null
        tMDBPicture.setPictureSize(null);

        // Create the TMDBPicture, which fails.
        TMDBPictureDTO tMDBPictureDTO = tMDBPictureMapper.toDto(tMDBPicture);


        restTMDBPictureMockMvc.perform(post("/api/tmdb-pictures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPictureDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTMDBPictures() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBPicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].pictureSize").value(hasItem(DEFAULT_PICTURE_SIZE)));
    }
    
    @Test
    @Transactional
    public void getTMDBPicture() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get the tMDBPicture
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures/{id}", tMDBPicture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tMDBPicture.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.pictureContentType").value(DEFAULT_PICTURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.picture").value(Base64Utils.encodeToString(DEFAULT_PICTURE)))
            .andExpect(jsonPath("$.pictureSize").value(DEFAULT_PICTURE_SIZE));
    }


    @Test
    @Transactional
    public void getTMDBPicturesByIdFiltering() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        Long id = tMDBPicture.getId();

        defaultTMDBPictureShouldBeFound("id.equals=" + id);
        defaultTMDBPictureShouldNotBeFound("id.notEquals=" + id);

        defaultTMDBPictureShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTMDBPictureShouldNotBeFound("id.greaterThan=" + id);

        defaultTMDBPictureShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTMDBPictureShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTMDBPicturesByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where tmdbId equals to DEFAULT_TMDB_ID
        defaultTMDBPictureShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPictureList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBPictureShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultTMDBPictureShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPictureList where tmdbId not equals to UPDATED_TMDB_ID
        defaultTMDBPictureShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultTMDBPictureShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the tMDBPictureList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBPictureShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where tmdbId is not null
        defaultTMDBPictureShouldBeFound("tmdbId.specified=true");

        // Get all the tMDBPictureList where tmdbId is null
        defaultTMDBPictureShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPicturesByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where tmdbId contains DEFAULT_TMDB_ID
        defaultTMDBPictureShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPictureList where tmdbId contains UPDATED_TMDB_ID
        defaultTMDBPictureShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultTMDBPictureShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPictureList where tmdbId does not contain UPDATED_TMDB_ID
        defaultTMDBPictureShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllTMDBPicturesByPictureSizeIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where pictureSize equals to DEFAULT_PICTURE_SIZE
        defaultTMDBPictureShouldBeFound("pictureSize.equals=" + DEFAULT_PICTURE_SIZE);

        // Get all the tMDBPictureList where pictureSize equals to UPDATED_PICTURE_SIZE
        defaultTMDBPictureShouldNotBeFound("pictureSize.equals=" + UPDATED_PICTURE_SIZE);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByPictureSizeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where pictureSize not equals to DEFAULT_PICTURE_SIZE
        defaultTMDBPictureShouldNotBeFound("pictureSize.notEquals=" + DEFAULT_PICTURE_SIZE);

        // Get all the tMDBPictureList where pictureSize not equals to UPDATED_PICTURE_SIZE
        defaultTMDBPictureShouldBeFound("pictureSize.notEquals=" + UPDATED_PICTURE_SIZE);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByPictureSizeIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where pictureSize in DEFAULT_PICTURE_SIZE or UPDATED_PICTURE_SIZE
        defaultTMDBPictureShouldBeFound("pictureSize.in=" + DEFAULT_PICTURE_SIZE + "," + UPDATED_PICTURE_SIZE);

        // Get all the tMDBPictureList where pictureSize equals to UPDATED_PICTURE_SIZE
        defaultTMDBPictureShouldNotBeFound("pictureSize.in=" + UPDATED_PICTURE_SIZE);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByPictureSizeIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where pictureSize is not null
        defaultTMDBPictureShouldBeFound("pictureSize.specified=true");

        // Get all the tMDBPictureList where pictureSize is null
        defaultTMDBPictureShouldNotBeFound("pictureSize.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPicturesByPictureSizeContainsSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where pictureSize contains DEFAULT_PICTURE_SIZE
        defaultTMDBPictureShouldBeFound("pictureSize.contains=" + DEFAULT_PICTURE_SIZE);

        // Get all the tMDBPictureList where pictureSize contains UPDATED_PICTURE_SIZE
        defaultTMDBPictureShouldNotBeFound("pictureSize.contains=" + UPDATED_PICTURE_SIZE);
    }

    @Test
    @Transactional
    public void getAllTMDBPicturesByPictureSizeNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        // Get all the tMDBPictureList where pictureSize does not contain DEFAULT_PICTURE_SIZE
        defaultTMDBPictureShouldNotBeFound("pictureSize.doesNotContain=" + DEFAULT_PICTURE_SIZE);

        // Get all the tMDBPictureList where pictureSize does not contain UPDATED_PICTURE_SIZE
        defaultTMDBPictureShouldBeFound("pictureSize.doesNotContain=" + UPDATED_PICTURE_SIZE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTMDBPictureShouldBeFound(String filter) throws Exception {
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBPicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].pictureSize").value(hasItem(DEFAULT_PICTURE_SIZE)));

        // Check, that the count call also returns 1
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTMDBPictureShouldNotBeFound(String filter) throws Exception {
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTMDBPicture() throws Exception {
        // Get the tMDBPicture
        restTMDBPictureMockMvc.perform(get("/api/tmdb-pictures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTMDBPicture() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        int databaseSizeBeforeUpdate = tMDBPictureRepository.findAll().size();

        // Update the tMDBPicture
        TMDBPicture updatedTMDBPicture = tMDBPictureRepository.findById(tMDBPicture.getId()).get();
        // Disconnect from session so that the updates on updatedTMDBPicture are not directly saved in db
        em.detach(updatedTMDBPicture);
        updatedTMDBPicture
            .tmdbId(UPDATED_TMDB_ID)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .pictureSize(UPDATED_PICTURE_SIZE);
        TMDBPictureDTO tMDBPictureDTO = tMDBPictureMapper.toDto(updatedTMDBPicture);

        restTMDBPictureMockMvc.perform(put("/api/tmdb-pictures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPictureDTO)))
            .andExpect(status().isOk());

        // Validate the TMDBPicture in the database
        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeUpdate);
        TMDBPicture testTMDBPicture = tMDBPictureList.get(tMDBPictureList.size() - 1);
        assertThat(testTMDBPicture.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testTMDBPicture.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testTMDBPicture.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testTMDBPicture.getPictureSize()).isEqualTo(UPDATED_PICTURE_SIZE);

        // Validate the TMDBPicture in Elasticsearch
        verify(mockTMDBPictureSearchRepository, times(1)).save(testTMDBPicture);
    }

    @Test
    @Transactional
    public void updateNonExistingTMDBPicture() throws Exception {
        int databaseSizeBeforeUpdate = tMDBPictureRepository.findAll().size();

        // Create the TMDBPicture
        TMDBPictureDTO tMDBPictureDTO = tMDBPictureMapper.toDto(tMDBPicture);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTMDBPictureMockMvc.perform(put("/api/tmdb-pictures")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPictureDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBPicture in the database
        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TMDBPicture in Elasticsearch
        verify(mockTMDBPictureSearchRepository, times(0)).save(tMDBPicture);
    }

    @Test
    @Transactional
    public void deleteTMDBPicture() throws Exception {
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);

        int databaseSizeBeforeDelete = tMDBPictureRepository.findAll().size();

        // Delete the tMDBPicture
        restTMDBPictureMockMvc.perform(delete("/api/tmdb-pictures/{id}", tMDBPicture.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TMDBPicture> tMDBPictureList = tMDBPictureRepository.findAll();
        assertThat(tMDBPictureList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TMDBPicture in Elasticsearch
        verify(mockTMDBPictureSearchRepository, times(1)).deleteById(tMDBPicture.getId());
    }

    @Test
    @Transactional
    public void searchTMDBPicture() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tMDBPictureRepository.saveAndFlush(tMDBPicture);
        when(mockTMDBPictureSearchRepository.search(queryStringQuery("id:" + tMDBPicture.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(tMDBPicture), PageRequest.of(0, 1), 1));

        // Search the tMDBPicture
        restTMDBPictureMockMvc.perform(get("/api/_search/tmdb-pictures?query=id:" + tMDBPicture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBPicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].pictureSize").value(hasItem(DEFAULT_PICTURE_SIZE)));
    }
}
