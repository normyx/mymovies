package com.mgoulene.web.rest;

import com.mgoulene.MymoviesApp;
import com.mgoulene.domain.TMDBPerson;
import com.mgoulene.repository.TMDBPersonRepository;
import com.mgoulene.repository.search.TMDBPersonSearchRepository;
import com.mgoulene.service.TMDBPersonService;
import com.mgoulene.service.dto.TMDBPersonDTO;
import com.mgoulene.service.mapper.TMDBPersonMapper;
import com.mgoulene.service.dto.TMDBPersonCriteria;
import com.mgoulene.service.TMDBPersonQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TMDBPersonResource} REST controller.
 */
@SpringBootTest(classes = MymoviesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TMDBPersonResourceIT {

    private static final Integer DEFAULT_TMDB_ID = 1;
    private static final Integer UPDATED_TMDB_ID = 2;
    private static final Integer SMALLER_TMDB_ID = 1 - 1;

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTHDAY = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DEATHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DEATHDAY = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DEATHDAY = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_AKA = "AAAAAAAAAA";
    private static final String UPDATED_AKA = "BBBBBBBBBB";

    private static final Integer DEFAULT_GENDER = 1;
    private static final Integer UPDATED_GENDER = 2;
    private static final Integer SMALLER_GENDER = 1 - 1;

    private static final String DEFAULT_BIOGRAPHY = "AAAAAAAAAA";
    private static final String UPDATED_BIOGRAPHY = "BBBBBBBBBB";

    private static final String DEFAULT_PLACE_OF_BIRTH = "AAAAAAAAAA";
    private static final String UPDATED_PLACE_OF_BIRTH = "BBBBBBBBBB";

    private static final String DEFAULT_HOMEPAGE = "AAAAAAAAAA";
    private static final String UPDATED_HOMEPAGE = "BBBBBBBBBB";

    @Autowired
    private TMDBPersonRepository tMDBPersonRepository;

    @Autowired
    private TMDBPersonMapper tMDBPersonMapper;

    @Autowired
    private TMDBPersonService tMDBPersonService;

    /**
     * This repository is mocked in the com.mgoulene.repository.search test package.
     *
     * @see com.mgoulene.repository.search.TMDBPersonSearchRepositoryMockConfiguration
     */
    @Autowired
    private TMDBPersonSearchRepository mockTMDBPersonSearchRepository;

    @Autowired
    private TMDBPersonQueryService tMDBPersonQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTMDBPersonMockMvc;

    private TMDBPerson tMDBPerson;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBPerson createEntity(EntityManager em) {
        TMDBPerson tMDBPerson = new TMDBPerson()
            .tmdbId(DEFAULT_TMDB_ID)
            .birthday(DEFAULT_BIRTHDAY)
            .deathday(DEFAULT_DEATHDAY)
            .name(DEFAULT_NAME)
            .aka(DEFAULT_AKA)
            .gender(DEFAULT_GENDER)
            .biography(DEFAULT_BIOGRAPHY)
            .placeOfBirth(DEFAULT_PLACE_OF_BIRTH)
            .homepage(DEFAULT_HOMEPAGE);
        return tMDBPerson;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBPerson createUpdatedEntity(EntityManager em) {
        TMDBPerson tMDBPerson = new TMDBPerson()
            .tmdbId(UPDATED_TMDB_ID)
            .birthday(UPDATED_BIRTHDAY)
            .deathday(UPDATED_DEATHDAY)
            .name(UPDATED_NAME)
            .aka(UPDATED_AKA)
            .gender(UPDATED_GENDER)
            .biography(UPDATED_BIOGRAPHY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .homepage(UPDATED_HOMEPAGE);
        return tMDBPerson;
    }

    @BeforeEach
    public void initTest() {
        tMDBPerson = createEntity(em);
    }

    @Test
    @Transactional
    public void createTMDBPerson() throws Exception {
        int databaseSizeBeforeCreate = tMDBPersonRepository.findAll().size();
        // Create the TMDBPerson
        TMDBPersonDTO tMDBPersonDTO = tMDBPersonMapper.toDto(tMDBPerson);
        restTMDBPersonMockMvc.perform(post("/api/tmdb-people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPersonDTO)))
            .andExpect(status().isCreated());

        // Validate the TMDBPerson in the database
        List<TMDBPerson> tMDBPersonList = tMDBPersonRepository.findAll();
        assertThat(tMDBPersonList).hasSize(databaseSizeBeforeCreate + 1);
        TMDBPerson testTMDBPerson = tMDBPersonList.get(tMDBPersonList.size() - 1);
        assertThat(testTMDBPerson.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testTMDBPerson.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testTMDBPerson.getDeathday()).isEqualTo(DEFAULT_DEATHDAY);
        assertThat(testTMDBPerson.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTMDBPerson.getAka()).isEqualTo(DEFAULT_AKA);
        assertThat(testTMDBPerson.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testTMDBPerson.getBiography()).isEqualTo(DEFAULT_BIOGRAPHY);
        assertThat(testTMDBPerson.getPlaceOfBirth()).isEqualTo(DEFAULT_PLACE_OF_BIRTH);
        assertThat(testTMDBPerson.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);

        // Validate the TMDBPerson in Elasticsearch
        verify(mockTMDBPersonSearchRepository, times(1)).save(testTMDBPerson);
    }

    @Test
    @Transactional
    public void createTMDBPersonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tMDBPersonRepository.findAll().size();

        // Create the TMDBPerson with an existing ID
        tMDBPerson.setId(1L);
        TMDBPersonDTO tMDBPersonDTO = tMDBPersonMapper.toDto(tMDBPerson);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTMDBPersonMockMvc.perform(post("/api/tmdb-people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPersonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBPerson in the database
        List<TMDBPerson> tMDBPersonList = tMDBPersonRepository.findAll();
        assertThat(tMDBPersonList).hasSize(databaseSizeBeforeCreate);

        // Validate the TMDBPerson in Elasticsearch
        verify(mockTMDBPersonSearchRepository, times(0)).save(tMDBPerson);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBPersonRepository.findAll().size();
        // set the field null
        tMDBPerson.setTmdbId(null);

        // Create the TMDBPerson, which fails.
        TMDBPersonDTO tMDBPersonDTO = tMDBPersonMapper.toDto(tMDBPerson);


        restTMDBPersonMockMvc.perform(post("/api/tmdb-people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPersonDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBPerson> tMDBPersonList = tMDBPersonRepository.findAll();
        assertThat(tMDBPersonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTMDBPeople() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBPerson.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].deathday").value(hasItem(DEFAULT_DEATHDAY.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].aka").value(hasItem(DEFAULT_AKA)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].biography").value(hasItem(DEFAULT_BIOGRAPHY)))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)));
    }
    
    @Test
    @Transactional
    public void getTMDBPerson() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get the tMDBPerson
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people/{id}", tMDBPerson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tMDBPerson.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.deathday").value(DEFAULT_DEATHDAY.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.aka").value(DEFAULT_AKA))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.biography").value(DEFAULT_BIOGRAPHY))
            .andExpect(jsonPath("$.placeOfBirth").value(DEFAULT_PLACE_OF_BIRTH))
            .andExpect(jsonPath("$.homepage").value(DEFAULT_HOMEPAGE));
    }


    @Test
    @Transactional
    public void getTMDBPeopleByIdFiltering() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        Long id = tMDBPerson.getId();

        defaultTMDBPersonShouldBeFound("id.equals=" + id);
        defaultTMDBPersonShouldNotBeFound("id.notEquals=" + id);

        defaultTMDBPersonShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTMDBPersonShouldNotBeFound("id.greaterThan=" + id);

        defaultTMDBPersonShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTMDBPersonShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId equals to DEFAULT_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId not equals to UPDATED_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId is not null
        defaultTMDBPersonShouldBeFound("tmdbId.specified=true");

        // Get all the tMDBPersonList where tmdbId is null
        defaultTMDBPersonShouldNotBeFound("tmdbId.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId is greater than or equal to DEFAULT_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.greaterThanOrEqual=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId is greater than or equal to UPDATED_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.greaterThanOrEqual=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId is less than or equal to DEFAULT_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.lessThanOrEqual=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId is less than or equal to SMALLER_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.lessThanOrEqual=" + SMALLER_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId is less than DEFAULT_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.lessThan=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId is less than UPDATED_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.lessThan=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByTmdbIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where tmdbId is greater than DEFAULT_TMDB_ID
        defaultTMDBPersonShouldNotBeFound("tmdbId.greaterThan=" + DEFAULT_TMDB_ID);

        // Get all the tMDBPersonList where tmdbId is greater than SMALLER_TMDB_ID
        defaultTMDBPersonShouldBeFound("tmdbId.greaterThan=" + SMALLER_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday equals to DEFAULT_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.equals=" + DEFAULT_BIRTHDAY);

        // Get all the tMDBPersonList where birthday equals to UPDATED_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.equals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday not equals to DEFAULT_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.notEquals=" + DEFAULT_BIRTHDAY);

        // Get all the tMDBPersonList where birthday not equals to UPDATED_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.notEquals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday in DEFAULT_BIRTHDAY or UPDATED_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.in=" + DEFAULT_BIRTHDAY + "," + UPDATED_BIRTHDAY);

        // Get all the tMDBPersonList where birthday equals to UPDATED_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.in=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday is not null
        defaultTMDBPersonShouldBeFound("birthday.specified=true");

        // Get all the tMDBPersonList where birthday is null
        defaultTMDBPersonShouldNotBeFound("birthday.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday is greater than or equal to DEFAULT_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.greaterThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the tMDBPersonList where birthday is greater than or equal to UPDATED_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.greaterThanOrEqual=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday is less than or equal to DEFAULT_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.lessThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the tMDBPersonList where birthday is less than or equal to SMALLER_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.lessThanOrEqual=" + SMALLER_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday is less than DEFAULT_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.lessThan=" + DEFAULT_BIRTHDAY);

        // Get all the tMDBPersonList where birthday is less than UPDATED_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.lessThan=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBirthdayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where birthday is greater than DEFAULT_BIRTHDAY
        defaultTMDBPersonShouldNotBeFound("birthday.greaterThan=" + DEFAULT_BIRTHDAY);

        // Get all the tMDBPersonList where birthday is greater than SMALLER_BIRTHDAY
        defaultTMDBPersonShouldBeFound("birthday.greaterThan=" + SMALLER_BIRTHDAY);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday equals to DEFAULT_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.equals=" + DEFAULT_DEATHDAY);

        // Get all the tMDBPersonList where deathday equals to UPDATED_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.equals=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday not equals to DEFAULT_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.notEquals=" + DEFAULT_DEATHDAY);

        // Get all the tMDBPersonList where deathday not equals to UPDATED_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.notEquals=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday in DEFAULT_DEATHDAY or UPDATED_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.in=" + DEFAULT_DEATHDAY + "," + UPDATED_DEATHDAY);

        // Get all the tMDBPersonList where deathday equals to UPDATED_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.in=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday is not null
        defaultTMDBPersonShouldBeFound("deathday.specified=true");

        // Get all the tMDBPersonList where deathday is null
        defaultTMDBPersonShouldNotBeFound("deathday.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday is greater than or equal to DEFAULT_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.greaterThanOrEqual=" + DEFAULT_DEATHDAY);

        // Get all the tMDBPersonList where deathday is greater than or equal to UPDATED_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.greaterThanOrEqual=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday is less than or equal to DEFAULT_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.lessThanOrEqual=" + DEFAULT_DEATHDAY);

        // Get all the tMDBPersonList where deathday is less than or equal to SMALLER_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.lessThanOrEqual=" + SMALLER_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday is less than DEFAULT_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.lessThan=" + DEFAULT_DEATHDAY);

        // Get all the tMDBPersonList where deathday is less than UPDATED_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.lessThan=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByDeathdayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where deathday is greater than DEFAULT_DEATHDAY
        defaultTMDBPersonShouldNotBeFound("deathday.greaterThan=" + DEFAULT_DEATHDAY);

        // Get all the tMDBPersonList where deathday is greater than SMALLER_DEATHDAY
        defaultTMDBPersonShouldBeFound("deathday.greaterThan=" + SMALLER_DEATHDAY);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where name equals to DEFAULT_NAME
        defaultTMDBPersonShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the tMDBPersonList where name equals to UPDATED_NAME
        defaultTMDBPersonShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where name not equals to DEFAULT_NAME
        defaultTMDBPersonShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the tMDBPersonList where name not equals to UPDATED_NAME
        defaultTMDBPersonShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByNameIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where name in DEFAULT_NAME or UPDATED_NAME
        defaultTMDBPersonShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the tMDBPersonList where name equals to UPDATED_NAME
        defaultTMDBPersonShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where name is not null
        defaultTMDBPersonShouldBeFound("name.specified=true");

        // Get all the tMDBPersonList where name is null
        defaultTMDBPersonShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPeopleByNameContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where name contains DEFAULT_NAME
        defaultTMDBPersonShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the tMDBPersonList where name contains UPDATED_NAME
        defaultTMDBPersonShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByNameNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where name does not contain DEFAULT_NAME
        defaultTMDBPersonShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the tMDBPersonList where name does not contain UPDATED_NAME
        defaultTMDBPersonShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByAkaIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where aka equals to DEFAULT_AKA
        defaultTMDBPersonShouldBeFound("aka.equals=" + DEFAULT_AKA);

        // Get all the tMDBPersonList where aka equals to UPDATED_AKA
        defaultTMDBPersonShouldNotBeFound("aka.equals=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByAkaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where aka not equals to DEFAULT_AKA
        defaultTMDBPersonShouldNotBeFound("aka.notEquals=" + DEFAULT_AKA);

        // Get all the tMDBPersonList where aka not equals to UPDATED_AKA
        defaultTMDBPersonShouldBeFound("aka.notEquals=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByAkaIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where aka in DEFAULT_AKA or UPDATED_AKA
        defaultTMDBPersonShouldBeFound("aka.in=" + DEFAULT_AKA + "," + UPDATED_AKA);

        // Get all the tMDBPersonList where aka equals to UPDATED_AKA
        defaultTMDBPersonShouldNotBeFound("aka.in=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByAkaIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where aka is not null
        defaultTMDBPersonShouldBeFound("aka.specified=true");

        // Get all the tMDBPersonList where aka is null
        defaultTMDBPersonShouldNotBeFound("aka.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPeopleByAkaContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where aka contains DEFAULT_AKA
        defaultTMDBPersonShouldBeFound("aka.contains=" + DEFAULT_AKA);

        // Get all the tMDBPersonList where aka contains UPDATED_AKA
        defaultTMDBPersonShouldNotBeFound("aka.contains=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByAkaNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where aka does not contain DEFAULT_AKA
        defaultTMDBPersonShouldNotBeFound("aka.doesNotContain=" + DEFAULT_AKA);

        // Get all the tMDBPersonList where aka does not contain UPDATED_AKA
        defaultTMDBPersonShouldBeFound("aka.doesNotContain=" + UPDATED_AKA);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender equals to DEFAULT_GENDER
        defaultTMDBPersonShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the tMDBPersonList where gender equals to UPDATED_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender not equals to DEFAULT_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.notEquals=" + DEFAULT_GENDER);

        // Get all the tMDBPersonList where gender not equals to UPDATED_GENDER
        defaultTMDBPersonShouldBeFound("gender.notEquals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultTMDBPersonShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the tMDBPersonList where gender equals to UPDATED_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender is not null
        defaultTMDBPersonShouldBeFound("gender.specified=true");

        // Get all the tMDBPersonList where gender is null
        defaultTMDBPersonShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender is greater than or equal to DEFAULT_GENDER
        defaultTMDBPersonShouldBeFound("gender.greaterThanOrEqual=" + DEFAULT_GENDER);

        // Get all the tMDBPersonList where gender is greater than or equal to UPDATED_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.greaterThanOrEqual=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender is less than or equal to DEFAULT_GENDER
        defaultTMDBPersonShouldBeFound("gender.lessThanOrEqual=" + DEFAULT_GENDER);

        // Get all the tMDBPersonList where gender is less than or equal to SMALLER_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.lessThanOrEqual=" + SMALLER_GENDER);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender is less than DEFAULT_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.lessThan=" + DEFAULT_GENDER);

        // Get all the tMDBPersonList where gender is less than UPDATED_GENDER
        defaultTMDBPersonShouldBeFound("gender.lessThan=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByGenderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where gender is greater than DEFAULT_GENDER
        defaultTMDBPersonShouldNotBeFound("gender.greaterThan=" + DEFAULT_GENDER);

        // Get all the tMDBPersonList where gender is greater than SMALLER_GENDER
        defaultTMDBPersonShouldBeFound("gender.greaterThan=" + SMALLER_GENDER);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByBiographyIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where biography equals to DEFAULT_BIOGRAPHY
        defaultTMDBPersonShouldBeFound("biography.equals=" + DEFAULT_BIOGRAPHY);

        // Get all the tMDBPersonList where biography equals to UPDATED_BIOGRAPHY
        defaultTMDBPersonShouldNotBeFound("biography.equals=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBiographyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where biography not equals to DEFAULT_BIOGRAPHY
        defaultTMDBPersonShouldNotBeFound("biography.notEquals=" + DEFAULT_BIOGRAPHY);

        // Get all the tMDBPersonList where biography not equals to UPDATED_BIOGRAPHY
        defaultTMDBPersonShouldBeFound("biography.notEquals=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBiographyIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where biography in DEFAULT_BIOGRAPHY or UPDATED_BIOGRAPHY
        defaultTMDBPersonShouldBeFound("biography.in=" + DEFAULT_BIOGRAPHY + "," + UPDATED_BIOGRAPHY);

        // Get all the tMDBPersonList where biography equals to UPDATED_BIOGRAPHY
        defaultTMDBPersonShouldNotBeFound("biography.in=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBiographyIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where biography is not null
        defaultTMDBPersonShouldBeFound("biography.specified=true");

        // Get all the tMDBPersonList where biography is null
        defaultTMDBPersonShouldNotBeFound("biography.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPeopleByBiographyContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where biography contains DEFAULT_BIOGRAPHY
        defaultTMDBPersonShouldBeFound("biography.contains=" + DEFAULT_BIOGRAPHY);

        // Get all the tMDBPersonList where biography contains UPDATED_BIOGRAPHY
        defaultTMDBPersonShouldNotBeFound("biography.contains=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByBiographyNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where biography does not contain DEFAULT_BIOGRAPHY
        defaultTMDBPersonShouldNotBeFound("biography.doesNotContain=" + DEFAULT_BIOGRAPHY);

        // Get all the tMDBPersonList where biography does not contain UPDATED_BIOGRAPHY
        defaultTMDBPersonShouldBeFound("biography.doesNotContain=" + UPDATED_BIOGRAPHY);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByPlaceOfBirthIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where placeOfBirth equals to DEFAULT_PLACE_OF_BIRTH
        defaultTMDBPersonShouldBeFound("placeOfBirth.equals=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the tMDBPersonList where placeOfBirth equals to UPDATED_PLACE_OF_BIRTH
        defaultTMDBPersonShouldNotBeFound("placeOfBirth.equals=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByPlaceOfBirthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where placeOfBirth not equals to DEFAULT_PLACE_OF_BIRTH
        defaultTMDBPersonShouldNotBeFound("placeOfBirth.notEquals=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the tMDBPersonList where placeOfBirth not equals to UPDATED_PLACE_OF_BIRTH
        defaultTMDBPersonShouldBeFound("placeOfBirth.notEquals=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByPlaceOfBirthIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where placeOfBirth in DEFAULT_PLACE_OF_BIRTH or UPDATED_PLACE_OF_BIRTH
        defaultTMDBPersonShouldBeFound("placeOfBirth.in=" + DEFAULT_PLACE_OF_BIRTH + "," + UPDATED_PLACE_OF_BIRTH);

        // Get all the tMDBPersonList where placeOfBirth equals to UPDATED_PLACE_OF_BIRTH
        defaultTMDBPersonShouldNotBeFound("placeOfBirth.in=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByPlaceOfBirthIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where placeOfBirth is not null
        defaultTMDBPersonShouldBeFound("placeOfBirth.specified=true");

        // Get all the tMDBPersonList where placeOfBirth is null
        defaultTMDBPersonShouldNotBeFound("placeOfBirth.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPeopleByPlaceOfBirthContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where placeOfBirth contains DEFAULT_PLACE_OF_BIRTH
        defaultTMDBPersonShouldBeFound("placeOfBirth.contains=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the tMDBPersonList where placeOfBirth contains UPDATED_PLACE_OF_BIRTH
        defaultTMDBPersonShouldNotBeFound("placeOfBirth.contains=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByPlaceOfBirthNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where placeOfBirth does not contain DEFAULT_PLACE_OF_BIRTH
        defaultTMDBPersonShouldNotBeFound("placeOfBirth.doesNotContain=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the tMDBPersonList where placeOfBirth does not contain UPDATED_PLACE_OF_BIRTH
        defaultTMDBPersonShouldBeFound("placeOfBirth.doesNotContain=" + UPDATED_PLACE_OF_BIRTH);
    }


    @Test
    @Transactional
    public void getAllTMDBPeopleByHomepageIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where homepage equals to DEFAULT_HOMEPAGE
        defaultTMDBPersonShouldBeFound("homepage.equals=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBPersonList where homepage equals to UPDATED_HOMEPAGE
        defaultTMDBPersonShouldNotBeFound("homepage.equals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByHomepageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where homepage not equals to DEFAULT_HOMEPAGE
        defaultTMDBPersonShouldNotBeFound("homepage.notEquals=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBPersonList where homepage not equals to UPDATED_HOMEPAGE
        defaultTMDBPersonShouldBeFound("homepage.notEquals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByHomepageIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where homepage in DEFAULT_HOMEPAGE or UPDATED_HOMEPAGE
        defaultTMDBPersonShouldBeFound("homepage.in=" + DEFAULT_HOMEPAGE + "," + UPDATED_HOMEPAGE);

        // Get all the tMDBPersonList where homepage equals to UPDATED_HOMEPAGE
        defaultTMDBPersonShouldNotBeFound("homepage.in=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByHomepageIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where homepage is not null
        defaultTMDBPersonShouldBeFound("homepage.specified=true");

        // Get all the tMDBPersonList where homepage is null
        defaultTMDBPersonShouldNotBeFound("homepage.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBPeopleByHomepageContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where homepage contains DEFAULT_HOMEPAGE
        defaultTMDBPersonShouldBeFound("homepage.contains=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBPersonList where homepage contains UPDATED_HOMEPAGE
        defaultTMDBPersonShouldNotBeFound("homepage.contains=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBPeopleByHomepageNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        // Get all the tMDBPersonList where homepage does not contain DEFAULT_HOMEPAGE
        defaultTMDBPersonShouldNotBeFound("homepage.doesNotContain=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBPersonList where homepage does not contain UPDATED_HOMEPAGE
        defaultTMDBPersonShouldBeFound("homepage.doesNotContain=" + UPDATED_HOMEPAGE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTMDBPersonShouldBeFound(String filter) throws Exception {
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBPerson.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].deathday").value(hasItem(DEFAULT_DEATHDAY.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].aka").value(hasItem(DEFAULT_AKA)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].biography").value(hasItem(DEFAULT_BIOGRAPHY)))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)));

        // Check, that the count call also returns 1
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTMDBPersonShouldNotBeFound(String filter) throws Exception {
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTMDBPerson() throws Exception {
        // Get the tMDBPerson
        restTMDBPersonMockMvc.perform(get("/api/tmdb-people/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTMDBPerson() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        int databaseSizeBeforeUpdate = tMDBPersonRepository.findAll().size();

        // Update the tMDBPerson
        TMDBPerson updatedTMDBPerson = tMDBPersonRepository.findById(tMDBPerson.getId()).get();
        // Disconnect from session so that the updates on updatedTMDBPerson are not directly saved in db
        em.detach(updatedTMDBPerson);
        updatedTMDBPerson
            .tmdbId(UPDATED_TMDB_ID)
            .birthday(UPDATED_BIRTHDAY)
            .deathday(UPDATED_DEATHDAY)
            .name(UPDATED_NAME)
            .aka(UPDATED_AKA)
            .gender(UPDATED_GENDER)
            .biography(UPDATED_BIOGRAPHY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .homepage(UPDATED_HOMEPAGE);
        TMDBPersonDTO tMDBPersonDTO = tMDBPersonMapper.toDto(updatedTMDBPerson);

        restTMDBPersonMockMvc.perform(put("/api/tmdb-people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPersonDTO)))
            .andExpect(status().isOk());

        // Validate the TMDBPerson in the database
        List<TMDBPerson> tMDBPersonList = tMDBPersonRepository.findAll();
        assertThat(tMDBPersonList).hasSize(databaseSizeBeforeUpdate);
        TMDBPerson testTMDBPerson = tMDBPersonList.get(tMDBPersonList.size() - 1);
        assertThat(testTMDBPerson.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testTMDBPerson.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testTMDBPerson.getDeathday()).isEqualTo(UPDATED_DEATHDAY);
        assertThat(testTMDBPerson.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTMDBPerson.getAka()).isEqualTo(UPDATED_AKA);
        assertThat(testTMDBPerson.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testTMDBPerson.getBiography()).isEqualTo(UPDATED_BIOGRAPHY);
        assertThat(testTMDBPerson.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testTMDBPerson.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);

        // Validate the TMDBPerson in Elasticsearch
        verify(mockTMDBPersonSearchRepository, times(1)).save(testTMDBPerson);
    }

    @Test
    @Transactional
    public void updateNonExistingTMDBPerson() throws Exception {
        int databaseSizeBeforeUpdate = tMDBPersonRepository.findAll().size();

        // Create the TMDBPerson
        TMDBPersonDTO tMDBPersonDTO = tMDBPersonMapper.toDto(tMDBPerson);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTMDBPersonMockMvc.perform(put("/api/tmdb-people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBPersonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBPerson in the database
        List<TMDBPerson> tMDBPersonList = tMDBPersonRepository.findAll();
        assertThat(tMDBPersonList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TMDBPerson in Elasticsearch
        verify(mockTMDBPersonSearchRepository, times(0)).save(tMDBPerson);
    }

    @Test
    @Transactional
    public void deleteTMDBPerson() throws Exception {
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);

        int databaseSizeBeforeDelete = tMDBPersonRepository.findAll().size();

        // Delete the tMDBPerson
        restTMDBPersonMockMvc.perform(delete("/api/tmdb-people/{id}", tMDBPerson.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TMDBPerson> tMDBPersonList = tMDBPersonRepository.findAll();
        assertThat(tMDBPersonList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TMDBPerson in Elasticsearch
        verify(mockTMDBPersonSearchRepository, times(1)).deleteById(tMDBPerson.getId());
    }

    @Test
    @Transactional
    public void searchTMDBPerson() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tMDBPersonRepository.saveAndFlush(tMDBPerson);
        when(mockTMDBPersonSearchRepository.search(queryStringQuery("id:" + tMDBPerson.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(tMDBPerson), PageRequest.of(0, 1), 1));

        // Search the tMDBPerson
        restTMDBPersonMockMvc.perform(get("/api/_search/tmdb-people?query=id:" + tMDBPerson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBPerson.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].deathday").value(hasItem(DEFAULT_DEATHDAY.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].aka").value(hasItem(DEFAULT_AKA)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].biography").value(hasItem(DEFAULT_BIOGRAPHY)))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)));
    }
}
