package com.mgoulene.web.rest;

import com.mgoulene.MymoviesApp;
import com.mgoulene.domain.TMDBMovie;
import com.mgoulene.domain.TMDBCredit;
import com.mgoulene.domain.TMDBGenre;
import com.mgoulene.repository.TMDBMovieRepository;
import com.mgoulene.repository.search.TMDBMovieSearchRepository;
import com.mgoulene.service.TMDBMovieService;
import com.mgoulene.service.dto.TMDBMovieDTO;
import com.mgoulene.service.mapper.TMDBMovieMapper;
import com.mgoulene.service.dto.TMDBMovieCriteria;
import com.mgoulene.service.TMDBMovieQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.domain.enumeration.TMDBMovieStatus;
/**
 * Integration tests for the {@link TMDBMovieResource} REST controller.
 */
@SpringBootTest(classes = MymoviesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TMDBMovieResourceIT {

    private static final Integer DEFAULT_TMDB_ID = 1;
    private static final Integer UPDATED_TMDB_ID = 2;
    private static final Integer SMALLER_TMDB_ID = 1 - 1;

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FOR_ADULT = false;
    private static final Boolean UPDATED_FOR_ADULT = true;

    private static final String DEFAULT_HOMEPAGE = "AAAAAAAAAA";
    private static final String UPDATED_HOMEPAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_LANGAGE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_LANGAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_OVERVIEW = "AAAAAAAAAA";
    private static final String UPDATED_OVERVIEW = "BBBBBBBBBB";

    private static final String DEFAULT_TAGLINE = "AAAAAAAAAA";
    private static final String UPDATED_TAGLINE = "BBBBBBBBBB";

    private static final TMDBMovieStatus DEFAULT_STATUS = TMDBMovieStatus.RUMORED;
    private static final TMDBMovieStatus UPDATED_STATUS = TMDBMovieStatus.PLANNED;

    private static final Float DEFAULT_VOTE_AVERAGE = 0F;
    private static final Float UPDATED_VOTE_AVERAGE = 1F;
    private static final Float SMALLER_VOTE_AVERAGE = 0F - 1F;

    private static final Integer DEFAULT_VOTE_COUNT = 1;
    private static final Integer UPDATED_VOTE_COUNT = 2;
    private static final Integer SMALLER_VOTE_COUNT = 1 - 1;

    private static final LocalDate DEFAULT_RELEASE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RELEASE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_RELEASE_DATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private TMDBMovieRepository tMDBMovieRepository;

    @Mock
    private TMDBMovieRepository tMDBMovieRepositoryMock;

    @Autowired
    private TMDBMovieMapper tMDBMovieMapper;

    @Mock
    private TMDBMovieService tMDBMovieServiceMock;

    @Autowired
    private TMDBMovieService tMDBMovieService;

    /**
     * This repository is mocked in the com.mgoulene.repository.search test package.
     *
     * @see com.mgoulene.repository.search.TMDBMovieSearchRepositoryMockConfiguration
     */
    @Autowired
    private TMDBMovieSearchRepository mockTMDBMovieSearchRepository;

    @Autowired
    private TMDBMovieQueryService tMDBMovieQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTMDBMovieMockMvc;

    private TMDBMovie tMDBMovie;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBMovie createEntity(EntityManager em) {
        TMDBMovie tMDBMovie = new TMDBMovie()
            .tmdbId(DEFAULT_TMDB_ID)
            .title(DEFAULT_TITLE)
            .forAdult(DEFAULT_FOR_ADULT)
            .homepage(DEFAULT_HOMEPAGE)
            .originalLangage(DEFAULT_ORIGINAL_LANGAGE)
            .originalTitle(DEFAULT_ORIGINAL_TITLE)
            .overview(DEFAULT_OVERVIEW)
            .tagline(DEFAULT_TAGLINE)
            .status(DEFAULT_STATUS)
            .voteAverage(DEFAULT_VOTE_AVERAGE)
            .voteCount(DEFAULT_VOTE_COUNT)
            .releaseDate(DEFAULT_RELEASE_DATE);
        return tMDBMovie;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBMovie createUpdatedEntity(EntityManager em) {
        TMDBMovie tMDBMovie = new TMDBMovie()
            .tmdbId(UPDATED_TMDB_ID)
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLangage(UPDATED_ORIGINAL_LANGAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE);
        return tMDBMovie;
    }

    @BeforeEach
    public void initTest() {
        tMDBMovie = createEntity(em);
    }

    @Test
    @Transactional
    public void createTMDBMovie() throws Exception {
        int databaseSizeBeforeCreate = tMDBMovieRepository.findAll().size();
        // Create the TMDBMovie
        TMDBMovieDTO tMDBMovieDTO = tMDBMovieMapper.toDto(tMDBMovie);
        restTMDBMovieMockMvc.perform(post("/api/tmdb-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBMovieDTO)))
            .andExpect(status().isCreated());

        // Validate the TMDBMovie in the database
        List<TMDBMovie> tMDBMovieList = tMDBMovieRepository.findAll();
        assertThat(tMDBMovieList).hasSize(databaseSizeBeforeCreate + 1);
        TMDBMovie testTMDBMovie = tMDBMovieList.get(tMDBMovieList.size() - 1);
        assertThat(testTMDBMovie.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testTMDBMovie.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testTMDBMovie.isForAdult()).isEqualTo(DEFAULT_FOR_ADULT);
        assertThat(testTMDBMovie.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);
        assertThat(testTMDBMovie.getOriginalLangage()).isEqualTo(DEFAULT_ORIGINAL_LANGAGE);
        assertThat(testTMDBMovie.getOriginalTitle()).isEqualTo(DEFAULT_ORIGINAL_TITLE);
        assertThat(testTMDBMovie.getOverview()).isEqualTo(DEFAULT_OVERVIEW);
        assertThat(testTMDBMovie.getTagline()).isEqualTo(DEFAULT_TAGLINE);
        assertThat(testTMDBMovie.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTMDBMovie.getVoteAverage()).isEqualTo(DEFAULT_VOTE_AVERAGE);
        assertThat(testTMDBMovie.getVoteCount()).isEqualTo(DEFAULT_VOTE_COUNT);
        assertThat(testTMDBMovie.getReleaseDate()).isEqualTo(DEFAULT_RELEASE_DATE);

        // Validate the TMDBMovie in Elasticsearch
        verify(mockTMDBMovieSearchRepository, times(1)).save(testTMDBMovie);
    }

    @Test
    @Transactional
    public void createTMDBMovieWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tMDBMovieRepository.findAll().size();

        // Create the TMDBMovie with an existing ID
        tMDBMovie.setId(1L);
        TMDBMovieDTO tMDBMovieDTO = tMDBMovieMapper.toDto(tMDBMovie);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTMDBMovieMockMvc.perform(post("/api/tmdb-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBMovieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBMovie in the database
        List<TMDBMovie> tMDBMovieList = tMDBMovieRepository.findAll();
        assertThat(tMDBMovieList).hasSize(databaseSizeBeforeCreate);

        // Validate the TMDBMovie in Elasticsearch
        verify(mockTMDBMovieSearchRepository, times(0)).save(tMDBMovie);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBMovieRepository.findAll().size();
        // set the field null
        tMDBMovie.setTmdbId(null);

        // Create the TMDBMovie, which fails.
        TMDBMovieDTO tMDBMovieDTO = tMDBMovieMapper.toDto(tMDBMovie);


        restTMDBMovieMockMvc.perform(post("/api/tmdb-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBMovieDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBMovie> tMDBMovieList = tMDBMovieRepository.findAll();
        assertThat(tMDBMovieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTMDBMovies() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].forAdult").value(hasItem(DEFAULT_FOR_ADULT.booleanValue())))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].originalLangage").value(hasItem(DEFAULT_ORIGINAL_LANGAGE)))
            .andExpect(jsonPath("$.[*].originalTitle").value(hasItem(DEFAULT_ORIGINAL_TITLE)))
            .andExpect(jsonPath("$.[*].overview").value(hasItem(DEFAULT_OVERVIEW)))
            .andExpect(jsonPath("$.[*].tagline").value(hasItem(DEFAULT_TAGLINE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].releaseDate").value(hasItem(DEFAULT_RELEASE_DATE.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllTMDBMoviesWithEagerRelationshipsIsEnabled() throws Exception {
        when(tMDBMovieServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies?eagerload=true"))
            .andExpect(status().isOk());

        verify(tMDBMovieServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllTMDBMoviesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(tMDBMovieServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies?eagerload=true"))
            .andExpect(status().isOk());

        verify(tMDBMovieServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getTMDBMovie() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get the tMDBMovie
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies/{id}", tMDBMovie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tMDBMovie.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.forAdult").value(DEFAULT_FOR_ADULT.booleanValue()))
            .andExpect(jsonPath("$.homepage").value(DEFAULT_HOMEPAGE))
            .andExpect(jsonPath("$.originalLangage").value(DEFAULT_ORIGINAL_LANGAGE))
            .andExpect(jsonPath("$.originalTitle").value(DEFAULT_ORIGINAL_TITLE))
            .andExpect(jsonPath("$.overview").value(DEFAULT_OVERVIEW))
            .andExpect(jsonPath("$.tagline").value(DEFAULT_TAGLINE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.voteAverage").value(DEFAULT_VOTE_AVERAGE.doubleValue()))
            .andExpect(jsonPath("$.voteCount").value(DEFAULT_VOTE_COUNT))
            .andExpect(jsonPath("$.releaseDate").value(DEFAULT_RELEASE_DATE.toString()));
    }


    @Test
    @Transactional
    public void getTMDBMoviesByIdFiltering() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        Long id = tMDBMovie.getId();

        defaultTMDBMovieShouldBeFound("id.equals=" + id);
        defaultTMDBMovieShouldNotBeFound("id.notEquals=" + id);

        defaultTMDBMovieShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTMDBMovieShouldNotBeFound("id.greaterThan=" + id);

        defaultTMDBMovieShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTMDBMovieShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId equals to DEFAULT_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId not equals to UPDATED_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId is not null
        defaultTMDBMovieShouldBeFound("tmdbId.specified=true");

        // Get all the tMDBMovieList where tmdbId is null
        defaultTMDBMovieShouldNotBeFound("tmdbId.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId is greater than or equal to DEFAULT_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.greaterThanOrEqual=" + DEFAULT_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId is greater than or equal to UPDATED_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.greaterThanOrEqual=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId is less than or equal to DEFAULT_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.lessThanOrEqual=" + DEFAULT_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId is less than or equal to SMALLER_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.lessThanOrEqual=" + SMALLER_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId is less than DEFAULT_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.lessThan=" + DEFAULT_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId is less than UPDATED_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.lessThan=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTmdbIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tmdbId is greater than DEFAULT_TMDB_ID
        defaultTMDBMovieShouldNotBeFound("tmdbId.greaterThan=" + DEFAULT_TMDB_ID);

        // Get all the tMDBMovieList where tmdbId is greater than SMALLER_TMDB_ID
        defaultTMDBMovieShouldBeFound("tmdbId.greaterThan=" + SMALLER_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where title equals to DEFAULT_TITLE
        defaultTMDBMovieShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the tMDBMovieList where title equals to UPDATED_TITLE
        defaultTMDBMovieShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where title not equals to DEFAULT_TITLE
        defaultTMDBMovieShouldNotBeFound("title.notEquals=" + DEFAULT_TITLE);

        // Get all the tMDBMovieList where title not equals to UPDATED_TITLE
        defaultTMDBMovieShouldBeFound("title.notEquals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultTMDBMovieShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the tMDBMovieList where title equals to UPDATED_TITLE
        defaultTMDBMovieShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where title is not null
        defaultTMDBMovieShouldBeFound("title.specified=true");

        // Get all the tMDBMovieList where title is null
        defaultTMDBMovieShouldNotBeFound("title.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBMoviesByTitleContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where title contains DEFAULT_TITLE
        defaultTMDBMovieShouldBeFound("title.contains=" + DEFAULT_TITLE);

        // Get all the tMDBMovieList where title contains UPDATED_TITLE
        defaultTMDBMovieShouldNotBeFound("title.contains=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTitleNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where title does not contain DEFAULT_TITLE
        defaultTMDBMovieShouldNotBeFound("title.doesNotContain=" + DEFAULT_TITLE);

        // Get all the tMDBMovieList where title does not contain UPDATED_TITLE
        defaultTMDBMovieShouldBeFound("title.doesNotContain=" + UPDATED_TITLE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByForAdultIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where forAdult equals to DEFAULT_FOR_ADULT
        defaultTMDBMovieShouldBeFound("forAdult.equals=" + DEFAULT_FOR_ADULT);

        // Get all the tMDBMovieList where forAdult equals to UPDATED_FOR_ADULT
        defaultTMDBMovieShouldNotBeFound("forAdult.equals=" + UPDATED_FOR_ADULT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByForAdultIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where forAdult not equals to DEFAULT_FOR_ADULT
        defaultTMDBMovieShouldNotBeFound("forAdult.notEquals=" + DEFAULT_FOR_ADULT);

        // Get all the tMDBMovieList where forAdult not equals to UPDATED_FOR_ADULT
        defaultTMDBMovieShouldBeFound("forAdult.notEquals=" + UPDATED_FOR_ADULT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByForAdultIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where forAdult in DEFAULT_FOR_ADULT or UPDATED_FOR_ADULT
        defaultTMDBMovieShouldBeFound("forAdult.in=" + DEFAULT_FOR_ADULT + "," + UPDATED_FOR_ADULT);

        // Get all the tMDBMovieList where forAdult equals to UPDATED_FOR_ADULT
        defaultTMDBMovieShouldNotBeFound("forAdult.in=" + UPDATED_FOR_ADULT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByForAdultIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where forAdult is not null
        defaultTMDBMovieShouldBeFound("forAdult.specified=true");

        // Get all the tMDBMovieList where forAdult is null
        defaultTMDBMovieShouldNotBeFound("forAdult.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByHomepageIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where homepage equals to DEFAULT_HOMEPAGE
        defaultTMDBMovieShouldBeFound("homepage.equals=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBMovieList where homepage equals to UPDATED_HOMEPAGE
        defaultTMDBMovieShouldNotBeFound("homepage.equals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByHomepageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where homepage not equals to DEFAULT_HOMEPAGE
        defaultTMDBMovieShouldNotBeFound("homepage.notEquals=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBMovieList where homepage not equals to UPDATED_HOMEPAGE
        defaultTMDBMovieShouldBeFound("homepage.notEquals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByHomepageIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where homepage in DEFAULT_HOMEPAGE or UPDATED_HOMEPAGE
        defaultTMDBMovieShouldBeFound("homepage.in=" + DEFAULT_HOMEPAGE + "," + UPDATED_HOMEPAGE);

        // Get all the tMDBMovieList where homepage equals to UPDATED_HOMEPAGE
        defaultTMDBMovieShouldNotBeFound("homepage.in=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByHomepageIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where homepage is not null
        defaultTMDBMovieShouldBeFound("homepage.specified=true");

        // Get all the tMDBMovieList where homepage is null
        defaultTMDBMovieShouldNotBeFound("homepage.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBMoviesByHomepageContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where homepage contains DEFAULT_HOMEPAGE
        defaultTMDBMovieShouldBeFound("homepage.contains=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBMovieList where homepage contains UPDATED_HOMEPAGE
        defaultTMDBMovieShouldNotBeFound("homepage.contains=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByHomepageNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where homepage does not contain DEFAULT_HOMEPAGE
        defaultTMDBMovieShouldNotBeFound("homepage.doesNotContain=" + DEFAULT_HOMEPAGE);

        // Get all the tMDBMovieList where homepage does not contain UPDATED_HOMEPAGE
        defaultTMDBMovieShouldBeFound("homepage.doesNotContain=" + UPDATED_HOMEPAGE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalLangageIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalLangage equals to DEFAULT_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldBeFound("originalLangage.equals=" + DEFAULT_ORIGINAL_LANGAGE);

        // Get all the tMDBMovieList where originalLangage equals to UPDATED_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldNotBeFound("originalLangage.equals=" + UPDATED_ORIGINAL_LANGAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalLangageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalLangage not equals to DEFAULT_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldNotBeFound("originalLangage.notEquals=" + DEFAULT_ORIGINAL_LANGAGE);

        // Get all the tMDBMovieList where originalLangage not equals to UPDATED_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldBeFound("originalLangage.notEquals=" + UPDATED_ORIGINAL_LANGAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalLangageIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalLangage in DEFAULT_ORIGINAL_LANGAGE or UPDATED_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldBeFound("originalLangage.in=" + DEFAULT_ORIGINAL_LANGAGE + "," + UPDATED_ORIGINAL_LANGAGE);

        // Get all the tMDBMovieList where originalLangage equals to UPDATED_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldNotBeFound("originalLangage.in=" + UPDATED_ORIGINAL_LANGAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalLangageIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalLangage is not null
        defaultTMDBMovieShouldBeFound("originalLangage.specified=true");

        // Get all the tMDBMovieList where originalLangage is null
        defaultTMDBMovieShouldNotBeFound("originalLangage.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalLangageContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalLangage contains DEFAULT_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldBeFound("originalLangage.contains=" + DEFAULT_ORIGINAL_LANGAGE);

        // Get all the tMDBMovieList where originalLangage contains UPDATED_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldNotBeFound("originalLangage.contains=" + UPDATED_ORIGINAL_LANGAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalLangageNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalLangage does not contain DEFAULT_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldNotBeFound("originalLangage.doesNotContain=" + DEFAULT_ORIGINAL_LANGAGE);

        // Get all the tMDBMovieList where originalLangage does not contain UPDATED_ORIGINAL_LANGAGE
        defaultTMDBMovieShouldBeFound("originalLangage.doesNotContain=" + UPDATED_ORIGINAL_LANGAGE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalTitle equals to DEFAULT_ORIGINAL_TITLE
        defaultTMDBMovieShouldBeFound("originalTitle.equals=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the tMDBMovieList where originalTitle equals to UPDATED_ORIGINAL_TITLE
        defaultTMDBMovieShouldNotBeFound("originalTitle.equals=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalTitle not equals to DEFAULT_ORIGINAL_TITLE
        defaultTMDBMovieShouldNotBeFound("originalTitle.notEquals=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the tMDBMovieList where originalTitle not equals to UPDATED_ORIGINAL_TITLE
        defaultTMDBMovieShouldBeFound("originalTitle.notEquals=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalTitleIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalTitle in DEFAULT_ORIGINAL_TITLE or UPDATED_ORIGINAL_TITLE
        defaultTMDBMovieShouldBeFound("originalTitle.in=" + DEFAULT_ORIGINAL_TITLE + "," + UPDATED_ORIGINAL_TITLE);

        // Get all the tMDBMovieList where originalTitle equals to UPDATED_ORIGINAL_TITLE
        defaultTMDBMovieShouldNotBeFound("originalTitle.in=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalTitle is not null
        defaultTMDBMovieShouldBeFound("originalTitle.specified=true");

        // Get all the tMDBMovieList where originalTitle is null
        defaultTMDBMovieShouldNotBeFound("originalTitle.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalTitleContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalTitle contains DEFAULT_ORIGINAL_TITLE
        defaultTMDBMovieShouldBeFound("originalTitle.contains=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the tMDBMovieList where originalTitle contains UPDATED_ORIGINAL_TITLE
        defaultTMDBMovieShouldNotBeFound("originalTitle.contains=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOriginalTitleNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where originalTitle does not contain DEFAULT_ORIGINAL_TITLE
        defaultTMDBMovieShouldNotBeFound("originalTitle.doesNotContain=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the tMDBMovieList where originalTitle does not contain UPDATED_ORIGINAL_TITLE
        defaultTMDBMovieShouldBeFound("originalTitle.doesNotContain=" + UPDATED_ORIGINAL_TITLE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByOverviewIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where overview equals to DEFAULT_OVERVIEW
        defaultTMDBMovieShouldBeFound("overview.equals=" + DEFAULT_OVERVIEW);

        // Get all the tMDBMovieList where overview equals to UPDATED_OVERVIEW
        defaultTMDBMovieShouldNotBeFound("overview.equals=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOverviewIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where overview not equals to DEFAULT_OVERVIEW
        defaultTMDBMovieShouldNotBeFound("overview.notEquals=" + DEFAULT_OVERVIEW);

        // Get all the tMDBMovieList where overview not equals to UPDATED_OVERVIEW
        defaultTMDBMovieShouldBeFound("overview.notEquals=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOverviewIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where overview in DEFAULT_OVERVIEW or UPDATED_OVERVIEW
        defaultTMDBMovieShouldBeFound("overview.in=" + DEFAULT_OVERVIEW + "," + UPDATED_OVERVIEW);

        // Get all the tMDBMovieList where overview equals to UPDATED_OVERVIEW
        defaultTMDBMovieShouldNotBeFound("overview.in=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOverviewIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where overview is not null
        defaultTMDBMovieShouldBeFound("overview.specified=true");

        // Get all the tMDBMovieList where overview is null
        defaultTMDBMovieShouldNotBeFound("overview.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBMoviesByOverviewContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where overview contains DEFAULT_OVERVIEW
        defaultTMDBMovieShouldBeFound("overview.contains=" + DEFAULT_OVERVIEW);

        // Get all the tMDBMovieList where overview contains UPDATED_OVERVIEW
        defaultTMDBMovieShouldNotBeFound("overview.contains=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByOverviewNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where overview does not contain DEFAULT_OVERVIEW
        defaultTMDBMovieShouldNotBeFound("overview.doesNotContain=" + DEFAULT_OVERVIEW);

        // Get all the tMDBMovieList where overview does not contain UPDATED_OVERVIEW
        defaultTMDBMovieShouldBeFound("overview.doesNotContain=" + UPDATED_OVERVIEW);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByTaglineIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tagline equals to DEFAULT_TAGLINE
        defaultTMDBMovieShouldBeFound("tagline.equals=" + DEFAULT_TAGLINE);

        // Get all the tMDBMovieList where tagline equals to UPDATED_TAGLINE
        defaultTMDBMovieShouldNotBeFound("tagline.equals=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTaglineIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tagline not equals to DEFAULT_TAGLINE
        defaultTMDBMovieShouldNotBeFound("tagline.notEquals=" + DEFAULT_TAGLINE);

        // Get all the tMDBMovieList where tagline not equals to UPDATED_TAGLINE
        defaultTMDBMovieShouldBeFound("tagline.notEquals=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTaglineIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tagline in DEFAULT_TAGLINE or UPDATED_TAGLINE
        defaultTMDBMovieShouldBeFound("tagline.in=" + DEFAULT_TAGLINE + "," + UPDATED_TAGLINE);

        // Get all the tMDBMovieList where tagline equals to UPDATED_TAGLINE
        defaultTMDBMovieShouldNotBeFound("tagline.in=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTaglineIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tagline is not null
        defaultTMDBMovieShouldBeFound("tagline.specified=true");

        // Get all the tMDBMovieList where tagline is null
        defaultTMDBMovieShouldNotBeFound("tagline.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBMoviesByTaglineContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tagline contains DEFAULT_TAGLINE
        defaultTMDBMovieShouldBeFound("tagline.contains=" + DEFAULT_TAGLINE);

        // Get all the tMDBMovieList where tagline contains UPDATED_TAGLINE
        defaultTMDBMovieShouldNotBeFound("tagline.contains=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByTaglineNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where tagline does not contain DEFAULT_TAGLINE
        defaultTMDBMovieShouldNotBeFound("tagline.doesNotContain=" + DEFAULT_TAGLINE);

        // Get all the tMDBMovieList where tagline does not contain UPDATED_TAGLINE
        defaultTMDBMovieShouldBeFound("tagline.doesNotContain=" + UPDATED_TAGLINE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where status equals to DEFAULT_STATUS
        defaultTMDBMovieShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the tMDBMovieList where status equals to UPDATED_STATUS
        defaultTMDBMovieShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where status not equals to DEFAULT_STATUS
        defaultTMDBMovieShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the tMDBMovieList where status not equals to UPDATED_STATUS
        defaultTMDBMovieShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultTMDBMovieShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the tMDBMovieList where status equals to UPDATED_STATUS
        defaultTMDBMovieShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where status is not null
        defaultTMDBMovieShouldBeFound("status.specified=true");

        // Get all the tMDBMovieList where status is null
        defaultTMDBMovieShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage equals to DEFAULT_VOTE_AVERAGE
        defaultTMDBMovieShouldBeFound("voteAverage.equals=" + DEFAULT_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage equals to UPDATED_VOTE_AVERAGE
        defaultTMDBMovieShouldNotBeFound("voteAverage.equals=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage not equals to DEFAULT_VOTE_AVERAGE
        defaultTMDBMovieShouldNotBeFound("voteAverage.notEquals=" + DEFAULT_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage not equals to UPDATED_VOTE_AVERAGE
        defaultTMDBMovieShouldBeFound("voteAverage.notEquals=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage in DEFAULT_VOTE_AVERAGE or UPDATED_VOTE_AVERAGE
        defaultTMDBMovieShouldBeFound("voteAverage.in=" + DEFAULT_VOTE_AVERAGE + "," + UPDATED_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage equals to UPDATED_VOTE_AVERAGE
        defaultTMDBMovieShouldNotBeFound("voteAverage.in=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage is not null
        defaultTMDBMovieShouldBeFound("voteAverage.specified=true");

        // Get all the tMDBMovieList where voteAverage is null
        defaultTMDBMovieShouldNotBeFound("voteAverage.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage is greater than or equal to DEFAULT_VOTE_AVERAGE
        defaultTMDBMovieShouldBeFound("voteAverage.greaterThanOrEqual=" + DEFAULT_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage is greater than or equal to (DEFAULT_VOTE_AVERAGE + 1)
        defaultTMDBMovieShouldNotBeFound("voteAverage.greaterThanOrEqual=" + (DEFAULT_VOTE_AVERAGE + 1));
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage is less than or equal to DEFAULT_VOTE_AVERAGE
        defaultTMDBMovieShouldBeFound("voteAverage.lessThanOrEqual=" + DEFAULT_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage is less than or equal to SMALLER_VOTE_AVERAGE
        defaultTMDBMovieShouldNotBeFound("voteAverage.lessThanOrEqual=" + SMALLER_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage is less than DEFAULT_VOTE_AVERAGE
        defaultTMDBMovieShouldNotBeFound("voteAverage.lessThan=" + DEFAULT_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage is less than (DEFAULT_VOTE_AVERAGE + 1)
        defaultTMDBMovieShouldBeFound("voteAverage.lessThan=" + (DEFAULT_VOTE_AVERAGE + 1));
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteAverageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteAverage is greater than DEFAULT_VOTE_AVERAGE
        defaultTMDBMovieShouldNotBeFound("voteAverage.greaterThan=" + DEFAULT_VOTE_AVERAGE);

        // Get all the tMDBMovieList where voteAverage is greater than SMALLER_VOTE_AVERAGE
        defaultTMDBMovieShouldBeFound("voteAverage.greaterThan=" + SMALLER_VOTE_AVERAGE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount equals to DEFAULT_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.equals=" + DEFAULT_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount equals to UPDATED_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.equals=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount not equals to DEFAULT_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.notEquals=" + DEFAULT_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount not equals to UPDATED_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.notEquals=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount in DEFAULT_VOTE_COUNT or UPDATED_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.in=" + DEFAULT_VOTE_COUNT + "," + UPDATED_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount equals to UPDATED_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.in=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount is not null
        defaultTMDBMovieShouldBeFound("voteCount.specified=true");

        // Get all the tMDBMovieList where voteCount is null
        defaultTMDBMovieShouldNotBeFound("voteCount.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount is greater than or equal to DEFAULT_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.greaterThanOrEqual=" + DEFAULT_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount is greater than or equal to UPDATED_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.greaterThanOrEqual=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount is less than or equal to DEFAULT_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.lessThanOrEqual=" + DEFAULT_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount is less than or equal to SMALLER_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.lessThanOrEqual=" + SMALLER_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount is less than DEFAULT_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.lessThan=" + DEFAULT_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount is less than UPDATED_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.lessThan=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByVoteCountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where voteCount is greater than DEFAULT_VOTE_COUNT
        defaultTMDBMovieShouldNotBeFound("voteCount.greaterThan=" + DEFAULT_VOTE_COUNT);

        // Get all the tMDBMovieList where voteCount is greater than SMALLER_VOTE_COUNT
        defaultTMDBMovieShouldBeFound("voteCount.greaterThan=" + SMALLER_VOTE_COUNT);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate equals to DEFAULT_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.equals=" + DEFAULT_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate equals to UPDATED_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.equals=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate not equals to DEFAULT_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.notEquals=" + DEFAULT_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate not equals to UPDATED_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.notEquals=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate in DEFAULT_RELEASE_DATE or UPDATED_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.in=" + DEFAULT_RELEASE_DATE + "," + UPDATED_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate equals to UPDATED_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.in=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate is not null
        defaultTMDBMovieShouldBeFound("releaseDate.specified=true");

        // Get all the tMDBMovieList where releaseDate is null
        defaultTMDBMovieShouldNotBeFound("releaseDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate is greater than or equal to DEFAULT_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.greaterThanOrEqual=" + DEFAULT_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate is greater than or equal to UPDATED_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.greaterThanOrEqual=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate is less than or equal to DEFAULT_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.lessThanOrEqual=" + DEFAULT_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate is less than or equal to SMALLER_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.lessThanOrEqual=" + SMALLER_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate is less than DEFAULT_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.lessThan=" + DEFAULT_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate is less than UPDATED_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.lessThan=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllTMDBMoviesByReleaseDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        // Get all the tMDBMovieList where releaseDate is greater than DEFAULT_RELEASE_DATE
        defaultTMDBMovieShouldNotBeFound("releaseDate.greaterThan=" + DEFAULT_RELEASE_DATE);

        // Get all the tMDBMovieList where releaseDate is greater than SMALLER_RELEASE_DATE
        defaultTMDBMovieShouldBeFound("releaseDate.greaterThan=" + SMALLER_RELEASE_DATE);
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByCreditsIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);
        TMDBCredit credits = TMDBCreditResourceIT.createEntity(em);
        em.persist(credits);
        em.flush();
        tMDBMovie.addCredits(credits);
        tMDBMovieRepository.saveAndFlush(tMDBMovie);
        Long creditsId = credits.getId();

        // Get all the tMDBMovieList where credits equals to creditsId
        defaultTMDBMovieShouldBeFound("creditsId.equals=" + creditsId);

        // Get all the tMDBMovieList where credits equals to creditsId + 1
        defaultTMDBMovieShouldNotBeFound("creditsId.equals=" + (creditsId + 1));
    }


    @Test
    @Transactional
    public void getAllTMDBMoviesByGenreIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);
        TMDBGenre genre = TMDBGenreResourceIT.createEntity(em);
        em.persist(genre);
        em.flush();
        tMDBMovie.addGenre(genre);
        tMDBMovieRepository.saveAndFlush(tMDBMovie);
        Long genreId = genre.getId();

        // Get all the tMDBMovieList where genre equals to genreId
        defaultTMDBMovieShouldBeFound("genreId.equals=" + genreId);

        // Get all the tMDBMovieList where genre equals to genreId + 1
        defaultTMDBMovieShouldNotBeFound("genreId.equals=" + (genreId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTMDBMovieShouldBeFound(String filter) throws Exception {
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].forAdult").value(hasItem(DEFAULT_FOR_ADULT.booleanValue())))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].originalLangage").value(hasItem(DEFAULT_ORIGINAL_LANGAGE)))
            .andExpect(jsonPath("$.[*].originalTitle").value(hasItem(DEFAULT_ORIGINAL_TITLE)))
            .andExpect(jsonPath("$.[*].overview").value(hasItem(DEFAULT_OVERVIEW)))
            .andExpect(jsonPath("$.[*].tagline").value(hasItem(DEFAULT_TAGLINE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].releaseDate").value(hasItem(DEFAULT_RELEASE_DATE.toString())));

        // Check, that the count call also returns 1
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTMDBMovieShouldNotBeFound(String filter) throws Exception {
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTMDBMovie() throws Exception {
        // Get the tMDBMovie
        restTMDBMovieMockMvc.perform(get("/api/tmdb-movies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTMDBMovie() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        int databaseSizeBeforeUpdate = tMDBMovieRepository.findAll().size();

        // Update the tMDBMovie
        TMDBMovie updatedTMDBMovie = tMDBMovieRepository.findById(tMDBMovie.getId()).get();
        // Disconnect from session so that the updates on updatedTMDBMovie are not directly saved in db
        em.detach(updatedTMDBMovie);
        updatedTMDBMovie
            .tmdbId(UPDATED_TMDB_ID)
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLangage(UPDATED_ORIGINAL_LANGAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE);
        TMDBMovieDTO tMDBMovieDTO = tMDBMovieMapper.toDto(updatedTMDBMovie);

        restTMDBMovieMockMvc.perform(put("/api/tmdb-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBMovieDTO)))
            .andExpect(status().isOk());

        // Validate the TMDBMovie in the database
        List<TMDBMovie> tMDBMovieList = tMDBMovieRepository.findAll();
        assertThat(tMDBMovieList).hasSize(databaseSizeBeforeUpdate);
        TMDBMovie testTMDBMovie = tMDBMovieList.get(tMDBMovieList.size() - 1);
        assertThat(testTMDBMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testTMDBMovie.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testTMDBMovie.isForAdult()).isEqualTo(UPDATED_FOR_ADULT);
        assertThat(testTMDBMovie.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);
        assertThat(testTMDBMovie.getOriginalLangage()).isEqualTo(UPDATED_ORIGINAL_LANGAGE);
        assertThat(testTMDBMovie.getOriginalTitle()).isEqualTo(UPDATED_ORIGINAL_TITLE);
        assertThat(testTMDBMovie.getOverview()).isEqualTo(UPDATED_OVERVIEW);
        assertThat(testTMDBMovie.getTagline()).isEqualTo(UPDATED_TAGLINE);
        assertThat(testTMDBMovie.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTMDBMovie.getVoteAverage()).isEqualTo(UPDATED_VOTE_AVERAGE);
        assertThat(testTMDBMovie.getVoteCount()).isEqualTo(UPDATED_VOTE_COUNT);
        assertThat(testTMDBMovie.getReleaseDate()).isEqualTo(UPDATED_RELEASE_DATE);

        // Validate the TMDBMovie in Elasticsearch
        verify(mockTMDBMovieSearchRepository, times(1)).save(testTMDBMovie);
    }

    @Test
    @Transactional
    public void updateNonExistingTMDBMovie() throws Exception {
        int databaseSizeBeforeUpdate = tMDBMovieRepository.findAll().size();

        // Create the TMDBMovie
        TMDBMovieDTO tMDBMovieDTO = tMDBMovieMapper.toDto(tMDBMovie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTMDBMovieMockMvc.perform(put("/api/tmdb-movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBMovieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBMovie in the database
        List<TMDBMovie> tMDBMovieList = tMDBMovieRepository.findAll();
        assertThat(tMDBMovieList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TMDBMovie in Elasticsearch
        verify(mockTMDBMovieSearchRepository, times(0)).save(tMDBMovie);
    }

    @Test
    @Transactional
    public void deleteTMDBMovie() throws Exception {
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);

        int databaseSizeBeforeDelete = tMDBMovieRepository.findAll().size();

        // Delete the tMDBMovie
        restTMDBMovieMockMvc.perform(delete("/api/tmdb-movies/{id}", tMDBMovie.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TMDBMovie> tMDBMovieList = tMDBMovieRepository.findAll();
        assertThat(tMDBMovieList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TMDBMovie in Elasticsearch
        verify(mockTMDBMovieSearchRepository, times(1)).deleteById(tMDBMovie.getId());
    }

    @Test
    @Transactional
    public void searchTMDBMovie() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tMDBMovieRepository.saveAndFlush(tMDBMovie);
        when(mockTMDBMovieSearchRepository.search(queryStringQuery("id:" + tMDBMovie.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(tMDBMovie), PageRequest.of(0, 1), 1));

        // Search the tMDBMovie
        restTMDBMovieMockMvc.perform(get("/api/_search/tmdb-movies?query=id:" + tMDBMovie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].forAdult").value(hasItem(DEFAULT_FOR_ADULT.booleanValue())))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].originalLangage").value(hasItem(DEFAULT_ORIGINAL_LANGAGE)))
            .andExpect(jsonPath("$.[*].originalTitle").value(hasItem(DEFAULT_ORIGINAL_TITLE)))
            .andExpect(jsonPath("$.[*].overview").value(hasItem(DEFAULT_OVERVIEW)))
            .andExpect(jsonPath("$.[*].tagline").value(hasItem(DEFAULT_TAGLINE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].releaseDate").value(hasItem(DEFAULT_RELEASE_DATE.toString())));
    }
}
