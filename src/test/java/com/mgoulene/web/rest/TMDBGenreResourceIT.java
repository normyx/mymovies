package com.mgoulene.web.rest;

import com.mgoulene.MymoviesApp;
import com.mgoulene.domain.TMDBGenre;
import com.mgoulene.domain.TMDBMovie;
import com.mgoulene.repository.TMDBGenreRepository;
import com.mgoulene.repository.search.TMDBGenreSearchRepository;
import com.mgoulene.service.TMDBGenreService;
import com.mgoulene.service.dto.TMDBGenreDTO;
import com.mgoulene.service.mapper.TMDBGenreMapper;
import com.mgoulene.service.dto.TMDBGenreCriteria;
import com.mgoulene.service.TMDBGenreQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TMDBGenreResource} REST controller.
 */
@SpringBootTest(classes = MymoviesApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TMDBGenreResourceIT {

    private static final Integer DEFAULT_TMDB_ID = 1;
    private static final Integer UPDATED_TMDB_ID = 2;
    private static final Integer SMALLER_TMDB_ID = 1 - 1;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TMDBGenreRepository tMDBGenreRepository;

    @Autowired
    private TMDBGenreMapper tMDBGenreMapper;

    @Autowired
    private TMDBGenreService tMDBGenreService;

    /**
     * This repository is mocked in the com.mgoulene.repository.search test package.
     *
     * @see com.mgoulene.repository.search.TMDBGenreSearchRepositoryMockConfiguration
     */
    @Autowired
    private TMDBGenreSearchRepository mockTMDBGenreSearchRepository;

    @Autowired
    private TMDBGenreQueryService tMDBGenreQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTMDBGenreMockMvc;

    private TMDBGenre tMDBGenre;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBGenre createEntity(EntityManager em) {
        TMDBGenre tMDBGenre = new TMDBGenre()
            .tmdbId(DEFAULT_TMDB_ID)
            .name(DEFAULT_NAME);
        return tMDBGenre;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TMDBGenre createUpdatedEntity(EntityManager em) {
        TMDBGenre tMDBGenre = new TMDBGenre()
            .tmdbId(UPDATED_TMDB_ID)
            .name(UPDATED_NAME);
        return tMDBGenre;
    }

    @BeforeEach
    public void initTest() {
        tMDBGenre = createEntity(em);
    }

    @Test
    @Transactional
    public void createTMDBGenre() throws Exception {
        int databaseSizeBeforeCreate = tMDBGenreRepository.findAll().size();
        // Create the TMDBGenre
        TMDBGenreDTO tMDBGenreDTO = tMDBGenreMapper.toDto(tMDBGenre);
        restTMDBGenreMockMvc.perform(post("/api/tmdb-genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBGenreDTO)))
            .andExpect(status().isCreated());

        // Validate the TMDBGenre in the database
        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeCreate + 1);
        TMDBGenre testTMDBGenre = tMDBGenreList.get(tMDBGenreList.size() - 1);
        assertThat(testTMDBGenre.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testTMDBGenre.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the TMDBGenre in Elasticsearch
        verify(mockTMDBGenreSearchRepository, times(1)).save(testTMDBGenre);
    }

    @Test
    @Transactional
    public void createTMDBGenreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tMDBGenreRepository.findAll().size();

        // Create the TMDBGenre with an existing ID
        tMDBGenre.setId(1L);
        TMDBGenreDTO tMDBGenreDTO = tMDBGenreMapper.toDto(tMDBGenre);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTMDBGenreMockMvc.perform(post("/api/tmdb-genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBGenreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBGenre in the database
        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeCreate);

        // Validate the TMDBGenre in Elasticsearch
        verify(mockTMDBGenreSearchRepository, times(0)).save(tMDBGenre);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBGenreRepository.findAll().size();
        // set the field null
        tMDBGenre.setTmdbId(null);

        // Create the TMDBGenre, which fails.
        TMDBGenreDTO tMDBGenreDTO = tMDBGenreMapper.toDto(tMDBGenre);


        restTMDBGenreMockMvc.perform(post("/api/tmdb-genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBGenreDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = tMDBGenreRepository.findAll().size();
        // set the field null
        tMDBGenre.setName(null);

        // Create the TMDBGenre, which fails.
        TMDBGenreDTO tMDBGenreDTO = tMDBGenreMapper.toDto(tMDBGenre);


        restTMDBGenreMockMvc.perform(post("/api/tmdb-genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBGenreDTO)))
            .andExpect(status().isBadRequest());

        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTMDBGenres() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBGenre.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getTMDBGenre() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get the tMDBGenre
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres/{id}", tMDBGenre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tMDBGenre.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }


    @Test
    @Transactional
    public void getTMDBGenresByIdFiltering() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        Long id = tMDBGenre.getId();

        defaultTMDBGenreShouldBeFound("id.equals=" + id);
        defaultTMDBGenreShouldNotBeFound("id.notEquals=" + id);

        defaultTMDBGenreShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTMDBGenreShouldNotBeFound("id.greaterThan=" + id);

        defaultTMDBGenreShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTMDBGenreShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId equals to DEFAULT_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId not equals to UPDATED_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId equals to UPDATED_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId is not null
        defaultTMDBGenreShouldBeFound("tmdbId.specified=true");

        // Get all the tMDBGenreList where tmdbId is null
        defaultTMDBGenreShouldNotBeFound("tmdbId.specified=false");
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId is greater than or equal to DEFAULT_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.greaterThanOrEqual=" + DEFAULT_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId is greater than or equal to UPDATED_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.greaterThanOrEqual=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId is less than or equal to DEFAULT_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.lessThanOrEqual=" + DEFAULT_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId is less than or equal to SMALLER_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.lessThanOrEqual=" + SMALLER_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsLessThanSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId is less than DEFAULT_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.lessThan=" + DEFAULT_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId is less than UPDATED_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.lessThan=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByTmdbIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where tmdbId is greater than DEFAULT_TMDB_ID
        defaultTMDBGenreShouldNotBeFound("tmdbId.greaterThan=" + DEFAULT_TMDB_ID);

        // Get all the tMDBGenreList where tmdbId is greater than SMALLER_TMDB_ID
        defaultTMDBGenreShouldBeFound("tmdbId.greaterThan=" + SMALLER_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllTMDBGenresByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where name equals to DEFAULT_NAME
        defaultTMDBGenreShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the tMDBGenreList where name equals to UPDATED_NAME
        defaultTMDBGenreShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where name not equals to DEFAULT_NAME
        defaultTMDBGenreShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the tMDBGenreList where name not equals to UPDATED_NAME
        defaultTMDBGenreShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByNameIsInShouldWork() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where name in DEFAULT_NAME or UPDATED_NAME
        defaultTMDBGenreShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the tMDBGenreList where name equals to UPDATED_NAME
        defaultTMDBGenreShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where name is not null
        defaultTMDBGenreShouldBeFound("name.specified=true");

        // Get all the tMDBGenreList where name is null
        defaultTMDBGenreShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllTMDBGenresByNameContainsSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where name contains DEFAULT_NAME
        defaultTMDBGenreShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the tMDBGenreList where name contains UPDATED_NAME
        defaultTMDBGenreShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllTMDBGenresByNameNotContainsSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        // Get all the tMDBGenreList where name does not contain DEFAULT_NAME
        defaultTMDBGenreShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the tMDBGenreList where name does not contain UPDATED_NAME
        defaultTMDBGenreShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllTMDBGenresByMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);
        TMDBMovie movie = TMDBMovieResourceIT.createEntity(em);
        em.persist(movie);
        em.flush();
        tMDBGenre.addMovie(movie);
        tMDBGenreRepository.saveAndFlush(tMDBGenre);
        Long movieId = movie.getId();

        // Get all the tMDBGenreList where movie equals to movieId
        defaultTMDBGenreShouldBeFound("movieId.equals=" + movieId);

        // Get all the tMDBGenreList where movie equals to movieId + 1
        defaultTMDBGenreShouldNotBeFound("movieId.equals=" + (movieId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTMDBGenreShouldBeFound(String filter) throws Exception {
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBGenre.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTMDBGenreShouldNotBeFound(String filter) throws Exception {
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTMDBGenre() throws Exception {
        // Get the tMDBGenre
        restTMDBGenreMockMvc.perform(get("/api/tmdb-genres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTMDBGenre() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        int databaseSizeBeforeUpdate = tMDBGenreRepository.findAll().size();

        // Update the tMDBGenre
        TMDBGenre updatedTMDBGenre = tMDBGenreRepository.findById(tMDBGenre.getId()).get();
        // Disconnect from session so that the updates on updatedTMDBGenre are not directly saved in db
        em.detach(updatedTMDBGenre);
        updatedTMDBGenre
            .tmdbId(UPDATED_TMDB_ID)
            .name(UPDATED_NAME);
        TMDBGenreDTO tMDBGenreDTO = tMDBGenreMapper.toDto(updatedTMDBGenre);

        restTMDBGenreMockMvc.perform(put("/api/tmdb-genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBGenreDTO)))
            .andExpect(status().isOk());

        // Validate the TMDBGenre in the database
        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeUpdate);
        TMDBGenre testTMDBGenre = tMDBGenreList.get(tMDBGenreList.size() - 1);
        assertThat(testTMDBGenre.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testTMDBGenre.getName()).isEqualTo(UPDATED_NAME);

        // Validate the TMDBGenre in Elasticsearch
        verify(mockTMDBGenreSearchRepository, times(1)).save(testTMDBGenre);
    }

    @Test
    @Transactional
    public void updateNonExistingTMDBGenre() throws Exception {
        int databaseSizeBeforeUpdate = tMDBGenreRepository.findAll().size();

        // Create the TMDBGenre
        TMDBGenreDTO tMDBGenreDTO = tMDBGenreMapper.toDto(tMDBGenre);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTMDBGenreMockMvc.perform(put("/api/tmdb-genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tMDBGenreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TMDBGenre in the database
        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TMDBGenre in Elasticsearch
        verify(mockTMDBGenreSearchRepository, times(0)).save(tMDBGenre);
    }

    @Test
    @Transactional
    public void deleteTMDBGenre() throws Exception {
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);

        int databaseSizeBeforeDelete = tMDBGenreRepository.findAll().size();

        // Delete the tMDBGenre
        restTMDBGenreMockMvc.perform(delete("/api/tmdb-genres/{id}", tMDBGenre.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TMDBGenre> tMDBGenreList = tMDBGenreRepository.findAll();
        assertThat(tMDBGenreList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TMDBGenre in Elasticsearch
        verify(mockTMDBGenreSearchRepository, times(1)).deleteById(tMDBGenre.getId());
    }

    @Test
    @Transactional
    public void searchTMDBGenre() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        tMDBGenreRepository.saveAndFlush(tMDBGenre);
        when(mockTMDBGenreSearchRepository.search(queryStringQuery("id:" + tMDBGenre.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(tMDBGenre), PageRequest.of(0, 1), 1));

        // Search the tMDBGenre
        restTMDBGenreMockMvc.perform(get("/api/_search/tmdb-genres?query=id:" + tMDBGenre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tMDBGenre.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
}
