package com.mgoulene.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBGenreDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBGenreDTO.class);
        TMDBGenreDTO tMDBGenreDTO1 = new TMDBGenreDTO();
        tMDBGenreDTO1.setId(1L);
        TMDBGenreDTO tMDBGenreDTO2 = new TMDBGenreDTO();
        assertThat(tMDBGenreDTO1).isNotEqualTo(tMDBGenreDTO2);
        tMDBGenreDTO2.setId(tMDBGenreDTO1.getId());
        assertThat(tMDBGenreDTO1).isEqualTo(tMDBGenreDTO2);
        tMDBGenreDTO2.setId(2L);
        assertThat(tMDBGenreDTO1).isNotEqualTo(tMDBGenreDTO2);
        tMDBGenreDTO1.setId(null);
        assertThat(tMDBGenreDTO1).isNotEqualTo(tMDBGenreDTO2);
    }
}
