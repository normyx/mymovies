package com.mgoulene.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBMovieDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBMovieDTO.class);
        TMDBMovieDTO tMDBMovieDTO1 = new TMDBMovieDTO();
        tMDBMovieDTO1.setId(1L);
        TMDBMovieDTO tMDBMovieDTO2 = new TMDBMovieDTO();
        assertThat(tMDBMovieDTO1).isNotEqualTo(tMDBMovieDTO2);
        tMDBMovieDTO2.setId(tMDBMovieDTO1.getId());
        assertThat(tMDBMovieDTO1).isEqualTo(tMDBMovieDTO2);
        tMDBMovieDTO2.setId(2L);
        assertThat(tMDBMovieDTO1).isNotEqualTo(tMDBMovieDTO2);
        tMDBMovieDTO1.setId(null);
        assertThat(tMDBMovieDTO1).isNotEqualTo(tMDBMovieDTO2);
    }
}
