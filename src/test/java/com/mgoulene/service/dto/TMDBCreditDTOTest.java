package com.mgoulene.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBCreditDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBCreditDTO.class);
        TMDBCreditDTO tMDBCreditDTO1 = new TMDBCreditDTO();
        tMDBCreditDTO1.setId(1L);
        TMDBCreditDTO tMDBCreditDTO2 = new TMDBCreditDTO();
        assertThat(tMDBCreditDTO1).isNotEqualTo(tMDBCreditDTO2);
        tMDBCreditDTO2.setId(tMDBCreditDTO1.getId());
        assertThat(tMDBCreditDTO1).isEqualTo(tMDBCreditDTO2);
        tMDBCreditDTO2.setId(2L);
        assertThat(tMDBCreditDTO1).isNotEqualTo(tMDBCreditDTO2);
        tMDBCreditDTO1.setId(null);
        assertThat(tMDBCreditDTO1).isNotEqualTo(tMDBCreditDTO2);
    }
}
