package com.mgoulene.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBPictureDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBPictureDTO.class);
        TMDBPictureDTO tMDBPictureDTO1 = new TMDBPictureDTO();
        tMDBPictureDTO1.setId(1L);
        TMDBPictureDTO tMDBPictureDTO2 = new TMDBPictureDTO();
        assertThat(tMDBPictureDTO1).isNotEqualTo(tMDBPictureDTO2);
        tMDBPictureDTO2.setId(tMDBPictureDTO1.getId());
        assertThat(tMDBPictureDTO1).isEqualTo(tMDBPictureDTO2);
        tMDBPictureDTO2.setId(2L);
        assertThat(tMDBPictureDTO1).isNotEqualTo(tMDBPictureDTO2);
        tMDBPictureDTO1.setId(null);
        assertThat(tMDBPictureDTO1).isNotEqualTo(tMDBPictureDTO2);
    }
}
