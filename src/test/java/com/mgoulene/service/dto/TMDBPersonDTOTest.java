package com.mgoulene.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBPersonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBPersonDTO.class);
        TMDBPersonDTO tMDBPersonDTO1 = new TMDBPersonDTO();
        tMDBPersonDTO1.setId(1L);
        TMDBPersonDTO tMDBPersonDTO2 = new TMDBPersonDTO();
        assertThat(tMDBPersonDTO1).isNotEqualTo(tMDBPersonDTO2);
        tMDBPersonDTO2.setId(tMDBPersonDTO1.getId());
        assertThat(tMDBPersonDTO1).isEqualTo(tMDBPersonDTO2);
        tMDBPersonDTO2.setId(2L);
        assertThat(tMDBPersonDTO1).isNotEqualTo(tMDBPersonDTO2);
        tMDBPersonDTO1.setId(null);
        assertThat(tMDBPersonDTO1).isNotEqualTo(tMDBPersonDTO2);
    }
}
