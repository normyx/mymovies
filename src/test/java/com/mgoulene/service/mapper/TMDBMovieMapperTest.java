package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TMDBMovieMapperTest {

    private TMDBMovieMapper tMDBMovieMapper;

    @BeforeEach
    public void setUp() {
        tMDBMovieMapper = new TMDBMovieMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tMDBMovieMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tMDBMovieMapper.fromId(null)).isNull();
    }
}
