package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TMDBPictureMapperTest {

    private TMDBPictureMapper tMDBPictureMapper;

    @BeforeEach
    public void setUp() {
        tMDBPictureMapper = new TMDBPictureMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tMDBPictureMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tMDBPictureMapper.fromId(null)).isNull();
    }
}
