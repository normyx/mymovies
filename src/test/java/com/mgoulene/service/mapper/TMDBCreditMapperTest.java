package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TMDBCreditMapperTest {

    private TMDBCreditMapper tMDBCreditMapper;

    @BeforeEach
    public void setUp() {
        tMDBCreditMapper = new TMDBCreditMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tMDBCreditMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tMDBCreditMapper.fromId(null)).isNull();
    }
}
