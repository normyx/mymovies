package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TMDBGenreMapperTest {

    private TMDBGenreMapper tMDBGenreMapper;

    @BeforeEach
    public void setUp() {
        tMDBGenreMapper = new TMDBGenreMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tMDBGenreMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tMDBGenreMapper.fromId(null)).isNull();
    }
}
