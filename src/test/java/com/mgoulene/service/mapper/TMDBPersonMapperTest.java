package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TMDBPersonMapperTest {

    private TMDBPersonMapper tMDBPersonMapper;

    @BeforeEach
    public void setUp() {
        tMDBPersonMapper = new TMDBPersonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tMDBPersonMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tMDBPersonMapper.fromId(null)).isNull();
    }
}
