package com.mgoulene.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBPictureTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBPicture.class);
        TMDBPicture tMDBPicture1 = new TMDBPicture();
        tMDBPicture1.setId(1L);
        TMDBPicture tMDBPicture2 = new TMDBPicture();
        tMDBPicture2.setId(tMDBPicture1.getId());
        assertThat(tMDBPicture1).isEqualTo(tMDBPicture2);
        tMDBPicture2.setId(2L);
        assertThat(tMDBPicture1).isNotEqualTo(tMDBPicture2);
        tMDBPicture1.setId(null);
        assertThat(tMDBPicture1).isNotEqualTo(tMDBPicture2);
    }
}
