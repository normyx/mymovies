package com.mgoulene.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBPersonTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBPerson.class);
        TMDBPerson tMDBPerson1 = new TMDBPerson();
        tMDBPerson1.setId(1L);
        TMDBPerson tMDBPerson2 = new TMDBPerson();
        tMDBPerson2.setId(tMDBPerson1.getId());
        assertThat(tMDBPerson1).isEqualTo(tMDBPerson2);
        tMDBPerson2.setId(2L);
        assertThat(tMDBPerson1).isNotEqualTo(tMDBPerson2);
        tMDBPerson1.setId(null);
        assertThat(tMDBPerson1).isNotEqualTo(tMDBPerson2);
    }
}
