package com.mgoulene.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBMovieTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBMovie.class);
        TMDBMovie tMDBMovie1 = new TMDBMovie();
        tMDBMovie1.setId(1L);
        TMDBMovie tMDBMovie2 = new TMDBMovie();
        tMDBMovie2.setId(tMDBMovie1.getId());
        assertThat(tMDBMovie1).isEqualTo(tMDBMovie2);
        tMDBMovie2.setId(2L);
        assertThat(tMDBMovie1).isNotEqualTo(tMDBMovie2);
        tMDBMovie1.setId(null);
        assertThat(tMDBMovie1).isNotEqualTo(tMDBMovie2);
    }
}
