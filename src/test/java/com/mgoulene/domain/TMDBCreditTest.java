package com.mgoulene.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBCreditTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBCredit.class);
        TMDBCredit tMDBCredit1 = new TMDBCredit();
        tMDBCredit1.setId(1L);
        TMDBCredit tMDBCredit2 = new TMDBCredit();
        tMDBCredit2.setId(tMDBCredit1.getId());
        assertThat(tMDBCredit1).isEqualTo(tMDBCredit2);
        tMDBCredit2.setId(2L);
        assertThat(tMDBCredit1).isNotEqualTo(tMDBCredit2);
        tMDBCredit1.setId(null);
        assertThat(tMDBCredit1).isNotEqualTo(tMDBCredit2);
    }
}
