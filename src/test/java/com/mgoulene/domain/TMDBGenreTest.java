package com.mgoulene.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class TMDBGenreTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TMDBGenre.class);
        TMDBGenre tMDBGenre1 = new TMDBGenre();
        tMDBGenre1.setId(1L);
        TMDBGenre tMDBGenre2 = new TMDBGenre();
        tMDBGenre2.setId(tMDBGenre1.getId());
        assertThat(tMDBGenre1).isEqualTo(tMDBGenre2);
        tMDBGenre2.setId(2L);
        assertThat(tMDBGenre1).isNotEqualTo(tMDBGenre2);
        tMDBGenre1.setId(null);
        assertThat(tMDBGenre1).isNotEqualTo(tMDBGenre2);
    }
}
