package com.mgoulene.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link TMDBMovieSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class TMDBMovieSearchRepositoryMockConfiguration {

    @MockBean
    private TMDBMovieSearchRepository mockTMDBMovieSearchRepository;

}
